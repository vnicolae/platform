<h4 class="pb-3 mt-4 mb-3 border-bottom">
    Lista bannere
    <a class="btn btn-primary float-right btn-custom" href="/admin/carousel/add" role="button">
        <i class="fas fa-plus"></i> Adauga banner
    </a>
</h4>

{if isset($successMessage)}
    <div class="alert alert-success" role="alert">
        {$successMessage}
    </div>
{/if}
{if isset($successMessageError)}
    <div class="alert alert-danger" role="alert">
        {$successMessageError}
    </div>
{/if}

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Imagine</th>
        <th scope="col">Titlu</th>
        <th scope="col">Subtitlu</th>
        <th scope="col">Link</th>
        <th scope="col">Status</th>
        <th scope="col">Actiuni</th>
    </tr>
    </thead>

    {if $imagesCarousel}
        {assign var="curNumber" value=1}
        {foreach from=$imagesCarousel item=imageCarousel}
            <tr>
                <th scope="row">{$curNumber++}</th>
                <td><img src="/images/carousel/{$imageCarousel.name}" alt="{$imageCarousel.title}" width="200" height="100"></td>
                <td>{$imageCarousel.title}</td>
                <td>{$imageCarousel.sub_title}</td>
                <td>{$imageCarousel.link}</td>
                <td>{if $imageCarousel.active == 1}Activ{else}Inactiv{/if}</td>
                <td>
                    {if $imageCarousel.active == 1}
                        <a href="/admin/carousel/disable/{$imageCarousel.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Dezactiveaza imaginea"
                           onclick="return confirm('Esti sigur ca vrei sa dezactivezi imaginea?')">
                            <i class="fas fa-lock"></i>
                        </a>
                    {else}
                        <a href="/admin/carousel/enable/{$imageCarousel.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Activeaza imaginea"
                           onclick="return confirm('Esti sigur ca vrei sa activezi imaginea?')">
                            <i class="fas fa-unlock"></i>
                        </a>
                    {/if}
                    <a href="/admin/carousel/edit/{$imageCarousel.id}" class="text-dark p-2"
                       data-toggle="tooltip" title="Editeaza imaginea">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </td>
            </tr>
        {/foreach}
    {else}
        <tr class="text-center">
            <td colspan="6">Niciun rezultat</td>
        </tr>
    {/if}
</table>
