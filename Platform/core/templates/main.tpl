<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="{$CONF.sitepath}">

    <title>{$title|default:$meta_titlu|default:"Sistem"}</title>

    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/css/bootstrap-reboot.min.css">

    <script src="//{$CONF.sitepath}lib/js/jquery.min.js"></script>
    <script src="//{$CONF.sitepath}lib/js/bootstrap.min.js"></script>
    <script src="//{$CONF.sitepath}lib/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/index.css">

    <link rel="shortcut icon" href="//{$CONF.sitepath}lib/images/favicon.png" type="image/png">
</head>

<body>
{if isset($smarty.session.user_id)}
    {include file="struct/header.tpl"}
{/if}

{$content}

{if isset($smarty.session.user_id)}
    {include file="struct/footer.tpl"}
{/if}
