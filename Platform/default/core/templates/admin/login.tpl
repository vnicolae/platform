<div class="row login-container border-bottom border-top">
    <div class="col-md-12 bg-light">
        <form method="POST" novalidate>
            <div class="form-group row mt-3">
                <div class="offset-5 col-sm-2 text-center">
                    <img src="//{$CONF.sitepath}images/admin/{$logoSite.logo_site}" style="width: 100%;">
                </div>
                <div class="offset-5 col-sm-2 text-center">
                    <h3 class="mb-0">Sistem - Login</h3>
                </div>
            </div>

            {if isset($error)}
                <div class="form-group row">
                    <div class="offset-4 col-sm-4 text-center">
                        <div class="alert alert-danger mb-0" role="alert">
                            <li class="list-group">{$error}</li>
                        </div>
                    </div>
                </div>
            {/if}

            <div class="form-group row">
                <label class="offset-3 col-sm-2 text-right col-form-label" for="email">Email address</label>
                <div class="col-sm-2">
                    <input type="email" class="form-control" name="email" id="email">
                </div>
            </div>

            <div class="form-group row">
                <label class="offset-3 col-sm-2 text-right col-form-label" for="password">Password</label>
                <div class="col-sm-2">
                    <input type="password" class="form-control" name="password" id="password">
                </div>
            </div>

            <div class="form-group row">
                <div class="offset-5 col-sm-2">
                    <button type="submit" class="btn btn-primary btn-block" name="enter">Log in</button>
                </div>
            </div>
        </form>
    </div>
</div>
