<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="border-top pt-3 pb-3 text-center">
                    <i class="far fa-copyright"></i> {$smarty.now|date_format:"%Y"} e-Learning Design
                </h6>
            </div>
        </div>
    </div>
</div>
