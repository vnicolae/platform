<h2 class="mt-4 mb-3 border-bottom-5">
    {$teacher->addressing} {$teacher->last_name} {$teacher->first_name}
</h2>

<h6 class="mb-3 border-bottom-2">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/teacher">Profesori</a> <i class="fas fa-angle-right"></i>
    {$teacher->addressing} {$teacher->last_name} {$teacher->first_name}
</h6>

<div class="row">
    <div class="col-md-12">
        {if isset($successMessage)}
            <div class="alert alert-success" role="alert">
                {$successMessage}
            </div>
        {/if}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row mb-3">
            <div class="col-md-3 text-center">
                <img src="/images/avatar/{$teacher->avatar|default:'default-avatar.jpg'}" style="max-width: 100%;">
            </div>

            <div class="col-md-9">
                <div class="description">
                    {if $teacher->description}
                        {$teacher->description|nl2br}
                    {else}
                        Acest profesor nu are descriere momentan.
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>

{if isset($smarty.session.client) && $smarty.session.client.client_id != $teacher->id}
    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Adreseaza o intrebare profesorului tau</h3>

        <form class="needs-validation" method="POST" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="message">
                    Mesaj <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <textarea class="form-control" id="message" name="message" rows="3" required></textarea>
                    <div class="invalid-feedback">Introduceti un Mesaj.</div>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    <button type="submit" class="btn btn-primary btn-custom" name="add_message">Trimite</button>
                </div>
            </div>
        </form>
    </div>
{/if}

<div class="row">
    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Lectii publicate</h3>
        <div class="row">
            {if empty($teacher->lessons)}
                <div class="col-md-12">
                    Ne pare rau, acest profesor nu a publicat nicio lectie momentan.
                </div>
            {else}
                {foreach from=$teacher->lessons item=lesson}
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        {include file='lessonBox.tpl'}
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
</div>
