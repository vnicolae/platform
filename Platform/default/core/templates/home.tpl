<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Hei, bine ai venit la noi!
</h4>

<div class="row">
    {foreach from=$products item=product}
        <div class=" item col-md-4">
            {include file='productDetail.tpl'}
        </div>
    {/foreach}
</div>
