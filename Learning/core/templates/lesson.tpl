<h2 class="mt-4 mb-3 border-bottom-5">
    {$lesson->name}
</h2>

<h6 class="mb-3 border-bottom-2">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/clasa/{$lesson->class->url}/">{$lesson->class->name}</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/clasa/{$lesson->class->url}/material/{$lesson->material->url}/">{$lesson->material->name}</a> <i class="fas fa-angle-right"></i>
    {$lesson->name}
</h6>

<div class="row">
    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Detalii</h3>
        <p>{$lesson->details|nl2br}</p>
    </div>

    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Materiale</h3>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Titlu</th>
                {if isset($smarty.session.client)}
                    <th scope="col">Descarca</th>
                {/if}
            </tr>
            </thead>
            {assign var="curNumber" value=1}
            {foreach from=$files item=file}
                <tr>
                    <th scope="row">{$curNumber++}</th>
                    <td>
                        <i class="fas fa-file-alt"></i>
                        {if isset($smarty.session.client)}
                        <a href="/lib/file/lesson/{$file.hash_name}" title="{$file.original_name}" class="text-dark" target="_blank">
                            {/if}

                            {$file.original_name}

                            {if isset($smarty.session.client)}
                        </a>
                        {/if}
                    </td>

                    {if isset($smarty.session.client)}
                        <td>
                            <i class="fas fa-download"></i>
                            <a href="/lesson/download/{$file.hash_name}" class="text-dark">Descarca</a>
                        </td>
                    {/if}
                </tr>
            {/foreach}
        </table>
    </div>

    {if isset($smarty.session.client) && $smarty.session.client.role == 1}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 text-center" style="margin: 0 auto;">
                    <div class="border border-width-2 border-info rounded mt-3 mb-3 p-3">
                        {if $isFavorite}
                            <p>Ai luat testul cu nota 10?<br>Sterge-o din lista ta!</p>
                        {else}
                            <p>Iti place lectia?<br>Salveaz-o in lista ta!</p>
                        {/if}
                        <form class="needs-validation" method="POST" novalidate>
                            {if $isFavorite}
                                <button type="submit" class="btn btn-primary btn-custom" name="remove_favorite">
                                    <i class="fas fa-heart-broken"></i> Elimina
                                </button>
                            {else}
                                <button type="submit" class="btn btn-primary btn-custom" name="add_favorite">
                                    <i class="fas fa-heart"></i> Adauga
                                </button>
                            {/if}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {/if}

    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Despre profesor</h3>
        <div class="row">
            <div class="col-md-12 mb-3" style="display: inline-flex;">
                <div class="mr-4">
                    <a href="/teacher/{$lesson->client->id}" title="{$lesson->client->last_name} {$lesson->client->first_name}" class="text-dark">
                        <img src="/images/avatar/{$lesson->client->avatar|default:'default-avatar.jpg'}" class="width-120">
                    </a>
                </div>
                <div>
                    <a href="/teacher/{$lesson->client->id}" title="{$lesson->client->last_name} {$lesson->client->first_name}" class="text-dark">
                        <b>{$lesson->client->addressing} {$lesson->client->last_name} {$lesson->client->first_name}</b>
                    </a>

                    <a href="/teacher/{$lesson->client->id}" title="{$lesson->client->last_name} {$lesson->client->first_name}" class="text-dark">
                        <div class="mai_mult">
                            citeste mai mult
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Recenzii</h3>
        <div>
            Parerea cititorilor:
            <div class="rating text-warning" style="display: inline-block;" data-inline>{$lesson->average|round}</div>
            <div class="list_litera litera" style="display: inline-block;">{$lesson->average|round:2} ({count($lesson->comments)} voturi)</div>
        </div>

        {if isset($smarty.session.client)}
            Spune si celorlalti cat de mult ti-a placut aceasta lectie!
            <form class="needs-validation" method="POST" novalidate>
                <div class="form-group row">
                    <label class="offset-1 col-sm-2 col-form-label" for="rating">
                        Votul tau <span class="text-danger font-weight-bold">*</span>
                    </label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control rating text-warning border-width-0 pl-0" name="rating" id="rating" required>
                        <div class="invalid-feedback">Introduceti un Rating.</div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="offset-1 col-sm-2 col-form-label" for="review">
                        Parerea ta
                    </label>
                    <div class="col-sm-8">
                        <textarea class="form-control" id="review" name="review" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <small class="offset-3 col-sm-8">
                        Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                    </small>
                </div>

                <div class="form-group row">
                    <div class="offset-3 col-sm-8">
                        <button type="submit" class="btn btn-primary btn-custom" name="add_review">Adauga</button>
                    </div>
                </div>
            </form>
        {/if}
    </div>

    <div class="col-md-12">
        <h3 class="mb-3 border-bottom-2">Recenziile cititorilor</h3>

        {if empty($lesson->comments)}
            <p>Momentan, nu avem recenzii.</p>
        {else}
            {foreach from=$lesson->comments item=$comment}
                {if $comment.review}
                    <div class="row">
                        <div class="offset-1 col-sm-10 pb-1 mb-3">
                            <div class="border rounded p-2">
                                <b>{$comment.first_name} {$comment.last_name}</b>
                                <span style="float: right;">
                                    <i class="far fa-clock"></i>
                                    {$comment.created_at|date_format:"%d.%m.%Y %H:%M"}
                                </span>
                                <br>
                                {$comment.review|nl2br}
                            </div>
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
    </div>
</div>

