<h2 class="mt-4 mb-3 border-bottom-5">
    Adauga lectie
</h2>

{if isset($error)}
    <div class="alert alert-danger" role="alert">
        {$error}
    </div>
{/if}

<div class="mt-4">
    <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="name">
                Titlu lectie <span class="text-danger font-weight-bold">*</span>
            </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" value="{if isset($smarty.post.name)}{$smarty.post.name}{/if}" required>
                <div class="invalid-feedback">Introduceti un Titlu valid.</div>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="class">
                Clasa <span class="text-danger font-weight-bold">*</span>
            </label>
            <div class="col-sm-8">
                <select name="class" class="form-control select2" id="class" required>
                    <option value="" selected></option>
                    {foreach from=$classes item=class}
                        <option value="{$class.id}" {if isset($smarty.post.class) && $smarty.post.class eq $class.id}selected{/if}>
                            {$class.name}
                        </option>
                    {/foreach}
                </select>
                <div class="invalid-feedback">Alegeti o Clasa valida.</div>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="material">
                Materie <span class="text-danger font-weight-bold">*</span>
            </label>
            <div class="col-sm-8">
                <select name="material" class="form-control select2" id="material" required>
                    <option value="" selected></option>
                    {foreach from=$materials item=material}
                        <option value="{$material.id}" {if isset($smarty.post.material) && $smarty.post.material eq $material.id}selected{/if}>
                            {$material.name}
                        </option>
                    {/foreach}
                </select>
                <div class="invalid-feedback">Alegeti o Materie valida.</div>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="visible">
                Vizibil
            </label>
            <div class="col-sm-8">
                <input type="checkbox" id="visible" name="visible" class="custom-control-input" value="1" {if isset($smarty.post.visible)}checked{/if}>
                <label class="custom-control-label position-relative vertical-webkit-middle" for="visible">
                    <i class="fas fa-info-circle ml-4" data-toggle="tooltip" title="Daca este bifat, lectia este vizibil in site"></i>
                </label>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="details">
                Detalii
            </label>
            <div class="col-sm-8">
                <textarea class="form-control" id="details" name="details" rows="3">{if isset($smarty.post.details)}{$smarty.post.details}{/if}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="files">
                Materiale
            </label>
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="files" name="files[]"
                           data-file-types="image/jpeg,image/jpg,image/png,.pdf" lang="ro"
                           accept="image/*,.pdf" onchange="preview_files();" multiple/>
                    <label class="custom-file-label" for="image">Alege material</label>
                </div>
                <div class="row" id="file_preview"></div>
            </div>
        </div>

        <div class="form-group row">
            <small class="offset-3 col-sm-8">
                Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
            </small>
        </div>

        <div class="form-group row">
            <div class="offset-3 col-sm-8">
                <button type="submit" class="btn btn-primary btn-custom" name="add_lesson">Salveaza</button>
                <a class="btn btn-danger btn-custom" href="/account/myLessons" role="button">Anuleaza</a>
            </div>
        </div>
    </form>
</div>
