<?php

function notFound()
{
    global $template;

    return $template->fetch('404.tpl');
}
