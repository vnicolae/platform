<h4 class="pb-3 mt-4 mb-3 border-bottom">
    Lista elevi
</h4>

{if isset($successMessage)}
    <div class="alert alert-success" role="alert">
        {$successMessage}
    </div>
{/if}

<div class="col-sm-12 text-center mb-3">
    <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
       onclick="$('#filter_student').toggle('slow');">Filtre</a>
</div>

<form id="filter_student" method="POST" novalidate=""
      style="display:{if isset($smarty.post.filter_student)}block{else}none{/if}">
    <div class="form-group row">
        <label class="col-sm-6 col-form-label" for="last_name">
            Nume
            <input type="text" class="form-control" name="last_name" id="last_name"
                   value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                   placeholder="ex: Popescu">
        </label>
        <label class="col-sm-6 col-form-label" for="first_name">
            Prenume
            <input type="text" class="form-control" name="first_name" id="first_name"
                   value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                   placeholder="ex: Ion">
        </label>
        <label class="col-sm-6 col-form-label" for="phone_number">
            Telefon
            <input type="text" class="form-control" name="phone_number" id="phone_number"
                   value="{if isset($smarty.post.phone_number)}{$smarty.post.phone_number}{/if}"
                   placeholder="ex: 07xxxxxxxx">
        </label>
        <label class="col-sm-6 col-form-label" for="email">
            Email
            <input type="email" class="form-control" name="email" id="email"
                   value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}"
                   placeholder="ex: popescu.ion@exemple.com">
        </label>

        <div class="col-sm-12 mt-2 text-center">
            <button type="submit" class="btn btn-success btn-custom" name="filter_student">Filtreaza</button>
        </div>
    </div>
</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nume</th>
        <th scope="col">Prenume</th>
        <th scope="col">Email</th>
        <th scope="col">Telefon</th>
        <th scope="col">Actiuni</th>
    </tr>
    </thead>

    {if $students}
        {assign var="curNumber" value=1}
        {foreach from=$students item=student}
            <tr>
                <th scope="row">{$curNumber++}</th>
                <td>{$student.last_name}</td>
                <td>{$student.first_name}</td>
                <td>{$student.email}</td>
                <td>{$student.phone_number}</td>
                <td>
                    <a href="/admin/students/view/{$student.id}" class="text-dark"
                       data-toggle="tooltip" title="Vezi detalii elev">
                        <i class="fas fa-eye"></i>
                    </a>
                    {if $student.block == 0}
                        <a href="/admin/students/block/{$student.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Blocheaza elevul"
                           onclick="return confirm('Esti sigur ca vrei sa blochezi elevul?')">
                            <i class="fas fa-lock"></i>
                        </a>
                    {else}
                        <a href="/admin/students/unblock/{$student.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Deblocheaza elevul"
                           onclick="return confirm('Esti sigur ca vrei sa deblochezi elevul?')">
                            <i class="fas fa-unlock"></i>
                        </a>
                    {/if}
                </td>
            </tr>
        {/foreach}
    {else}
        <tr class="text-center">
            <td colspan="6">Niciun rezultat</td>
        </tr>
    {/if}
</table>
