$(document).ready(function () {
    // Enable Carousel Controls
    $(".prev-slider").click(function () {
        $("#demo").carousel("prev");
    });

    $(".next-slider").click(function () {
        $("#demo").carousel("next");
    });

    // Enable Carousel Controls
    $(".prev-product").click(function () {
        $("#product").carousel("prev");
    });

    $(".next-product").click(function () {
        $("#product").carousel("next");
    });
});

$("ul.nav li.my-account-dropdown").hover(function () {
    $(this).find('.my-account-dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function () {
    $(this).find('.my-account-dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

$("ul.nav li.my-cart-dropdown").hover(function () {
    $(this).find('.my-cart-dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function () {
    $(this).find('.my-cart-dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

$("a#add-to-cart").click(function () {
    $.ajax({
        url: "/ajax/addToCart/" + $(this).attr("data-id"),
        dataType: 'json',
        success: function (result) {
            $('#cart-info').html(result.result);
            $('#cart-products-counter').html(result.cartProductsCounter);
        }
    });
});

$("a.delete-to-cart").click(function () {
    $.ajax({
        url: "/ajax/deleteToCart/" + $(this).attr("data-id"),
        dataType: 'json',
        success: function (result) {
            $('#cart-info').html(result.result);
            $('#cart-products-counter').html(result.cartProductsCounter);
        }
    });
});

$("select.sync-quantity-to-cart-page").change(function () {
    var typeDelivery = "personal";
    if($('#courier').is(':checked')) {
        var typeDelivery = "courier";
    }
    $.ajax({
        type: 'POST',
        url: "/ajax/syncQuantityToCartPage/" + $(this).attr("data-id"),
        data: {
            quantity: $(this).val(),
            typeDelivery: typeDelivery
        },
        dataType: 'json',
        success: function (result) {
            $('#cart-page-info').html(result.result);
            $('#cart-products-counter').html(result.cartProductsCounter);
        }
    });
});

$("a.delete-product-to-cart-page").click(function () {
    var typeDelivery = "personal";
    if($('#courier').is(':checked')) {
        var typeDelivery = "courier";
    }
    $.ajax({
        type: 'POST',
        url: "/ajax/deleteProductToCartPage/" + $(this).attr("data-id"),
        data: {
            typeDelivery: typeDelivery
        },
        dataType: 'json',
        success: function (result) {
            $('#cart-page-info').html(result.result);
            $('#cart-products-counter').html(result.cartProductsCounter);
        }
    });
});

// Cart Type of Person
$('input[name="typePerson"]').change(function () {
    $('input[name="billingData"]').prop('checked', false);
    $('.sub-box-type-person').hide();
});

$('input[name="billingData"]').change(function () {
    if ($("#new-individual-person").is(':checked')) {
        $('.detendent-new-individual-person').removeAttr('disabled');
    } else {
        $('.detendent-new-individual-person').attr('disabled', true);
    }
    if ($("#new-legal-person").is(':checked')) {
        $('.detendent-new-legal-person').removeAttr('disabled');
    } else {
        $('.detendent-new-legal-person').attr('disabled', true);
    }
});

$('input[name="typePerson"]').click(function () {
    var inputValue = $(this).attr("id");
    var targetBox = $("." + inputValue);
    $(".box-type-person").not(targetBox).hide();
    $(targetBox).show();
});

$('input[name="billingData"]').click(function () {
    var inputValue = $(this).attr("id");
    var targetBox = $("." + inputValue);
    $(".sub-box-type-person").not(targetBox).hide();
    $(targetBox).show();
});
// End Cart Type of Person

// Cart Type of Delivery
$('input[name="typeDelivery"]').change(function () {
    $('input[name="deliveryData"]').prop('checked', false);
    $('.sub-box-type-delivery').hide();
});

$('input[name="deliveryData"]').change(function () {
    if ($("#new-courier").is(':checked')) {
        $('.detendent-new-courier').removeAttr('disabled');
    } else {
        $('.detendent-new-courier').attr('disabled', true);
    }
});

$('input[name="typeDelivery"]').click(function () {
    var inputValue = $(this).attr("id");
    var targetBox = $("." + inputValue);
    $(".box-type-delivery").not(targetBox).hide();
    $(targetBox).show();
});

$('input[name="deliveryData"]').click(function () {
    var inputValue = $(this).attr("id");
    var targetBox = $("." + inputValue);
    $(".sub-box-type-delivery").not(targetBox).hide();
    $(targetBox).show();
});
// End Cart Type of Delivery

// Cart Type of Payment
$('input[name="paymentData"]').click(function () {
    var inputValue = $(this).attr("id");
    var targetBox = $("." + inputValue);
    $(".sub-box-type-payment").not(targetBox).hide();
    $(targetBox).show();
});
// End Cart Type of Payment

// Synchronise Delivery Price
$('input[name="typeDelivery"]').change(function () {
    $.ajax({
        type: 'POST',
        url: "/ajax/syncDeliveryPrice/",
        data: {
            typeDelivery: $(this).val()
        },
        dataType: 'json',
        success: function (result) {
            $('#deliveryPrice').html(result.deliveryPrice);
            $('#totalPrice').html(result.totalPrice);
        }
    });
});
// End Synchronise Delivery Price

// Disable form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

// Disable form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation-cart');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$('.datepicker').datepicker({
    weekStart: 1,
    daysOfWeekHighlighted: "6,0",
    autoclose: true,
    todayHighlight: true
});
$('input[name="release_date"]').datepicker("setDate", new Date());
$('input[name="due_date"]').datepicker("setDate", new Date());

// Return Number Of Invoice By Series Id
$('select[name="series_invoice"]').change(function () {
    $.ajax({        
        url: "/ajax/getNumberOfInvoice/" + $(this).val(),
        success: function (result) {
            $('#number_invoice').val(result);
        }
    });
});

// Return Number Of Receipt By Series Id
$('select[name="series_receipt"]').change(function () {
    $.ajax({
        url: "/ajax/getNumberOfReceipt/" + $(this).val(),
        success: function (result) {
            $('#number_receipt').val(result);
        }
    });
});

// Tooltip
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
var loadFile = function(event) {
    var output = document.getElementById('preview_carousel');
    output.src = URL.createObjectURL(event.target.files[0]);
};

function deletePreview (ele, i) {
    try {
        $(ele).parent().remove();
        window.filesToUpload.splice(i, 1);
    } catch (e) {
        console.log(e.message);
    }
}

function preview_images() {
    var total_file = document.getElementById("images").files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append(
            "<div class='col-md-3'>" +
            "<i class='fas fa-trash-alt bg-dark text-light position-absolute right-0 p-2 rounded-circle' onClick='deletePreview(this, " + i + ")'></i>" +
            "<img style='width: 100%' src='" + URL.createObjectURL(event.target.files[i]) + "'>" +
            "</div>"
        );
    }
}

// Return Url of Product
$('input[name="name"].product-name').change(function () {
    $.ajax({
        type: 'POST',
        url: "/ajax/getUrlProduct",
        data: {
            productName: $(this).val()
        },
        success: function (result) {
            $('input[name="url"].product-url').val(result);
        }
    });
});

// Return Url of Category
$('input[name="name"].category-name').change(function () {
    $.ajax({
        type: 'POST',
        url: "/ajax/getUrlCategory",
        data: {
            categoryName: $(this).val()
        },
        success: function (result) {
            $('input[name="url"].category-url').val(result);
        }
    });
});
