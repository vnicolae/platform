<?php

function lessons()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'view') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT * FROM lesson WHERE id = '" . intval($link[4]) . "';";
                $lesson = $database->query($sql)->fetch_object();

                if (empty($lesson)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT * FROM file WHERE lesson_id =  '" . $lesson->id . "';";
                $files = $database->select($sql);

                $lesson->class = getClass($lesson->class_id);
                $lesson->material = getMaterial($lesson->material_id);

                $template->assign('lesson', $lesson);
                $template->assign('files', $files);

                return $template->fetch('admin/lesson/lessonDetail.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'cancel') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE lesson
                        SET active = 0
                        WHERE active = 1 AND id = '" . intval($link[4]) . "';";
                $database->query($sql);

                $_SESSION['message'] = 'Lectia cu numarul #' . intval($link[4]) . ' a fost anulata.';

                header("Location: /admin");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    $where = "WHERE 1";
    if (isset($_POST['filter_lesson'])) {
        if ($_POST['last_name']) {
            $where .= " AND c.last_name LIKE '%" . $database->escape($_POST['last_name']) . "%'";
        }
        if ($_POST['first_name']) {
            $where .= " AND c.first_name LIKE '%" . $database->escape($_POST['first_name']) . "%'";
        }
        if ($_POST['class']) {
            $where .= " AND l.class_id = '" . intval($_POST['class']) . "'";
        }
        if ($_POST['material']) {
            $where .= " AND l.material_id = '" . intval($_POST['material']) . "'";
        }
        if ($_POST['created_at']) {
            $where .= " AND l.created_at >= '" . date('Y-m-d', strtotime($_POST['created_at'])) . "'";
            $where .= " AND l.created_at < '" . date('Y-m-d', strtotime($_POST['created_at'] . ' +1 day')) . "'";
        }
        if ($_POST['updated_at']) {
            $where .= " AND l.updated_at >= '" . date('Y-m-d', strtotime($_POST['updated_at'])) . "'";
            $where .= " AND l.updated_at < '" . date('Y-m-d', strtotime($_POST['updated_at'] . ' +1 day')) . "'";
        }
        if ($_POST['visible']) {
            if ($_POST['visible'] === 'visible') {
                $where .= " AND l.visible = '1'";
            } elseif ($_POST['visible'] === 'invisible') {
                $where .= " AND l.visible = '0'";
            }
        }
        if ($_POST['active']) {
            if ($_POST['active'] === 'active') {
                $where .= " AND l.active = '1'";
            } elseif ($_POST['active'] === 'inactive') {
                $where .= " AND l.active = '0'";
            }
        }
    }

    $sql = "SELECT l.*, c.first_name, c.last_name
            FROM lesson l
            JOIN client c ON c.id = l.client_id
            {$where}
            ORDER BY l.created_at DESC;";
    $lessons = $database->select($sql);

    foreach ($lessons as $key => $lesson) {
        $lessons[$key]['class'] = getClass($lesson['class_id']);
        $lessons[$key]['material'] = getMaterial($lesson['material_id']);
        $lessons[$key]['client'] = getClient($lesson['client_id']);
        $lessons[$key]['comments'] = getComments($lesson['id']);
    }

    if (isset($_SESSION['message'])) {
        $successMessage = $_SESSION['message'];
        $template->assign('successMessage', $successMessage);
        unset($_SESSION['message']);
    }

    $template->assign('lessons', $lessons);
    $template->assign('classes', getAllClasses());
    $template->assign('materials', getAllMaterials());

    return $template->fetch('admin/lesson/lessonList.tpl');
}
