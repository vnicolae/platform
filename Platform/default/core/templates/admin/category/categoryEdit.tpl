<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Editeaza categoria #{$categoryEdit->id}
    </h4>

    {if isset($error)}
        <div class="alert alert-danger" role="alert">
            {$error}
        </div>
    {/if}

    <div class="mt-4">
        <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="name">
                    Nume categorie <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control category-name" id="name" name="name"
                           value="{$categoryEdit->name}" required>
                    <div class="invalid-feedback">Introduceti un Nume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="url">
                    Link <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control category-url" id="url" name="url"
                           value="{$categoryEdit->url}" required>
                    <div class="invalid-feedback">Introduceti un Link valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="category">
                    Categorie parinte
                </label>
                <div class="col-sm-8">
                    <select name="category" class="form-control select2">
                        <option value="" selected></option>
                        {foreach from=$categories item=category}
                            {if $category.id != $categoryEdit->id}
                                <option value="{$category.id}"
                                        {if $categoryEdit->category_id eq $category.id}selected{/if}>
                                    {$category.name}
                                </option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="active">
                    Vizibil
                </label>
                <div class="col-sm-8">
                    <input type="checkbox" id="active" name="active" class="custom-control-input" value="1"
                           {if $categoryEdit->active}checked{/if}>
                    <label class="custom-control-label position-relative vertical-webkit-middle" for="active">
                        <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                           title="Daca este bifat, categoria este vizibila in site"></i>
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    {if isset($smarty.session.admin.roles.edit_category)}
                        <button type="submit" class="btn btn-primary btn-custom" name="edit_category">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/categories/list" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
