<div class="container" id="content-site-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Adauga business
    </h4>

    <div class="mt-4">
        <form class="needs-validation" method="POST" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="name">
                    Denumire companie <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" required>
                    <div class="invalid-feedback">Introduceti o Denumire companie valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="admin_last_name">
                    Nume administrator <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="admin_last_name" name="admin_last_name" required>
                    <div class="invalid-feedback">Introduceti un Nume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="admin_first_name">
                    Prenume administrator <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="admin_first_name" name="admin_first_name" required>
                    <div class="invalid-feedback">Introduceti un Prenume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="admin_email">
                    Email administrator <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" id="admin_email" name="admin_email" required>
                    <div class="invalid-feedback">Introduceti un Email valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="admin_password">
                    Parola administrator <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="admin_password" name="admin_password" required>
                    <div class="invalid-feedback">Introduceti o Parola valida.</div>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    <button type="submit" class="btn btn-primary btn-custom" name="save">Salveaza</button>
                    <a class="btn btn-danger btn-custom" href="/" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
