<link rel="stylesheet" type="text/css" href="http://{$CONF.sitepath}lib/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="http://{$CONF.sitepath}lib/index.css">

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Denumire<br>client</th>
        <th scope="col">Serie<br>Numar</th>
        <th scope="col">Data<br>emitere</th>
        <th scope="col">Data<br>scadenta</th>
        <th scope="col">Val.<br>fara TVA</th>
        <th scope="col">Val.<br>TVA</th>
        <th scope="col">Total<br>general</th>
        <th scope="col">Doc.<br>plata</th>
        <th scope="col">Val.<br>achitata</th>
        <th scope="col">Rest<br>plata</th>
    </tr>
    <tr>
        <th scope="col">0</th>
        <th scope="col">1</th>
        <th scope="col">2</th>
        <th scope="col">3</th>
        <th scope="col">4</th>
        <th scope="col">5</th>
        <th scope="col">6</th>
        <th scope="col">7</th>
        <th scope="col">8</th>
        <th scope="col">9</th>
        <th scope="col">10</th>
    </tr>
    </thead>

    {if $results}
        {assign var="curNumber" value=1}
        {assign var="amountPaid" value=0}
        {assign var="amountDue" value=0}
        {foreach from=$results item=result}
            <tr>
                <th scope="row">{$curNumber++}</th>
                <td>
                    <a href="/admin/client/view/{$result.client_id}"
                       class="text-dark" data-toggle="tooltip" title="Vezi detalii client">
                        {$result.last_name} {$result.first_name}
                    </a>
                </td>
                <td>
                    <a href="/admin/invoice/view/{$result.id}"
                       class="text-dark" data-toggle="tooltip" title="Vezi detalii factura">
                        {$result.series_value} {$result.number}
                    </a>
                </td>
                <td>{$result.release_date|date_format:"%d.%m.%Y"}</td>
                <td>{$result.due_date|date_format:"%d.%m.%Y"}</td>
                <td>{$result.totals.subTotalWithoutVAT} Lei</td>
                <td>{$result.totals.subTotalVAT} Lei</td>
                <td>{$result.total_price|string_format: "%.2f"} Lei</td>
                <td>
                    {if isset($result.receipt.id)}
                        <a href="/admin/receipt/view/{$result.receipt.id}"
                           class="text-dark" data-toggle="tooltip" title="Vezi detalii factura">
                            {$result.receipt.series_value} {$result.receipt.number}
                        </a>
                    {else}
                        -
                    {/if}
                </td>
                <td>
                    {if isset($result.receipt.id)}
                        {$result.total_price|string_format: "%.2f"} Lei
                        {$amountPaid = $amountPaid + $result.total_price}
                    {else}
                        0.00 Lei
                    {/if}
                </td>
                <td>
                    {if isset($result.receipt.id)}
                        0.00 Lei
                    {else}
                        {$result.total_price|string_format: "%.2f"} Lei
                        {$amountDue = $amountDue + $result.total_price}
                    {/if}
                </td>
            </tr>
        {/foreach}

        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col">{$totalWithoutVAT|string_format: "%.2f"} Lei</th>
            <th scope="col">{$totalVAT|string_format: "%.2f"} Lei</th>
            <th scope="col">{$totalGeneral|string_format: "%.2f"} Lei</th>
            <th scope="col"></th>
            <th scope="col">{$amountPaid|string_format: "%.2f"} Lei</th>
            <th scope="col">{$amountDue|string_format: "%.2f"} Lei</th>
        </tr>
    {else}
        <tr class="text-center">
            <td colspan="11">Niciun rezultat</td>
        </tr>
    {/if}
</table>
