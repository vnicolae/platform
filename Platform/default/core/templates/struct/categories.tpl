<div class="list-group border rounded-bottom mt-3 list-categories">
    <ul class="nav flex-column flex-nowrap">
        {foreach from=$categories item=category name=category}
            <li class="nav-item {if not $smarty.foreach.category.last}border-bottom{/if}">
                <a class="nav-link collapsed text-dark" href="#" data-toggle="collapse"
                   data-target="#submenu{$category.id}">
                    {$category.name}
                </a>
                <div class="collapse" id="submenu{$category.id}">
                    <ul class="flex-column nav pl-2 flex-nowrap">
                        <li class="nav-item">
                            <a class="nav-link pl-4 text-dark" href="//{$CONF.sitepath}category/{$category.url}">
                                Toate produsele
                            </a>
                        </li>
                        {if isset($category.sub_categories)}
                            {foreach from=$category.sub_categories item=subCategory}
                                <li class="nav-item">
                                    <a class="nav-link pl-4 text-dark" href="//{$CONF.sitepath}category/{$subCategory.url}">
                                        {$subCategory.name}
                                    </a>
                                </li>
                            {/foreach}
                        {/if}
                    </ul>
                </div>
            </li>
        {/foreach}
    </ul>
</div>

<style>
    .nav-link[data-toggle].collapsed:after {
        content: "▾";
        float: right;
    }

    .nav-link[data-toggle]:not(.collapsed):after {
        content: "▴";
        float: right;
    }
</style>
