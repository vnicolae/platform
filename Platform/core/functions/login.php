<?php

function login()
{
    global $db;
    global $CONF;
    global $template;

    $error = '';
    if (isset($_POST['enter'])) {
        $email = $db->escape_string($_POST['email']);
        $password = $db->escape_string($_POST['password']);

        $sql = "SELECT * FROM user WHERE email = '{$email}'";
        $account = $db->query($sql)->fetch_object();

        if (empty($account)) {
            $error = 'E-mail incorect!';
        } elseif (md5($password) != $account->password) {
            $error = 'Parola incorecta!';
        } elseif ($account->active != 1) {
            $error = 'Acces limitat!';
        }

        if ($error) {
            $template->assign('error', $error);
        } else {
            $_SESSION['user_id'] = $account->id;
            $_SESSION['email'] = $account->email;
            $_SESSION['type'] = $account->type;

            header("Location: /");
        }
    }

    $template->assign('CONF', $CONF);

    return $template->fetch('login.tpl');
}
