<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Intra in cont
</h4>

<form class="needs-validation" method="POST" novalidate autocomplete="off">
    {if isset($errors) && empty($error)}
        <div class="form-group row">
            <div class="offset-4 col-sm-6">
                <div class="alert alert-danger mb-0" role="alert">
                    {foreach from=$errors item=error}
                        <li class="list-group">{$error}</li>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}

    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="email">
            Email <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="email" class="form-control" id="email" name="email"
                   value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" required>
            <div class="invalid-feedback">Introduceti un Email valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="password">
            Parola <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="password" class="form-control" id="password" name="password" required>
            <div class="invalid-feedback">Introduceti o Parola valida.</div>
        </div>
    </div>

    <div class="form-group row">
        <small class="offset-4 col-sm-7">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-7">
            <button type="submit" class="btn btn-primary btn-custom" name="login_account">Intra in cont</button>
        </div>
    </div>
</form>
