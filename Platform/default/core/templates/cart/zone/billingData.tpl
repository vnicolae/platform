<div class="custom-control custom-radio custom-control-inline m-2">
    <input type="radio" id="individual-person" name="typePerson" class="custom-control-input"
           value="individual-person" required checked>
    <label class="custom-control-label" for="individual-person">Persoana fizica</label>
</div>

<div class="custom-control custom-radio custom-control-inline m-2">
    <input type="radio" id="legal-person" name="typePerson" class="custom-control-input"
           value="legal-person" required>
    <label class="custom-control-label" for="legal-person">Persoana juridica</label>
</div>

<div class="box-type-person individual-person" style="display: block">
    {foreach from=$billing_data_individual_person item=individual_person}
        <div class="custom-control custom-radio m-2">
            <input type="radio" id="existing-individual-person-{$individual_person.id}" name="billingData"
                   class="custom-control-input" value="{$individual_person.id}" required>
            <label class="custom-control-label" for="existing-individual-person-{$individual_person.id}">
                <div class="existing-individual-person-{$individual_person.id} sub-box-type-person font-weight-bold">
                    Nume si numar de telefon:
                </div>
                <div>
                    <span class="person_full_name">{$individual_person.full_name}</span> -
                    <span class="person_phone_number">{$individual_person.phone_number}</span>
                </div>
                <div class="existing-individual-person-{$individual_person.id} sub-box-type-person font-weight-bold">
                    Adresa:
                </div>
                <div>
                    <span class="person_address">{$individual_person.address}</span>
                </div>
            </label>
        </div>
    {/foreach}

    <div class="custom-control custom-radio m-2">
        <input type="radio" id="new-individual-person" name="billingData" class="custom-control-input"
               value="new-individual-person" required>
        <label class="custom-control-label" for="new-individual-person">
            Adauga date de facturare
        </label>
        <div class="invalid-feedback">Selecteaza o modalitate de facturare</div>

        <div class="new-individual-person sub-box-type-person">
            <div class="form-group row">
                <div class="col-sm-12 font-weight-bold pt-2">Persoana de contact</div>
                <label class="col-sm-6 col-form-label" for="full_name">
                    Nume si Prenume <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-individual-person" id="full_name"
                           placeholder="ex: Popescu Ion" name="individual-person[full_name]" required>
                    <div class="invalid-feedback">Introduceti un Nume si Prenume valid.</div>
                </label>
                <label class="col-sm-6 col-form-label" for="phone_number">
                    Telefon <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-individual-person" id="phone_number"
                           placeholder="ex: 07xxxxxxxx" name="individual-person[phone_number]" required>
                    <div class="invalid-feedback">Introduceti un Telefon valid.</div>
                </label>

                <div class="col-sm-12 font-weight-bold pt-2">Adresa de facturare</div>
                <label class="col-sm-12 col-form-label" for="address">
                    Adresa <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-individual-person" id="address"
                           placeholder="ex: Strada, numar, bloc, scara, etaj, apartament, judet, localitate"
                           name="individual-person[address]" required>
                    <div class="invalid-feedback">Introduceti o Adresa valida.</div>
                </label>

                <small class="col-sm-12">
                    Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>
        </div>
    </div>
</div>

<div class="box-type-person legal-person">
    {foreach from=$billing_data_legal_person item=legal_person}
        <div class="custom-control custom-radio m-2">
            <input type="radio" id="existing-legal-person-{$legal_person.id}" name="billingData"
                   class="custom-control-input" value="{$legal_person.id}" required>
            <label class="custom-control-label" for="existing-legal-person-{$legal_person.id}">
                <div class="existing-legal-person-{$legal_person.id} sub-box-type-person font-weight-bold">
                    Nume companie si CUI:
                </div>
                <div>
                    <span class="person_full_name">{$legal_person.full_name}</span> -
                    <span class="person_unique_registration_code">{$legal_person.unique_registration_code}</span>
                </div>
                <div class="existing-legal-person-{$legal_person.id} sub-box-type-person font-weight-bold">
                    Sediu central:
                </div>
                <div>
                    <span class="person_address">{$legal_person.address}</span>
                </div>
            </label>
        </div>
    {/foreach}

    <div class="custom-control custom-radio m-2">
        <input type="radio" id="new-legal-person" name="billingData" class="custom-control-input"
               value="new-legal-person" required>
        <label class="custom-control-label" for="new-legal-person">
            Adauga date de facturare
        </label>
        <div class="invalid-feedback">Selecteaza o modalitate de facturare</div>

        <div class="new-legal-person sub-box-type-person">
            <div class="form-group row">
                <div class="col-sm-12 font-weight-bold pt-2">Detalii companie</div>
                <label class="col-sm-6 col-form-label" for="full_name">
                    Nume companie <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-legal-person" id="full_name"
                           placeholder="ex: Exemplu Com SRL" name="legal-person[full_name]" required>
                    <div class="invalid-feedback">Introduceti un Nume companie valid.</div>
                </label>
                <label class="col-sm-6 col-form-label" for="unique_registration_code">
                    Cod unic de inregistrare <span class="text-danger font-weight-bold">*</span>
                    <div class="row">
                        <label class="col-sm-4 col-form-label pt-0 pb-0">
                            <select class="form-control detendent-new-legal-person"
                                    name="legal-person[vat_payer]" required>
                                <option value="0">-</option>
                                <option value="RO">RO</option>
                            </select>
                        </label>
                        <label class="col-sm-8 col-form-label pt-0 pb-0">
                            <input type="text" class="form-control detendent-new-legal-person"
                                   id="unique_registration_code" placeholder="ex: 1234567"
                                   name="legal-person[unique_registration_code]" required>
                            <div class="invalid-feedback">Introduceti un Cod fiscal valid.</div>
                        </label>
                    </div>
                </label>

                <div class="col-sm-12 font-weight-bold pt-2">Sediu central</div>
                <label class="col-sm-12 col-form-label" for="address">
                    Adresa <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-legal-person" id="address"
                           placeholder="ex: Strada, numar, bloc, scara, etaj, apartament, judet, localitate"
                           name="legal-person[address]" required>
                    <div class="invalid-feedback">Introduceti o Adresa valida.</div>
                </label>

                <small class="col-sm-12">
                    Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>
        </div>
    </div>
</div>