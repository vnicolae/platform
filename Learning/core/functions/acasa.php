<?php

function acasa()
{
    global $template;

    $template->assign('classes', getAllClasses());
    $template->assign('materials', getAllMaterials());

    return $template->fetch('home.tpl');
}
