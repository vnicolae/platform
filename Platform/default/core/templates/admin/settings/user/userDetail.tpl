<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Utilizator #{$user->first_name} {$user->last_name}
        {if isset($smarty.session.admin.roles.access_settings)}
            <a class="btn btn-primary float-right" href="/admin/settings/user/edit/{$user->id}" role="button">
                <i class="fas fa-pencil-alt"></i> Editeaza utilizatorul
            </a>
        {/if}
    </h4>

    <div class="border rounded-bottom p-3 mb-3">
        <div class="row">
            <div class="col-4">
                <h6>Email</h6>
            </div>
            <div class="col-8">
                {$user->email}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Nume</h6>
            </div>
            <div class="col-8">
                {$user->last_name}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Prenume</h6>
            </div>
            <div class="col-8">
                {$user->first_name}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Telefon</h6>
            </div>
            <div class="col-8">
                {$user->phone_number}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>CNP</h6>
            </div>
            <div class="col-8">
                {$user->cnp}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Serie CI</h6>
            </div>
            <div class="col-8">
                {$user->ci_series}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Numar CI</h6>
            </div>
            <div class="col-8">
                {$user->ci_number}
            </div>
        </div>
    </div>

    <h4 class="pb-3 mt-4 mb-0">
        Drepturi
    </h4>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            {foreach from=$roles item=role}
                <td>{$role.name}</td>
            {/foreach}
        </tr>
        </thead>

        <tr>
            {foreach from=$roles item=role}
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="{$role.key}" name="roles[{$role.key}]"
                               class="custom-control-input" value="{$role.id}"
                               {if isset($userRoles[$role.key])}checked{/if} disabled>
                        <label class="custom-control-label" for="{$role.key}">
                            <i class="fas fa-info-circle" data-toggle="tooltip" title="{$role.description}"></i>
                        </label>
                    </div>
                </td>
            {/foreach}
        </tr>
    </table>
</div>
