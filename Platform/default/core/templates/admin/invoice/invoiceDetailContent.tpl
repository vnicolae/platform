<link rel="stylesheet" type="text/css" href="http://{$CONF.sitepath}lib/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="http://{$CONF.sitepath}lib/index.css">

<div class="form-group row header-invoice font-weight-bold font-size-12 position-relative">
    <div class="col-sm-5">
        <table class="table table-borderless table-paddingless m-0">
            <tbody>
            <tr>
                <td width="50%">Denumire furnizor</td>
                <td width="50%">{$business->name}</td>
            </tr>
            <tr>
                <td>Cod unic de identificare</td>
                <td>{$business->unique_registration_code}</td>
            </tr>
            <tr>
                <td>Numar registru comert</td>
                <td>{$business->trade_register_code}</td>
            </tr>
            <tr>
                <td>Capital social</td>
                <td>{$business->social_capital}</td>
            </tr>
            <tr>
                <td>Sediu central</td>
                <td>{$business->address}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-sm-5 text-right float-right position-absolute right-0 top-0">
        <table class="table table-borderless table-paddingless m-0">
            {if $order->order_json->person->type_person == 'individualPerson'}
                <tr>
                    <td width="50%">Nume complet</td>
                    <td width="50%">{$order->order_json->person->full_name}</td>
                </tr>
                <tr>
                    <td>Telefon</td>
                    <td>{$order->order_json->person->phone_number}</td>
                </tr>
                <tr>
                    <td>Adresa</td>
                    <td>{$order->order_json->person->address}</td>
                </tr>
            {elseif $order->order_json->person->type_person == 'legalPerson'}
                <tr>
                    <td width="50%">Denumire companie</td>
                    <td width="50%">{$order->order_json->person->full_name}</td>
                </tr>
                <tr>
                    <td>Cod unic de identificare</td>
                    <td>{$order->order_json->person->unique_registration_code}</td>
                </tr>
                <tr>
                    <td>Sediu central</td>
                    <td>{$order->order_json->person->address}</td>
                </tr>
            {/if}
        </table>
    </div>
</div>

<div class="form-group row text-center font-weight-bold font-size-18">
    <div class="col-sm-12">
        FACTURA FISCALA
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4 m-auto">
        <table class="table table-borderless table-paddingless font-size-12 font-weight-bold m-0">
            <tr>
                <td width="60%">Seria</td>
                <td width="40%">{$invoice->series_value}</td>
            </tr>
            <tr>
                <td width="60%">Nr. factura</td>
                <td width="40%">{$invoice->number}</td>
            </tr>
            <tr>
                <td width="60%">Data (zi.luna.an)</td>
                <td width="40%">{$invoice->release_date|date_format:"%d.%m.%Y"}</td>
            </tr>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4 m-auto">
        <table class="table table-borderless table-paddingless font-size-12 font-weight-bold m-0">
            <tr>
                <td width="60%">Data scadenta (zi.luna.an)</td>
                <td width="40%">{$invoice->due_date|date_format:"%d.%m.%Y"}</td>
            </tr>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-12">
        <table id="table-data" class="table table-padding-1 table-bordered font-size-12">
            <tr>
                <th width="5%" class="text-center align-middle">
                    Nr.<br>crt.
                </th>
                <th width="35%" class="text-center align-middle">
                    Denumire produs
                </th>
                <th width="10%" class="text-center align-middle">
                    U.M.
                </th>
                <th width="10%" class="text-center align-middle">
                    Cant.
                </th>
                <th width="10%" class="text-center align-middle">
                    Pret unitar<br>(fara TVA)<br>-<span class="valuta_pret_unitar">Lei</span>-
                </th>
                <th width="10%" class="text-center align-middle">
                    Cota TVA<br><br>-<span class="valuta_pret_unitar">Lei</span>-
                </th>
                <th width="10%" class="text-center align-middle">
                    Valoarea<br><br>-<span class="valuta_pret_unitar">Lei</span>-
                </th>
                <th width="10%" class="text-center align-middle">
                    TVA<br><br>-<span class="valuta_pret_unitar">Lei</span>-
                </th>
            </tr>

            <tr class="bg-light font-weight-bold">
                <td class="text-center align-middle">0</td>
                <td class="text-center align-middle">1</td>
                <td class="text-center align-middle">2</td>
                <td class="text-center align-middle">3</td>
                <td class="text-center align-middle">4</td>
                <td class="text-center align-middle">5</td>
                <td class="text-center align-middle">6</td>
                <td class="text-center align-middle">7</td>
            </tr>

            {assign var="curNumber" value=1}
            {assign var="subTotalWithoutVAT" value=0}
            {assign var="subTotalVAT" value=0}
            {foreach from=$order->order_json->cart->products item=product}
                <tr>
                    <td class="text-center align-middle font-weight-bold">
                        {$curNumber++}
                    </td>
                    <td class="align-middle">
                        {$product->name}
                    </td>
                    <td class="text-center align-middle">
                        buc
                    </td>
                    <td class="text-center align-middle">
                        {$product->quantity}
                    </td>
                    <td class="text-center align-middle">
                        {assign var="priceWithoutVAT" value=(($product->price * 100) / ($product->vat->value + 100))}
                        {$priceWithoutVAT|string_format: "%.2f"}
                    </td>
                    <td class="text-center align-middle">
                        {$product->vat->value|string_format: "%.2f"}
                    </td>
                    <td class="text-center align-middle">
                        {assign var="productWithoutVAT" value=($product->quantity * $priceWithoutVAT)}
                        {$subTotalWithoutVAT = $subTotalWithoutVAT + $productWithoutVAT}
                        {$productWithoutVAT|string_format: "%.2f"}
                    </td>
                    <td class="text-center align-middle">
                        {assign var="priceVAT" value=($product->quantity * (($priceWithoutVAT * $product->vat->value) / 100))}
                        {$subTotalVAT = $subTotalVAT + $priceVAT}
                        {$priceVAT|string_format: "%.2f"}
                    </td>
                </tr>
            {/foreach}

            {if $order->order_json->delivery->type_delivery == 'courier'}
                <tr>
                    <td class="text-center align-middle font-weight-bold">
                        {$curNumber++}
                    </td>
                    <td class="align-middle">
                        Transport curier
                    </td>
                    <td class="text-center align-middle">
                        buc
                    </td>
                    <td class="text-center align-middle">
                        1
                    </td>
                    <td class="text-center align-middle">
                        {assign var="priceWithoutVAT" value=(($order->order_json->cart->deliveryPrice * 100) / (19.00 + 100))}
                        {$priceWithoutVAT|string_format: "%.2f"}
                    </td>
                    <td class="text-center align-middle">
                        19.00
                    </td>
                    <td class="text-center align-middle">
                        {$subTotalWithoutVAT = $subTotalWithoutVAT + $priceWithoutVAT}
                        {$priceWithoutVAT|string_format: "%.2f"}
                    </td>
                    <td class="text-center align-middle">
                        {assign var="priceVAT" value=(($priceWithoutVAT * $product->vat->value) / 100)}
                        {$subTotalVAT = $subTotalVAT + $priceVAT}
                        {$priceVAT|string_format: "%.2f"}
                    </td>
                </tr>
            {/if}

            <tr>
                <td></td>
                <td><br><br><br><br><br><br><br><br><br><br>
                    <div class="issued_by">
                        Intocmit de: {$user->last_name} {$user->first_name}<br>
                        CNP: {$user->cnp}
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="p-0" colspan="4" rowspan="2">
                    <table cellpadding="2" cellspacing="0" width="100%" class="invoice_table_footer"
                           style="border: 0px;">
                        <tbody>
                        <tr>
                            <td valign="top" width="40%">
                                Semnatura si stampila furnizorului
                            </td>
                            <td>
                                Numele delegatului:
                                {$order->order_json->client->last_name} {$order->order_json->client->first_name}
                                <br>
                                Act delegat: B.I./C.I. seria ___ nr. ________
                                <br>
                                <br>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td class="text-right align-middle font-weight-bold" colspan="2">TOTAL</td>
                <td class="text-center align-middle font-weight-bold">
                    {$subTotalWithoutVAT|string_format: "%.2f"}
                </td>
                <td class="text-center align-middle font-weight-bold">
                    {$subTotalVAT|string_format: "%.2f"}
                </td>
            </tr>
            <tr>
                <td class="text-right align-middle font-weight-bold" colspan="2">TOTAL GENERAL</td>
                <td class="text-center align-middle font-weight-bold" colspan="2">
                    {$order->order_json->cart->totalPrice|string_format: "%.2f"}
                </td>
            </tr>
        </table>
        <div class="font-size-12">
            Conform art. 319 alin. (29) din Legea nr. 227/2015 privind Codul Fiscal, factura este valabila
            fara semnatura si stampila.
        </div>
    </div>
</div>
