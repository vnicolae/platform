<?php

function listBusiness()
{
    global $db;
    global $template;

    $sql = "SELECT * FROM business";
    $businesses = $db->select($sql);

    if(isset($_SESSION['message'])) {
        $succesMessage = $_SESSION['message'];
        $template->assign('succesMessage', $succesMessage);
        unset($_SESSION['message']);
    }

    $template->assign('businesses', $businesses);

    return $template->fetch('admin/listBusiness.tpl');
}
