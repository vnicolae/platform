<div class="border rounded mb-3">
    <div class="img-hover overflow-hidden height-250">
        <a href="/product/{$product.url}" title="">
            <img src="/images/product/{$product.image|default:'default-image.jpg'}" class="img-fluid">
        </a>
    </div>

    <div class="text-center">
        <a href="/product/{$product.url}" class="nav-link text-dark">
            <h6 class="pl-2 pr-2 mb-0 overflow-hidden height-40 max-height-40">{$product.name}</h6>
        </a>
        {if $product.is_discount}
            <h6><del>{$product.price} Lei</del></h6>
            <h5>{$product.new_price} Lei</h5>
        {else}
            <h6>&nbsp;</h6>
            <h5>{$product.price} Lei</h5>
        {/if}
    </div>
</div>
