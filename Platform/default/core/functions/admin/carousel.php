<?php

function carousel()
{
    global $db;
    global $CONF;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $sql = "SELECT * FROM image_carousel";
            $imagesCarousel = $db->select($sql);

            $template->assign('imagesCarousel', $imagesCarousel);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }
            if (isset($_SESSION['message-error'])) {
                $successMessageError = $_SESSION['message-error'];
                $template->assign('successMessageError', $successMessageError);
                unset($_SESSION['message-error']);
            }

            return $template->fetch('admin/carousel/carouselList.tpl');
        }

        if ($link[3] === 'add') {
            if (isset($_POST['add_carousel'])) {
                if (!$_FILES['image']['name']) {
                    $error = 'Va rugam sa atasati un fisier de tipul imagine!';

                    $template->assign('CONF', $CONF);
                    $template->assign('error', $error);

                    return $template->fetch('admin/carousel/carouselAdd.tpl');
                }

                $fileName = $_FILES['image']['name'];
                $explodeFileName = explode('.', $fileName);

                $file_ext = strtolower(end($explodeFileName));

                $extensions = array("jpeg", "jpg", "png");
                if (in_array($file_ext, $extensions) === false) {
                    $error = 'Fisierul atasat nu este de tipul imagine!';

                    $template->assign('CONF', $CONF);
                    $template->assign('error', $error);

                    return $template->fetch('admin/carousel/carouselAdd.tpl');
                }

                move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    $CONF['serverpath'] . "images/carousel/" . $fileName
                );

                $active = 1;
                if (!isset($_POST['active'])) {
                    $active = 0;
                }

                $sql = "INSERT INTO image_carousel (name, title, sub_title, link, active) VALUES (
                        '" . $db->escape_string($fileName) . "',
                        '" . $db->escape_string($_POST['title']) . "',
                        '" . $db->escape_string($_POST['sub_title']) . "',
                        '" . $db->escape_string($_POST['link']) . "',
                        '" . intval($active) . "')";
                $db->query($sql);

                $_SESSION['message'] = 'Imaginea a fost adaugata';

                header("Location: /admin/carousel/list");
            }

            $template->assign('CONF', $CONF);

            return $template->fetch('admin/carousel/carouselAdd.tpl');
        }

        if ($link[3] === 'edit') {
            if (isset($link[4]) && intval($link[4])) {
                if (isset($_POST['edit_carousel'])) {
                    $queryImageName = '';

                    if ($_FILES['image']['name']) {
                        $fileName = $_FILES['image']['name'];
                        $explodeFileName = explode('.', $fileName);

                        $file_ext = strtolower(end($explodeFileName));

                        $extensions = array("jpeg", "jpg", "png");
                        if (in_array($file_ext, $extensions) === false) {
                            $error = 'Va rugam sa atasati un fisier de tipul imagine!';

                            $sql = "SELECT * FROM image_carousel WHERE id =  '" . intval($link[4]) . "'";
                            $imageCarousel = $db->query($sql)->fetch_object();

                            $template->assign('imageCarousel', $imageCarousel);
                            $template->assign('error', $error);

                            return $template->fetch('admin/carousel/carouselEdit.tpl');
                        }

                        move_uploaded_file(
                            $_FILES['image']['tmp_name'],
                            $CONF['serverpath'] . "images/carousel/" . $fileName
                        );

                        $queryImageName = "name = '" . $db->escape_string($fileName) . "',";
                    }

                    $active = 1;
                    if (!isset($_POST['active'])) {
                        $active = 0;
                    }

                    $sql = "UPDATE image_carousel SET 
                            $queryImageName
                            title = '" . $db->escape_string($_POST['title']) . "',
                            sub_title = '" . $db->escape_string($_POST['sub_title']) . "',
                            link = '" . $db->escape_string($_POST['link']) . "',
                            active = '" . intval($active) . "'
                            WHERE id = '" . intval($link[4]) . "'";
                    $db->query($sql);

                    $_SESSION['message'] = 'Imaginea a fost editata';

                    header("Location: /admin/carousel/list");
                }

                $sql = "SELECT * FROM image_carousel WHERE id =  '" . intval($link[4]) . "'";
                $imageCarousel = $db->query($sql)->fetch_object();

                if (empty($imageCarousel)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $template->assign('imageCarousel', $imageCarousel);

                return $template->fetch('admin/carousel/carouselEdit.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'enable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE image_carousel SET active = 1 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 0";
                $db->query($sql);

                $_SESSION['message'] = 'Imaginea a fost activata';

                header("Location: /admin/carousel/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'disable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE image_carousel SET active = 0 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 1";
                $db->query($sql);

                $_SESSION['message'] = 'Imaginea a fost dezactivata';

                header("Location: /admin/carousel/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
