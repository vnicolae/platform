<?php

use Dompdf\Dompdf;

function invoice()
{
    global $db;
    global $CONF;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'add') {
            if (isset($link[4]) && intval($link[4])) {
                if (isset($_POST['add_invoice'])) {
                    $sql = "INSERT INTO invoice (series_id, number, release_date, due_date, user_id, order_id) VALUES (
                            '" . $db->escape_string($_POST['series_invoice']) . "',
                            '" . $db->escape_string($_POST['number_invoice']) . "',
                            '" . date('Y-m-d', strtotime($_POST['release_date'])) . "',
                            '" . date('Y-m-d', strtotime($_POST['due_date'])) . "',
                            '" . intval($_SESSION['admin']['user_id']) . "',
                            '" . intval($link[4]) . "')";
                    $invoiceId = $db->query($sql, true);

                    $sql = "UPDATE `order` SET active = 2
                            WHERE active = 1 AND id = '" . intval($link[4]) . "'";
                    $db->query($sql);

                    $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                            JOIN series_invoice si ON i.series_id = si.id
                            WHERE i.id = '" . intval($invoiceId) . "'";
                    $invoice = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM `order` WHERE id = '" . intval($invoice->order_id) . "'";
                    $order = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM user WHERE id =  '" . intval($invoice->user_id) . "'";
                    $user = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM business WHERE id =  '" . intval($user->business_id) . "'";
                    $business = $db->query($sql)->fetch_object();

                    if (empty($invoice) || empty($order) || empty($user) || empty($business)) {
                        return $template->fetch('admin/somethingWentWrong.tpl');
                    }

                    $order->order_json = json_decode($order->order_json);

                    $template->assign('order', $order);
                    $template->assign('invoice', $invoice);
                    $template->assign('user', $user);
                    $template->assign('business', $business);
                    $template->assign('CONF', $CONF);

                    $html = $template->fetch('admin/invoice/invoiceDetailContent.tpl');

                    $dompdf = new Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4', 'portrait');
                    $dompdf->render();

                    $pdf_name = "Factura-{$invoice->series_value}-{$invoice->number}.pdf";

                    // Output the generated PDF to Browser
                    // $dompdf->stream($pdf_name);

                    $pdf = $dompdf->output();
                    $file_location = $CONF['serverpath'] . "lib/file/invoice/".$pdf_name;
                    file_put_contents($file_location, $pdf);

                    $_SESSION['message'] = 'Factura a fost creata';

                    header("Location: /admin/invoice/list");
                    exit;
                }

                $sql = "SELECT * FROM `order`
                        WHERE id =  '" . intval($link[4]) . "' AND active = 1";
                $order = $db->query($sql)->fetch_object();

                if (empty($order)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT i.id FROM invoice i
                        WHERE i.active = 1 AND i.order_id = {$order->id}";
                $order->invoice = $db->query($sql)->fetch_assoc();

                $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                $business = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM user WHERE id =  '" . intval($_SESSION['admin']['user_id']) . "'";
                $user = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM series_invoice WHERE active = 1";
                $seriesInvoices = $db->select($sql);

                if (empty($business) || empty($user)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $order->order_json = json_decode($order->order_json);

                $template->assign('order', $order);
                $template->assign('business', $business);
                $template->assign('user', $user);
                $template->assign('seriesInvoices', $seriesInvoices);

                return $template->fetch('admin/invoice/invoiceAdd.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'view') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                        JOIN series_invoice si ON i.series_id = si.id
                        WHERE i.id = '" . intval($link[4]) . "'";
                $invoice = $db->query($sql)->fetch_object();

                if (empty($invoice)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT * FROM `order` WHERE id = '" . intval($invoice->order_id) . "'";
                $order = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM user WHERE id =  '" . intval($invoice->user_id) . "'";
                $user = $db->query($sql)->fetch_object();

                if (empty($user)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT * FROM business WHERE id =  '" . intval($user->business_id) . "'";
                $business = $db->query($sql)->fetch_object();

                $sql = "SELECT r.id, r.number, sr.value as 'series_value'
                        FROM receipt r
                        JOIN series_receipt sr ON r.series_id = sr.id
                        WHERE r.active = 1 AND r.invoice_id = {$invoice->id}";
                $invoice->receipt = $db->query($sql)->fetch_object();

                if (empty($order) || empty($business)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $order->order_json = json_decode($order->order_json);

                if ($invoice->reference_id !== 0) {
                    $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                        JOIN series_invoice si ON i.series_id = si.id
                        WHERE i.id = '" . intval($invoice->reference_id) . "'";
                    $invoiceCancel = $db->query($sql)->fetch_object();

                    $template->assign('invoiceCancel', $invoiceCancel);
                }

                $template->assign('invoice', $invoice);
                $template->assign('order', $order);
                $template->assign('user', $user);
                $template->assign('business', $business);
                $template->assign('CONF', $CONF);

                return $template->fetch('admin/invoice/invoiceDetail.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_invoice'])) {
                if ($_POST['series']) {
                    $where .= " AND i.series_id = '" . intval($_POST['series']) . "'";
                }
                if ($_POST['number']) {
                    $where .= " AND i.number = '" . intval($_POST['number']) . "'";
                }
                if ($_POST['last_name']) {
                    $where .= " AND c.last_name LIKE '%" . $db->escape_string($_POST['last_name']) . "%'";
                }
                if ($_POST['first_name']) {
                    $where .= " AND c.first_name LIKE '%" . $db->escape_string($_POST['first_name']) . "%'";
                }
                if ($_POST['order']) {
                    $where .= " AND i.order_id = '" . intval($_POST['order']) . "'";
                }
                if ($_POST['user']) {
                    $where .= " AND i.user_id = '" . intval($_POST['user']) . "'";
                }
                if ($_POST['invoice_date']) {
                    $where .= " AND i.release_date >= '" . date('Y-m-d', strtotime($_POST['invoice_date'])) . "'";
                    $where .= " AND i.release_date < '" . date('Y-m-d', strtotime($_POST['invoice_date'] . ' +1 day')) . "'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND i.active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND i.active = '0'";
                    }
                }
            }

            $sql = "SELECT * FROM user";
            $users = $db->select($sql);

            $sql = "SELECT * FROM series_invoice";
            $seriesInvoice = $db->select($sql);

            $sql = "SELECT i.*, c.first_name, c.last_name, o.total_price, o.client_id, si.value as 'series_value'
                    FROM invoice i
                    JOIN series_invoice si ON i.series_id = si.id
                    JOIN `order` o ON i.order_id = o.id
                    JOIN client c ON o.client_id = c.id
                    {$where}
                    ORDER BY i.timestamp DESC";
            $invoices = $db->select($sql);

            foreach ($invoices as $key => $invoice) {
                $sql = "SELECT r.id, r.number, sr.value as 'series_value'
                        FROM receipt r
                        JOIN series_receipt sr ON r.series_id = sr.id
                        WHERE r.active = 1 AND r.invoice_id = {$invoice['id']}";
                $invoices[$key]['receipt'] = $db->query($sql)->fetch_assoc();
            }

            $template->assign('invoices', $invoices);
            $template->assign('users', $users);
            $template->assign('seriesInvoice', $seriesInvoice);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/invoice/invoiceList.tpl');
        }

        if ($link[3] === 'cancel') {
            if (isset($link[4]) && intval($link[4])) {
                if (isset($_POST['cancel_invoice'])) {
                    $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                            JOIN series_invoice si ON i.series_id = si.id
                            WHERE i.id = '" . intval($link[4]) . "' AND i.active = 1";
                    $invoiceCancel = $db->query($sql)->fetch_object();

                    $sql = "INSERT INTO invoice (series_id, number, release_date, due_date, user_id, order_id, reference_id, active) VALUES (
                            '" . $db->escape_string($_POST['series_invoice']) . "',
                            '" . $db->escape_string($_POST['number_invoice']) . "',
                            '" . date('Y-m-d', strtotime($_POST['release_date'])) . "',
                            '" . date('Y-m-d', strtotime('now')) . "',
                            '" . intval($_SESSION['admin']['user_id']) . "',
                            '" . intval($invoiceCancel->order_id) . "',
                            '" . intval($link[4]) . "',
                            0)";
                    $invoiceId = $db->query($sql, true);

                    $sql = "UPDATE invoice SET
                            cancel_id = '" . intval($invoiceId) . "', 
                            active = 0
                            WHERE id = '" . intval($link[4]) . "' AND active = 1";
                    $db->query($sql);

                    $sql = "UPDATE receipt SET
                            active = 0
                            WHERE invoice_id = '" . intval($link[4]) . "' AND active = 1";
                    $db->query($sql);

                    $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                            JOIN series_invoice si ON i.series_id = si.id
                            WHERE i.id = '" . intval($invoiceId) . "'";
                    $invoice = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM `order` WHERE id = '" . intval($invoice->order_id) . "'";
                    $order = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM user WHERE id =  '" . intval($invoice->user_id) . "'";
                    $user = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM business WHERE id =  '" . intval($user->business_id) . "'";
                    $business = $db->query($sql)->fetch_object();

                    if (empty($invoiceCancel) || empty($invoice) || empty($order) || empty($user) || empty($business)) {
                        return $template->fetch('admin/somethingWentWrong.tpl');
                    }

                    $order->order_json = json_decode($order->order_json);

                    foreach ($order->order_json->cart->products as $product) {
                        $sql = "UPDATE product SET
                            stock = stock + " . intval($product->quantity) . "
                            WHERE id = '" . intval($product->id) . "'";
                        $db->query($sql);
                    }

                    $template->assign('invoiceCancel', $invoiceCancel);
                    $template->assign('invoice', $invoice);
                    $template->assign('order', $order);
                    $template->assign('user', $user);
                    $template->assign('business', $business);
                    $template->assign('CONF', $CONF);

                    $html = $template->fetch('admin/invoice/invoiceCancelDetailContent.tpl');

                    $dompdf = new Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4', 'portrait');
                    $dompdf->render();

                    $pdf_name = "Factura-{$invoice->series_value}-{$invoice->number}.pdf";

                    // Output the generated PDF to Browser
                    // $dompdf->stream($pdf_name);

                    $pdf = $dompdf->output();
                    $file_location = $CONF['serverpath'] . "lib/file/invoice/" . $pdf_name;
                    file_put_contents($file_location, $pdf);

                    $_SESSION['message'] = 'Factura a fost stornata';

                    header("Location: /admin/invoice/list");
                    exit;
                }

                $sql = "SELECT i.*, si.value AS 'series_value' FROM invoice i
                        JOIN series_invoice si ON i.series_id = si.id
                        WHERE i.id =  '" . intval($link[4]) . "' AND i.active = 1";
                $invoice = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM `order`
                        WHERE id =  '" . intval($invoice->order_id) . "' AND active = 2";
                $order = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                $business = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM user WHERE id =  '" . intval($_SESSION['admin']['user_id']) . "'";
                $user = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM series_invoice WHERE active = 1";
                $seriesInvoices = $db->select($sql);

                if (empty($invoice) || empty($order) || empty($business) || empty($user)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $order->order_json = json_decode($order->order_json);

                $template->assign('invoice', $invoice);
                $template->assign('order', $order);
                $template->assign('business', $business);
                $template->assign('user', $user);
                $template->assign('seriesInvoices', $seriesInvoices);

                return $template->fetch('admin/invoice/invoiceCancel.tpl');

            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
