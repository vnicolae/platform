<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Editeaza datele companiei
    </h4>

    {if isset($error)}
        <div class="alert alert-danger" role="alert">
            {$error}
        </div>
    {/if}

    <div class="mt-4">
        <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="address">
                    Adresa completa <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="address" name="address"
                           value="{$business->address}" required>
                    <div class="invalid-feedback">Introduceti un Cod unic de identificare valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="unique_registration_code">
                    Cod unic identificare <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="unique_registration_code" name="unique_registration_code"
                           value="{$business->unique_registration_code}" required>
                    <div class="invalid-feedback">Introduceti un Cod unic de identificare valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="trade_register_code">
                    Numar registru comert <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="trade_register_code" name="trade_register_code"
                           value="{$business->trade_register_code}" required>
                    <div class="invalid-feedback">Introduceti un Numar registru comert valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="social_capital">
                    Capital social <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="social_capital" name="social_capital"
                           value="{$business->social_capital}" required>
                    <div class="invalid-feedback">Introduceti un Capital social valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="bank_name">
                    Nume banca
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="bank_name" name="bank_name"
                           value="{$business->bank_name}">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="iban">
                    IBAN
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="iban" name="iban"
                           value="{$business->iban}">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="logo">
                    Logo site
                    <br><strong>250x50</strong>
                </label>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="logo" name="logo"
                               data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                               accept="image/*" onchange="loadFile(event)">
                        <label class="custom-file-label" for="logo">
                            {if $business->logo_site}{$business->logo_site}{else}Alege imagine{/if}
                        </label>
                    </div>
                    <img {if $business->logo_site}src="/images/{$business->logo_site}"{/if}
                         id="preview_carousel" style="max-width: 100%;">
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    {if isset($smarty.session.admin.roles.access_settings)}
                        <button type="submit" class="btn btn-primary btn-custom" name="edit_business">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/settings/company" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
