<?php

function settings()
{
    global $db;
    global $CONF;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'company') {
            if (isset($link[4]) && $link[4]) {
                if ($link[4] === 'edit') {
                    if (isset($_POST['edit_business'])) {
                        $queryImageName = '';
                        if ($_FILES['logo']['name']) {
                            $fileName = $_FILES['logo']['name'];
                            $explodeFileName = explode('.', $fileName);

                            $file_ext = strtolower(end($explodeFileName));

                            $extensions = array("jpeg", "jpg", "png");
                            if (in_array($file_ext, $extensions) === false) {
                                $error = 'Va rugam sa atasati un fisier de tipul imagine!';

                                $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                                $business = $db->query($sql)->fetch_object();

                                $template->assign('business', $business);
                                $template->assign('error', $error);

                                return $template->fetch('admin/settings/company/companyEdit.tpl');
                            }

                            file_get_contents(
                                $_FILES['logo']['tmp_name'],
                                $CONF['serverpath'] . "images/" . $fileName
                            );

                            copy(
                                $CONF['serverpath'] . "images/" . $fileName,
                                $CONF['serverpath'] . "images/admin/" . $fileName
                            );

                            $queryImageName = "logo_site = '" . $db->escape_string($fileName) . "',";
                        }

                        $sql = "UPDATE business SET 
                                $queryImageName
                                address = '" . $db->escape_string($_POST['address']) . "',
                                unique_registration_code = '" . $db->escape_string($_POST['unique_registration_code']) . "',
                                trade_register_code = '" . $db->escape_string($_POST['trade_register_code']) . "',
                                social_capital = '" . $db->escape_string($_POST['social_capital']) . "',
                                bank_name = '" . $db->escape_string($_POST['bank_name']) . "',
                                iban = '" . $db->escape_string($_POST['iban']) . "'
                                WHERE id = '" . intval($_SESSION['admin']['business_id']) . "' AND active = 1";
                        $db->query($sql);

                        $_SESSION['message'] = 'Datele companiei au fost actualizate';

                        header("Location: /admin/settings/company");
                    }

                    $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                    $business = $db->query($sql)->fetch_object();

                    if (empty($business)) {
                        return $template->fetch('admin/somethingWentWrong.tpl');
                    }

                    $template->assign('business', $business);

                    return $template->fetch('admin/settings/company/companyEdit.tpl');
                }

                return $template->fetch('admin/404.tpl');
            }

            $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
            $business = $db->query($sql)->fetch_object();

            if (empty($business)) {
                return $template->fetch('admin/somethingWentWrong.tpl');
            }

            $template->assign('business', $business);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/settings/company/companyDetail.tpl');
        }

        if ($link[3] === 'series') {
            if (isset($link[4]) && $link[4]) {
                // link.4 == add-invoice // series for invoices
                if ($link[4] === 'add-invoice') {
                    if (isset($_POST['add_series_invoice'])) {
                        $sql = "INSERT INTO series_invoice 
                                (name, value) VALUES (
                                '" . $db->escape_string($_POST['name']) . "',
                                '" . $db->escape_string($_POST['value']) . "')";
                        $db->query($sql);

                        $_SESSION['message-invoice'] = 'Seria pentru factura a fost adaugata';

                        header("Location: /admin/settings/series");
                    }

                    return $template->fetch('admin/settings/series/seriesInvoiceAdd.tpl');
                }

                if ($link[4] === 'edit-invoice') {
                    if (isset($link[5]) && intval($link[5])) {
                        if (isset($_POST['edit_series_invoice'])) {
                            $sql = "UPDATE series_invoice SET 
                                    name = '" . $db->escape_string($_POST['name']) . "',
                                    value = '" . $db->escape_string($_POST['value']) . "'
                                    WHERE id = '" . intval($link[5]) . "' AND active = 1";
                            $db->query($sql);

                            $_SESSION['message-invoice'] = 'Seria pentru factura a fost editata';

                            header("Location: /admin/settings/series");
                        }

                        $sql = "SELECT * FROM series_invoice WHERE id =  '" . intval($link[5]) . "' AND active = 1";
                        $seriesInvoice = $db->query($sql)->fetch_object();

                        if (empty($seriesInvoice)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $template->assign('seriesInvoice', $seriesInvoice);

                        return $template->fetch('admin/settings/series/seriesInvoiceEdit.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'enable-invoice') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE series_invoice SET active = 1 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 0";
                        $db->query($sql);

                        $_SESSION['message-invoice'] = 'Seria pentru factura a fost activata';

                        header("Location: /admin/settings/series");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'disable-invoice') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE series_invoice SET active = 0 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 1";
                        $db->query($sql);

                        $_SESSION['message-invoice'] = 'Seria pentru factura a fost dezactivata';

                        header("Location: /admin/settings/series");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                // link.4 == add-receipt // series for receipts
                if ($link[4] === 'add-receipt') {
                    if (isset($_POST['add_series_receipt'])) {
                        $sql = "INSERT INTO series_receipt 
                                    (name, value) VALUES (
                                    '" . $db->escape_string($_POST['name']) . "',
                                    '" . $db->escape_string($_POST['value']) . "')";
                        $db->query($sql);

                        $_SESSION['message-receipt'] = 'Seria pentru chitanta a fost adaugata';

                        header("Location: /admin/settings/series");
                    }

                    return $template->fetch('admin/settings/series/seriesReceiptAdd.tpl');
                }

                if ($link[4] === 'edit-receipt') {
                    if (isset($link[5]) && intval($link[5])) {
                        if (isset($_POST['edit_series_receipt'])) {
                            $sql = "UPDATE series_receipt SET 
                                    name = '" . $db->escape_string($_POST['name']) . "',
                                    value = '" . $db->escape_string($_POST['value']) . "'
                                    WHERE id = '" . intval($link[5]) . "' AND active = 1";
                            $db->query($sql);

                            $_SESSION['message-receipt'] = 'Seria pentru chitanta a fost editata';

                            header("Location: /admin/settings/series");
                        }

                        $sql = "SELECT * FROM series_receipt WHERE id =  '" . intval($link[5]) . "' AND active = 1";
                        $seriesReceipt = $db->query($sql)->fetch_object();

                        if (empty($seriesReceipt)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $template->assign('seriesReceipt', $seriesReceipt);

                        return $template->fetch('admin/settings/series/seriesReceiptEdit.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'enable-receipt') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE series_receipt SET active = 1 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 0";
                        $db->query($sql);

                        $_SESSION['message-receipt'] = 'Seria pentru chitanta a fost activata';

                        header("Location: /admin/settings/series");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'disable-receipt') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE series_receipt SET active = 0 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 1";
                        $db->query($sql);

                        $_SESSION['message-receipt'] = 'Seria pentru chitanta a fost dezactivata';

                        header("Location: /admin/settings/series");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                return $template->fetch('admin/404.tpl');
            }

            $sql = "SELECT * FROM series_invoice";
            $seriesInvoices = $db->select($sql);

            $sql = "SELECT * FROM series_receipt";
            $seriesReceipts = $db->select($sql);

            $template->assign('seriesInvoices', $seriesInvoices);
            $template->assign('seriesReceipts', $seriesReceipts);

            if (isset($_SESSION['message-invoice'])) {
                $successMessageInvoice = $_SESSION['message-invoice'];
                $template->assign('successMessageInvoice', $successMessageInvoice);
                unset($_SESSION['message-invoice']);
            }
            if (isset($_SESSION['message-receipt'])) {
                $successMessageReceipt = $_SESSION['message-receipt'];
                $template->assign('successMessageReceipt', $successMessageReceipt);
                unset($_SESSION['message-receipt']);
            }

            return $template->fetch('admin/settings/series/seriesList.tpl');
        }

        if ($link[3] === 'user') {
            if (isset($link[4]) && $link[4]) {
                if ($link[4] === 'add') {
                    if (isset($_POST['add_user'])) {
                        $sql = "INSERT INTO user 
                                (email, password, first_name, last_name, phone_number, cnp, ci_series, ci_number, business_id, type) 
                                VALUES (
                                    '" . $db->escape_string($_POST['email']) . "',
                                    '" . md5($db->escape_string($_POST['password'])) . "',
                                    '" . $db->escape_string($_POST['first_name']) . "',
                                    '" . $db->escape_string($_POST['last_name']) . "',
                                    '" . $db->escape_string($_POST['phone_number']) . "',
                                    '" . $db->escape_string($_POST['cnp']) . "',
                                    '" . $db->escape_string($_POST['ci_series']) . "',
                                    '" . $db->escape_string($_POST['ci_number']) . "',
                                    '" . intval($_SESSION['admin']['business_id']) . "',
                                    '10'
                                )";
                        $userId = $db->query($sql, true);

                        foreach ($_POST['roles'] as $key=>$role) {
                            $sql = "INSERT INTO user_role(user_id, role_id) 
                                        VALUES ('" . intval($userId) . "', '" . intval($role) . "')";
                            $db->query($sql);
                        }

                        $_SESSION['message'] = 'Utilizatorul a fost adaugat';

                        header("Location: /admin/settings/user");
                    }

                    $sql = "SELECT * FROM role WHERE active = 1";
                    $roles = $db->select($sql);

                    $template->assign('roles', $roles);

                    return $template->fetch('admin/settings/user/userAdd.tpl');
                }

                if ($link[4] === 'edit') {
                    if (isset($link[5]) && intval($link[5])) {
                        if (isset($_POST['edit_user'])) {
                            $sql = "UPDATE user SET 
                                    first_name = '" . $db->escape_string($_POST['first_name']) . "',
                                    last_name = '" . $db->escape_string($_POST['last_name']) . "',
                                    phone_number = '" . $db->escape_string($_POST['phone_number']) . "',
                                    cnp = '" . $db->escape_string($_POST['cnp']) . "',
                                    ci_series = '" . $db->escape_string($_POST['ci_series']) . "',
                                    ci_number = '" . $db->escape_string($_POST['ci_number']) . "'
                                    WHERE id = '" . intval($link[5]) . "' AND active = 1 AND 
                                    business_id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                            $db->query($sql);

                            $sql = "DELETE FROM user_role WHERE user_id = '" . intval($link[5]) . "'";
                            $db->query($sql);

                            foreach ($_POST['roles'] as $key=>$role) {
                                $sql = "INSERT INTO user_role(user_id, role_id) 
                                        VALUES ('" . intval($link[5]) . "', '" . intval($role) . "')";
                                $db->query($sql);
                            }

                            $_SESSION['message'] = 'Datele utilizatorului au fost actualizate';

                            header("Location: /admin/settings/user");
                        }

                        $sql = "SELECT * FROM user WHERE id = '" . intval($link[5]) . "' AND 
                                business_id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                        $user = $db->query($sql)->fetch_object();

                        $sql = "SELECT * FROM role WHERE active = 1";
                        $roles = $db->select($sql);

                        if (empty($user)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $template->assign('user', $user);
                        $template->assign('roles', $roles);
                        $template->assign('userRoles', getUserRoles($link[5]));

                        return $template->fetch('admin/settings/user/userEdit.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'view') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "SELECT * FROM user WHERE id = '" . intval($link[5]) . "' AND 
                                business_id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                        $user = $db->query($sql)->fetch_object();

                        if (empty($user)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $sql = "SELECT * FROM role WHERE active = 1";
                        $roles = $db->select($sql);

                        $template->assign('user', $user);
                        $template->assign('roles', $roles);
                        $template->assign('userRoles', getUserRoles($link[5]));

                        return $template->fetch('admin/settings/user/userDetail.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'enable') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE user SET active = 1 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 0 AND 
                                business_id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                        $db->query($sql);

                        $_SESSION['message'] = 'Utilizatorul a fost activat';

                        header("Location: /admin/settings/user");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'disable') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE user SET active = 0 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 1 AND 
                                business_id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                        $db->query($sql);

                        $_SESSION['message'] = 'Utilizatorul a fost dezactivat';

                        header("Location: /admin/settings/user");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                return $template->fetch('admin/404.tpl');
            }

            $where = "";
            if (isset($_POST['filter_user'])) {
                if ($_POST['last_name']) {
                    $where .= " AND last_name LIKE '%" . $db->escape_string($_POST['last_name']) . "%'";
                }
                if ($_POST['first_name']) {
                    $where .= " AND first_name LIKE '%" . $db->escape_string($_POST['first_name']) . "%'";
                }
                if ($_POST['email']) {
                    $where .= " AND email LIKE '%" . $db->escape_string($_POST['email']) . "%'";
                }
                if ($_POST['phone_number']) {
                    $where .= " AND phone_number LIKE '%" . $db->escape_string($_POST['phone_number']) . "%'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND active = '0'";
                    }
                }
            }

            $sql = "SELECT * FROM user 
                    WHERE business_id =  '" . intval($_SESSION['admin']['business_id']) . "'
                    {$where}";
            $users = $db->select($sql);

            $template->assign('users', $users);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/settings/user/userList.tpl');
        }

        if ($link[3] === 'vat') {
            if (isset($link[4]) && $link[4]) {
                if ($link[4] === 'add') {
                    if (isset($_POST['add_vat'])) {
                        $sql = "INSERT INTO vat 
                                (name, value) VALUES (
                                '" . $db->escape_string($_POST['name']) . "',
                                '" . $db->escape_string($_POST['value']) . "')";
                        $db->query($sql);

                        $_SESSION['message'] = 'Cota de TVA a fost adaugata';

                        header("Location: /admin/settings/vat");
                    }

                    return $template->fetch('admin/settings/vat/vatAdd.tpl');
                }

                if ($link[4] === 'edit') {
                    if (isset($link[5]) && intval($link[5])) {
                        if (isset($_POST['edit_vat'])) {
                            $sql = "UPDATE vat SET 
                                    name = '" . $db->escape_string($_POST['name']) . "',
                                    value = '" . $db->escape_string($_POST['value']) . "'
                                    WHERE id = '" . intval($link[5]) . "' AND active = 1";
                            $db->query($sql);

                            $_SESSION['message'] = 'Cota de TVA a fost editata';

                            header("Location: /admin/settings/vat");
                        }

                        $sql = "SELECT * FROM vat WHERE id =  '" . intval($link[5]) . "' AND active = 1";
                        $vat = $db->query($sql)->fetch_object();

                        if (empty($vat)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $template->assign('vat', $vat);

                        return $template->fetch('admin/settings/vat/vatEdit.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'enable') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE vat SET active = 1 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 0";
                        $db->query($sql);

                        $_SESSION['message'] = 'Cota de TVA a fost activata';

                        header("Location: /admin/settings/vat");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'disable') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE vat SET active = 0 WHERE 
                                id = '" . intval($link[5]) . "' AND active = 1";
                        $db->query($sql);

                        $_SESSION['message'] = 'Cota de TVA a fost dezactivata';

                        header("Location: /admin/settings/vat");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                return $template->fetch('admin/404.tpl');
            }

            $sql = "SELECT * FROM vat";
            $vats = $db->select($sql);

            $template->assign('vats', $vats);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/settings/vat/vatList.tpl');
        }
    }

    return $template->fetch('admin/404.tpl');
}
