<?php

/**
 * @param $data
 */
function print_ra($data)
{
    echo "<pre>";

    print_r($data);

    echo "</pre>";
}

/**
 * @return array
 */
function getAllParentsCategories()
{
    global $db;

    $sql = "SELECT * FROM category where category_id = 0 AND active = 1";
    $categories = $db->select($sql);

    return $categories;
}

/**
 * @return array
 */
function getAllCategories()
{
    global $db;

    $sql = "SELECT * FROM category where category_id = 0 AND active = 1";
    $categories = $db->select($sql);

    foreach ($categories as $key => $category) {
        $sql = "SELECT * FROM category where category_id = {$category['id']} AND active = 1";
        $subCategories = $db->select($sql);

        if ((count($subCategories))) {
            $categories[$key]['sub_categories'] = $subCategories;
        }
    }

    return $categories;
}

/**
 * @return array
 */
function getAllProducts()
{
    global $db;

    $sql = "SELECT * FROM product WHERE active = 1 ORDER BY timestamp DESC";
    $products = $db->select($sql);

    return $products;
}

/**
 * @param int $categoryId
 *
 * @return array
 */
function getProductsByCategory(int $categoryId)
{
    global $db;

    $sql = "SELECT * FROM product WHERE category_id IN (
                SELECT id FROM category WHERE (id = {$categoryId} OR category_id = {$categoryId}) AND active = 1
            ) AND active = 1";
    $products = $db->select($sql);

    return $products;
}

/**
 * @return array
 */
function getAllVats()
{
    global $db;

    $sql = "SELECT * FROM vat WHERE active = 1";
    $vats = $db->select($sql);

    return $vats;
}

/**
 * @return array
 */
function getImagesCarousel()
{
    global $db;

    $sql = "SELECT * FROM image_carousel WHERE active = 1";
    $imagesCarousel = $db->select($sql);

    return $imagesCarousel;
}

/**
 * @param int $userId
 *
 * @return array
 */
function getUserRoles(int $userId)
{
    global $db;

    $sql = "SELECT r.key FROM user_role ur
            JOIN user u ON u.id = ur.user_id
            JOIN role r ON r.id = ur.role_id 
            WHERE ur.user_id = '" . intval($userId) . "' AND
            business_id =  '" . intval($_SESSION['admin']['business_id']) . "'";
    $userRolesResult = $db->select($sql);

    $userRoles = array();
    foreach ($userRolesResult as $key => $userRoleResult) {
        $userRoles[$userRoleResult['key']] = 1;
    }

    return $userRoles;
}

/**
 * @return array
 */
function getLogoSite()
{
    global $db;

    $sql = "SELECT logo_site FROM business";
    $logoSite = $db->query($sql)->fetch_assoc();

    return $logoSite;
}

/**
 * @param float $number
 * @return string
 */
function getNumberInLetters(float $number)
{
    $pos = strpos($number, '.');

    if ($pos == false) {
        $lei = nrLit($number);

        return $lei . " Lei";
    } else {
        $bani = strstr($number, '.');
        $bani = ltrim($bani, '.');
        $number = strstr($number, '.', true);
        $lei = nrLit($number);

        if ($bani >= 1 && $bani <= 9 && strlen($bani) == 1) {
            $bani *= 10;
        }

        $bani = nrLit($bani);

        if ($bani != "") {
            return $lei . " lei si" . $bani . " bani";
        } else {
            return $lei . " lei";
        }
    }
}

/**
 * @param int $intreg
 * @return mixed
 */
function nrLit(int $intreg = 0)
{
    $den = "";
    $nr = str_pad($intreg, 9, 0, STR_PAD_LEFT);
    $len = strlen($nr);
    $mii = ltrim(substr($nr, $len - 3), '0');
    $sute_mii = ltrim(substr($nr, $len - 6, 3), '0');
    $milioane = ltrim(substr($nr, $len - 9, 3), '0');

    if ($milioane) {
        if (strlen($milioane) == 3) {
            $den .= getSute(substr($milioane, 0, 1));
            $milioane = $milioane % 100;
        }

        $den .= " ";

        if (substr($milioane, 0, 1) == '1' && strlen($milioane) == 2) {
            $den .= getZeci($milioane);
        } else {
            unset($v);
            if (strlen($milioane) == 2) {
                $v[] = getZeci(substr($milioane, 0, 1));
                $v[] = getCifre(substr($milioane, 1, 1));

                $den .= $v[0] . " si " . $v[1];
            } else {
                $v[] = getCifre(substr($milioane, 0, 1));

                $den .= $v[0];
            }
        }

        $den .= " milioane ";
    }

    if ($sute_mii) {
        if (strlen($sute_mii) == 3) {
            $den .= getSute(substr($sute_mii, 0, 1));
            $sute_mii = $sute_mii % 100;
        }

        $den .= " ";

        if (substr($sute_mii, 0, 1) == '1' && strlen($sute_mii) == 2) {
            $den .= getZeci($sute_mii, 1);
        } else {
            unset($v);
            if (strlen($sute_mii) == 2) {
                $v[] = getZeci(substr($sute_mii, 0, 1));
                $v[] = getCifre(substr($sute_mii, 1, 1));

                $den .= $v[0] . " si " . $v[1];
            } else {
                $v[] = getCifre(substr($sute_mii, 0, 1));

                $den .= $v[0];
            }
        }

        $den .= " mii ";
    }

    if ($mii) {
        if (strlen($mii) == 3) {
            $den .= getSute(substr($mii, 0, 1));
            $mii = $mii % 100;
        }
        $den .= " ";
        if (substr($mii, 0, 1) == '1' && strlen($mii) == 2) {
            $den .= getZeci($mii);
        } else {
            unset($v);
            if (strlen($mii) == 2) {
                $v[] = getZeci(substr($mii, 0, 1));
                $v[] = getCifre(substr($mii, 1, 1));

                $den .= $v[0] . " si " . $v[1];
            } else {
                $v[] = getCifre(substr($mii, 0, 1));

                $den .= $v[0];
            }
        }
    }

    $den = str_ireplace("doi mii", "doua mii", $den);
    $den = str_ireplace("doi milioane", "doua milioane", $den);
    $den = str_ireplace("unu mii", "o mie", $den);
    $den = str_ireplace("unu milioane", "un milion", $den);

    return $den;
}

/**
 * @param int $cf
 * @return string
 */
function getCifre(int $cf)
{
    switch ($cf) {
        case 1:
            return "unu";
            break;
        case 2:
            return "doi";
            break;
        case 3:
            return "trei";
            break;
        case 4:
            return "patru";
            break;
        case 5:
            return "cinci";
            break;
        case 6:
            return "sase";
            break;
        case 7:
            return "sapte";
            break;
        case 8:
            return "opt";
            break;
        case 9:
            return "noua";
            break;
        default:
            return "";
    }
}

/**
 * @param int $cf
 * @return string
 */
function getZeci(int $cf = 0)
{
    switch ($cf) {
        case 1:
            return "zece";
            break;
        case 2:
            return "douazeci";
            break;
        case 3:
            return "treizeci";
            break;
        case 4:
            return "patruzeci";
            break;
        case 5:
            return "cincizeci";
            break;
        case 6:
            return "saizeci";
            break;
        case 7:
            return "saptezeci";
            break;
        case 8:
            return "optzeci";
            break;
        case 9:
            return "nouazeci";
            break;
        case 10:
            return "zece";
            break;
        case 11:
            return "unsprezece";
            break;
        case 12:
            return "doisprezece";
            break;
        case 13:
            return "treisprezece";
            break;
        case 14:
            return "paisprezece";
            break;
        case 15:
            return "cincisprezece";
            break;
        case 16:
            return "saisprezece";
            break;
        case 17:
            return "saptesprezece";
            break;
        case 18:
            return "optsprezece";
            break;
        case 19:
            return "nouasprezece";
            break;
        default:
            return "";
    }
}

/**
 * @param int $cf
 * @return string
 */
function getSute(int $cf = 0)
{
    switch ($cf) {
        case 1:
            return "o suta";
            break;
        case 2:
            return "doua sute";
            break;
        case 3:
            return "trei sute";
            break;
        case 4:
            return "patru sute";
            break;
        case 5:
            return "cinci sute";
            break;
        case 6:
            return "sase sute";
            break;
        case 7:
            return "sapte sute";
            break;
        case 8:
            return "opt sute";
            break;
        case 9:
            return "noua sute";
            break;
        default:
            return "";
    }
}

/**
 * @param int $cf
 * @return string
 */
function getMii(int $cf = 0)
{
    switch ($cf) {
        case 1:
            return "o mie";
            break;
        case 2:
            return "doua mii";
            break;
        case 3:
            return "trei mii";
            break;
        case 4:
            return "patru mii";
            break;
        case 5:
            return "cinci mii";
            break;
        case 6:
            return "sase mii";
            break;
        case 7:
            return "sapte mii";
            break;
        case 8:
            return "opt mii";
            break;
        case 9:
            return "noua mii";
            break;
        default:
            return "";
    }
}

/**
 * @param int $cf
 * @return string
 */
function getMilioane(int $cf = 0)
{
    switch ($cf) {
        case 1:
            return "un milion";
            break;
        case 2:
            return "doua milioane";
            break;
        case 3:
            return "trei milioane";
            break;
        case 4:
            return "patru milioane";
            break;
        case 5:
            return "cinci milioane";
            break;
        case 6:
            return "sase milioane";
            break;
        case 7:
            return "sapte milioane";
            break;
        case 8:
            return "opt milioane";
            break;
        case 9:
            return "noua milioane";
            break;
        default:
            return "";
    }
}
