<?php

function NotFoundAdmin404()
{
    global $template;

    return $template->fetch('admin/404.tpl');
}
