<div class="border rounded p-3 mb-3">
    <h5 class="pb-1 mb-3 border-bottom">
        Adrese de livrare
        <a class="btn-sm btn-primary margin-top-minus-5 float-right" role="button"
           href="/account/myAddresses/addressDelivery/add">
            <i class="fas fa-plus"></i> Adauga adresa
        </a>
    </h5>

    {if empty($delivery_data_courier)}
        <div class="text-center">
            <h5>Nu ati salvat nicio adresa de livrare.</h5>
        </div>
    {else}
        {foreach from=$delivery_data_courier item=courier name=courier}
            <div class="row">
                <div class="col-9">
                    <div class="font-weight-bold">
                        Nume si numar de telefon:
                    </div>
                    <div>
                        <span class="person_full_name">{$courier.full_name}</span> -
                        <span class="person_phone_number">{$courier.phone_number}</span>
                    </div>
                    <div class="font-weight-bold">
                        Adresa:
                    </div>
                    <div>
                        <span class="person_address">{$courier.address}</span>
                    </div>
                </div>

                <div class="col-3 m-auto text-right">
                    <a href="/account/myAddresses/addressDelivery/edit/{$courier.id}"
                       class="text-dark p-2" data-toggle="tooltip" title="Modifica adresa">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a href="/account/myAddresses/addressDelivery/delete/{$courier.id}"
                       class="text-dark p-2" data-toggle="tooltip" title="Sterge adresa"
                       onclick="return confirm('Esti sigur ca vrei sa stergi adresa de livrare?')">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </div>
            </div>

            {if not $smarty.foreach.courier.last}
                <hr>
            {/if}
        {/foreach}
    {/if}
</div>
