<div class="custom-control custom-radio m-2">
    <input type="radio" id="type-cash" name="paymentData" class="custom-control-input" value="cash" required>
    <label class="custom-control-label" for="type-cash">
        Ramburs numerar<br>
        <div class="type-cash sub-box-type-payment font-weight-bold">
            Vei plati in momentul in care primesti comanda.
        </div>
    </label>
</div>

<div class="custom-control custom-radio m-2">
    <input type="radio" id="type-order" name="paymentData" class="custom-control-input" value="order" required>
    <label class="custom-control-label" for="type-order">
        Ordin de plata<br>
        <div class="type-order sub-box-type-payment font-weight-bold">
            Dupa plasarea comenzii, vei primi prin email factura proforma cu toate detaliile de plata.
        </div>
    </label>
    <div class="invalid-feedback">Selecteaza o modalitate de plata</div>
</div>


