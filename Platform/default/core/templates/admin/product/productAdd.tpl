<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Adauga produs
    </h4>

    {if isset($error)}
        <div class="alert alert-danger" role="alert">
            {$error}
        </div>
    {/if}

    <div class="mt-4">
        <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="name">
                    Nume produs <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control product-name" id="name" name="name"
                           value="{if isset($smarty.post.name)}{$smarty.post.name}{/if}" required>
                    <div class="invalid-feedback">Introduceti un Nume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="url">
                    Link <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control product-url" id="url" name="url"
                           value="{if isset($smarty.post.url)}{$smarty.post.url}{/if}" required>
                    <div class="invalid-feedback">Introduceti un Link valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="category">
                    Categorie <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <select name="category" class="form-control select2" required>
                        <option value="" selected></option>
                        {foreach from=$categories item=category}
                            <option value="{$category.id}"
                                    {if isset($smarty.post.category) && $smarty.post.category eq $category.id}selected{/if}>
                                {$category.name}
                            </option>
                            {if isset($category.sub_categories)}
                                {foreach from=$category.sub_categories item=subCategory}
                                    <option value="{$subCategory.id}"
                                            {if isset($smarty.post.category) && $smarty.post.category eq $subCategory.id}selected{/if}>
                                        &nbsp;&nbsp;&nbsp;&nbsp;{$subCategory.name}
                                    </option>
                                {/foreach}
                            {/if}
                        {/foreach}
                    </select>
                    <div class="invalid-feedback">Alegeti o Categorie valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="price">
                    Pret <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="price" name="price"
                           value="{if isset($smarty.post.price)}{$smarty.post.price}{/if}"
                           placeholder="123.45" required>
                    <div class="invalid-feedback">Introduceti un Pret valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="vat">
                    Cota TVA <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <select name="vat" class="form-control select2" required>
                        <option value="" selected></option>
                        {foreach from=$vats item=vat}
                            <option value="{$vat.id}"
                                    {if isset($smarty.post.vat) && $smarty.post.vat eq $vat.id}selected{/if}>
                                {$vat.value}% - {$vat.name}
                            </option>
                        {/foreach}
                    </select>
                    <div class="invalid-feedback">Alegeti o Cota TVA valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="stock">
                    Stoc <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="stock" name="stock"
                           value="{if isset($smarty.post.stock)}{$smarty.post.stock}{/if}"
                           placeholder="55" required>
                    <div class="invalid-feedback">Introduceti un Stoc valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="discount">
                    Discount?
                </label>
                <div class="col-sm-8">
                    <input type="checkbox" id="discount" name="discount" class="custom-control-input" value="1"
                           {if isset($smarty.post.discount)}checked{/if} onclick="$('#filter_product').toggle('slow');">
                    <label class="custom-control-label position-relative vertical-webkit-middle" for="discount">
                        <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                           title="Daca este bifat, produsul are discount"></i>
                    </label>
                </div>
            </div>
            <div class="form-group row" id="filter_product"
                 style="display:{if isset($smarty.post.discount)}flex{else}none{/if}">
                <label class="offset-1 col-sm-2 col-form-label" for="new_price">
                    Pret nou
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="new_price" name="new_price"
                           value="{if isset($smarty.post.new_price)}{$smarty.post.new_price}{/if}"
                           placeholder="123.45">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="active">
                    Vizibil
                </label>
                <div class="col-sm-8">
                    <input type="checkbox" id="active" name="active" class="custom-control-input" value="1"
                           {if isset($smarty.post.active)}checked{/if}>
                    <label class="custom-control-label position-relative vertical-webkit-middle" for="active">
                        <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                           title="Daca este bifat, produsul este vizibil in site"></i>
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="details">
                    Detalii
                </label>
                <div class="col-sm-8">
                    <textarea class="form-control" id="details" name="details" rows="3">{if isset($smarty.post.details)}{$smarty.post.details}{/if}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="image">
                    Imagine <span class="text-danger font-weight-bold">*</span>
                    <br><strong>395x350</strong>
                </label>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image"
                               data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                               accept="image/*" onchange="loadFile(event)">
                        <label class="custom-file-label" for="image">Alege imagine</label>
                    </div>
                    <img id="preview_carousel" style="max-width: 100%;">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="images">
                    Imagini secundare
                    <br><strong>395x350</strong>
                </label>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="images" name="images[]"
                               data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                               accept="image/*" onchange="preview_images();" multiple/>
                        <label class="custom-file-label" for="image">Alege imagine</label>
                    </div>
                    <div class="row" id="image_preview"></div>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    {if isset($smarty.session.admin.roles.add_product)}
                        <button type="submit" class="btn btn-primary btn-custom" name="add_product">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/products/list" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
