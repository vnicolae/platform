<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Detalii client
    </h4>

    <div class="row">
        <div class="col-md-12">
            {if isset($successMessage)}
                <div class="alert alert-success" role="alert">
                    {$successMessage}
                </div>
            {/if}
        </div>

        <div class="col-md-12">
            <div class="border rounded-bottom p-3 mb-3">
                <div class="row">
                    <div class="col-4">
                        <h6>Email</h6>
                    </div>
                    <div class="col-8">
                        {$client->email}
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <h6>Forma de adresare</h6>
                    </div>
                    <div class="col-8">
                        {if isset($client->addressing)}
                            {$client->addressing}
                        {else}
                            -
                        {/if}
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <h6>Nume</h6>
                    </div>
                    <div class="col-8">
                        {$client->last_name}
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <h6>Prenume</h6>
                    </div>
                    <div class="col-8">
                        {$client->first_name}
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <h6>Telefon</h6>
                    </div>
                    <div class="col-8">
                        {$client->phone_number}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h4 class="pb-3 mt-3 mb-3 border-bottom">
        Istoric comenzi
    </h4>

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_order').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_order" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_order)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="order_id">
                Id comanda
                <input type="text" class="form-control" name="order_id" id="order_id"
                       value="{if isset($smarty.post.order_id)}{$smarty.post.order_id}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="email">
                Data comanda
                <input type="text" class="datepicker form-control" name="order_date" id="order_date"
                       data-date-format="dd.mm.yyyy"
                       value="{if isset($smarty.post.order_date)}{$smarty.post.order_date}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="status">
                Status
                <select name="status" class="form-control">
                    <option value="" selected></option>
                    <option value="placed"
                            {if isset($smarty.post.status) && $smarty.post.status eq "placed"}selected{/if}>
                        Comanda plasata
                    </option>
                    <option value="processed"
                            {if isset($smarty.post.status) && $smarty.post.status eq "processed"}selected{/if}>
                        Comanda procesata
                    </option>
                    <option value="canceled"
                            {if isset($smarty.post.status) && $smarty.post.status eq "canceled"}selected{/if}>
                        Comanda anulata
                    </option>
                </select>
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_order">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Client</th>
            <th scope="col">Total</th>
            <th scope="col">Data</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $orders}
            {foreach from=$orders item=order}
                <tr>
                    <th scope="row">{$order.id}</th>
                    <td>{$order.first_name} {$order.last_name}</td>
                    <td>{$order.total_price} Lei</td>
                    <td>{$order.timestamp|date_format:"%d.%m.%Y %H:%M"}</td>
                    <td>
                        {if $order.active == 1}
                            Comanda plasata
                        {elseif $order.active == 2}
                            Comanda procesata
                        {else}
                            Comanda anulata
                        {/if}
                    </td>
                    <td>
                        <a href="/admin/order/view/{$order.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Vezi detalii comanda">
                            <i class="fas fa-eye"></i>
                        </a>
                        {if $order.active == 1 && !isset($order.invoice.id) && isset($smarty.session.admin.roles.add_invoice)}
                            <a href="/admin/invoice/add/{$order.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Emite factura">
                                <i class="fas fa-file-invoice"></i>
                            </a>
                        {elseif $order.active == 2}
                            <a href="/lib/file/invoice/Factura-{$order.invoice.series_value}-{$order.invoice.number}.pdf"
                               class="text-dark p-2" data-toggle="tooltip" title="Vizualizeaza factura" target="_blank">
                                <i class="fas fa-file-pdf"></i>
                            </a>
                        {/if}
                        {if $order.active == 1 && !isset($order.invoice.id) && isset($smarty.session.admin.roles.cancel_order)}
                            <a href="/admin/order/cancel/{$order.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Anuleaza comanda"
                               onclick="return confirm('Esti sigur ca vrei sa anulezi comanda?')">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="6">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
