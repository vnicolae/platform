<?php

use Dompdf\Dompdf;

function receipt()
{
    global $db;
    global $CONF;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'add') {
            if (isset($link[4]) && intval($link[4])) {
                if (isset($_POST['add_receipt'])) {
                    $sql = "INSERT INTO receipt (series_id, number, release_date, user_id, invoice_id) VALUES (
                            '" . $db->escape_string($_POST['series_receipt']) . "',
                            '" . $db->escape_string($_POST['number_receipt']) . "',
                            '" . date('Y-m-d', strtotime($_POST['release_date'])) . "',
                            '" . intval($_SESSION['admin']['user_id']) . "',
                            '" . intval($link[4]) . "')";
                    $receiptId = $db->query($sql, true);

                    $sql = "SELECT r.*, sr.value as 'series_value' FROM receipt r
                            JOIN series_receipt sr ON r.series_id = sr.id
                            WHERE r.id = '" . intval($receiptId) . "'";
                    $receipt = $db->query($sql)->fetch_object();

                    $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                            JOIN series_invoice si ON i.series_id = si.id
                            WHERE i.id = '" . intval($receipt->invoice_id) . "'";
                    $invoice = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM `order` WHERE id = '" . intval($invoice->order_id) . "'";
                    $order = $db->query($sql)->fetch_object();

                    $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                    $business = $db->query($sql)->fetch_object();

                    if (empty($receipt) || empty($invoice) || empty($order) || empty($business)) {
                        return $template->fetch('admin/somethingWentWrong.tpl');
                    }

                    $order->order_json = json_decode($order->order_json);

                    $template->assign('receipt', $receipt);
                    $template->assign('invoice', $invoice);
                    $template->assign('order', $order);
                    $template->assign('business', $business);
                    $template->assign('totalPriceLetters', getNumberInLetters($order->total_price));
                    $template->assign('CONF', $CONF);

                    $html = $template->fetch('admin/receipt/receiptDetailContent.tpl');

                    $dompdf = new Dompdf();
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper('A4', 'portrait');
                    $dompdf->render();

                    $pdf_name = "Chitanta-{$receipt->series_value}-{$receipt->number}.pdf";

                    // Output the generated PDF to Browser
                    // $dompdf->stream($pdf_name);

                    $pdf = $dompdf->output();
                    $file_location = $CONF['serverpath'] . "lib/file/receipt/".$pdf_name;
                    file_put_contents($file_location, $pdf);

                    $_SESSION['message'] = 'Chitanta a fost creata';

                    header("Location: /admin/receipt/list");
                    exit;
                }

                $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                        JOIN series_invoice si ON i.series_id = si.id
                        WHERE i.id =  '" . intval($link[4]) . "' AND i.active = 1";
                $invoice = $db->query($sql)->fetch_object();

                if (empty($invoice)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT r.id FROM receipt r
                        WHERE r.active = 1 AND r.invoice_id = {$invoice->id}";
                $invoice->receipt = $db->query($sql)->fetch_assoc();

                $sql = "SELECT * FROM `order` WHERE id = '" . intval($invoice->order_id) . "'";
                $order = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                $business = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM series_receipt WHERE active = 1";
                $seriesReceipts = $db->select($sql);

                if (empty($order) || empty($business)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $order->order_json = json_decode($order->order_json);

                $template->assign('invoice', $invoice);
                $template->assign('business', $business);
                $template->assign('order', $order);
                $template->assign('seriesReceipts', $seriesReceipts);
                $template->assign('totalPriceLetters', getNumberInLetters($order->total_price));

                return $template->fetch('admin/receipt/receiptAdd.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'view') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT r.*, sr.value as 'series_value' FROM receipt r
                        JOIN series_receipt sr ON r.series_id = sr.id
                        WHERE r.id = '" . intval($link[4]) . "'";
                $receipt = $db->query($sql)->fetch_object();

                $sql = "SELECT i.*, si.value as 'series_value' FROM invoice i
                            JOIN series_invoice si ON i.series_id = si.id
                            WHERE i.id = '" . intval($receipt->invoice_id) . "'";
                $invoice = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM `order` WHERE id = '" . intval($invoice->order_id) . "'";
                $order = $db->query($sql)->fetch_object();

                $sql = "SELECT * FROM business WHERE id =  '" . intval($_SESSION['admin']['business_id']) . "'";
                $business = $db->query($sql)->fetch_object();

                if (empty($receipt) || empty($invoice) || empty($order) || empty($business)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $order->order_json = json_decode($order->order_json);

                $template->assign('receipt', $receipt);
                $template->assign('invoice', $invoice);
                $template->assign('order', $order);
                $template->assign('business', $business);
                $template->assign('totalPriceLetters', getNumberInLetters($order->total_price));
                $template->assign('CONF', $CONF);

                return $template->fetch('admin/receipt/receiptDetail.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_receipt'])) {
                if ($_POST['series']) {
                    $where .= " AND r.series_id = '" . intval($_POST['series']) . "'";
                }
                if ($_POST['number']) {
                    $where .= " AND r.number = '" . intval($_POST['number']) . "'";
                }
                if ($_POST['invoice_series']) {
                    $where .= " AND i.series_id = '" . intval($_POST['invoice_series']) . "'";
                }
                if ($_POST['invoice_number']) {
                    $where .= " AND i.number = '" . intval($_POST['invoice_number']) . "'";
                }
                if ($_POST['last_name']) {
                    $where .= " AND c.last_name LIKE '%" . $db->escape_string($_POST['last_name']) . "%'";
                }
                if ($_POST['first_name']) {
                    $where .= " AND c.first_name LIKE '%" . $db->escape_string($_POST['first_name']) . "%'";
                }
                if ($_POST['order']) {
                    $where .= " AND i.order_id = '" . intval($_POST['order']) . "'";
                }
                if ($_POST['user']) {
                    $where .= " AND r.user_id = '" . intval($_POST['user']) . "'";
                }
                if ($_POST['receipt_date']) {
                    $where .= " AND r.release_date >= '" . date('Y-m-d', strtotime($_POST['receipt_date'])) . "'";
                    $where .= " AND r.release_date < '" . date('Y-m-d', strtotime($_POST['receipt_date'] . ' +1 day')) . "'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND r.active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND r.active = '0'";
                    }
                }
            }

            $sql = "SELECT * FROM user";
            $users = $db->select($sql);

            $sql = "SELECT * FROM series_receipt";
            $seriesReceipt = $db->select($sql);

            $sql = "SELECT * FROM series_invoice";
            $seriesInvoice = $db->select($sql);

            $sql = "SELECT r.*, i.number AS 'invoice_number', i.order_id as 'order_id',
                    c.first_name, c.last_name, o.total_price, o.client_id,
                    sr.value as 'series_value', si.value as 'invoice_series_value'
                    FROM receipt r
                    JOIN series_receipt sr ON r.series_id = sr.id
                    JOIN invoice i ON r.invoice_id = i.id
                    JOIN series_invoice si ON i.series_id = si.id
                    JOIN `order` o ON i.order_id = o.id
                    JOIN client c ON o.client_id = c.id
                    {$where}
                    ORDER BY r.timestamp DESC";
            $receipts = $db->select($sql);

            $template->assign('receipts', $receipts);
            $template->assign('users', $users);
            $template->assign('seriesReceipt', $seriesReceipt);
            $template->assign('seriesInvoice', $seriesInvoice);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/receipt/receiptList.tpl');
        }

        if ($link[3] === 'cancel') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE receipt SET active = 0 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 1";

                $db->query($sql);

                $_SESSION['message'] = 'Chitanta a fost anulata';

                header("Location: /admin/receipt/list");
                exit;
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
