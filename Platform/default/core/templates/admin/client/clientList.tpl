<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Lista clienti
    </h4>

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_client').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_client" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_client)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-6 col-form-label" for="last_name">
                Nume
                <input type="text" class="form-control" name="last_name" id="last_name"
                       value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                       placeholder="ex: Popescu">
            </label>
            <label class="col-sm-6 col-form-label" for="first_name">
                Prenume
                <input type="text" class="form-control" name="first_name" id="first_name"
                       value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                       placeholder="ex: Ion">
            </label>
            <label class="col-sm-6 col-form-label" for="phone_number">
                Telefon
                <input type="text" class="form-control" name="phone_number" id="phone_number"
                       value="{if isset($smarty.post.phone_number)}{$smarty.post.phone_number}{/if}"
                       placeholder="ex: 07xxxxxxxx">
            </label>
            <label class="col-sm-6 col-form-label" for="email">
                Email
                <input type="email" class="form-control" name="email" id="email"
                       value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}"
                       placeholder="ex: popescu.ion@exemple.com">
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_client">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nume</th>
            <th scope="col">Email</th>
            <th scope="col">Telefon</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $clients}
            {assign var="curNumber" value=1}
            {foreach from=$clients item=client}
                <tr>
                    <th scope="row">{$curNumber++}</th>
                    <td>{$client.first_name} {$client.last_name}</td>
                    <td>{$client.email}</td>
                    <td>{$client.phone_number}</td>
                    <td>
                        <a href="/admin/client/view/{$client.id}" class="text-dark"
                           data-toggle="tooltip" title="Vezi detalii comanda">
                            <i class="fas fa-eye"></i>
                        </a>
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="5">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
