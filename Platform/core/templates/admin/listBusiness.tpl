<div class="container" id="content-site-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Business
        <a class="btn btn-primary float-right" href="/business/add" role="button">
            <i class="fas fa-plus"></i> Adauga business
        </a>
    </h4>

    {if isset($succesMessage)}
        <div class="alert alert-success" role="alert">
            {$succesMessage}
        </div>
    {/if}

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Denumire</th>
            <th scope="col">Nume</th>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tbody>
        {if $businesses}
            {assign var="curNumber" value=1}
            {foreach from=$businesses item=business}
                <tr>
                    <th scope="row">{$curNumber++}</th>
                    <td>{$business.name}</td>
                    <td>{$business.admin_name}</td>
                    <td>{$business.admin_email}</td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="4">Niciun rezultat</td>
            </tr>
        {/if}
        </tbody>
    </table>
</div>
