<?php

function account()
{
    global $db;
    global $link;
    global $template;

    if (isset($link[2]) && $link[2] === 'logout') {
        unset($_SESSION['client']);

        header("Location: /");
    }

    if (isset($link[2]) && $link[2] === 'myOrders') {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            if (isset($link[3]) && $link[3]) {
                if ($link[3] === 'view') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "SELECT * FROM `order` WHERE 
                                client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                id = '" . intval($link[4]) . "'";
                        $order = $db->query($sql)->fetch_object();

                        if (empty($order)) {
                            die('Something went wrong!');
                        }

                        $order->order_json = json_decode($order->order_json);

                        $template->assign('order', $order);

                        return $template->fetch('account/order/orderDetail.tpl');
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'cancel') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "UPDATE `order` SET active = 0 WHERE 
                                client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                active = 1 AND id = '" . intval($link[4]) . "'";
                        $db->query($sql);

                        $_SESSION['message'] = 'Comanda cu numarul #' . intval($link[4]) . ' a fost anulata';

                        header("Location: /account/myOrders");
                    }

                    return $template->fetch('404.tpl');
                }

                return $template->fetch('404.tpl');
            }

            $sql = "SELECT * FROM `order` 
                    WHERE client_id = '" . $_SESSION['client']['client_id'] . "'
                    ORDER BY timestamp DESC";
            $orders = $db->select($sql);

            foreach ($orders as $key => $order) {
                $sql = "SELECT i.id, i.number, si.value as 'series_value'
                        FROM invoice i
                        JOIN series_invoice si ON i.series_id = si.id
                        WHERE i.active = 1 AND i.order_id = {$order['id']}";
                $orders[$key]['invoice'] = $db->query($sql)->fetch_assoc();
            }

            $template->assign('orders', $orders);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('account/myOrders.tpl');
        }

        return $template->fetch('404.tpl');
    }

    if (isset($link[2]) && $link[2] === 'myAddresses') {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            if (isset($link[3]) && $link[3]) {
                if ($link[3] === 'addressDelivery') {
                    if (isset($link[4]) && $link[4] === 'add') {
                        if (isset($_POST['add_address_delivery'])) {
                            $sql = "INSERT INTO client_delivery_data 
                                    (full_name, phone_number, address, client_id) VALUES (
                                    '" . $db->escape_string($_POST['full_name']) . "',
                                    '" . $db->escape_string($_POST['phone_number']) . "',
                                    '" . $db->escape_string($_POST['address']) . "',
                                    '" . $_SESSION['client']['client_id'] . "')";
                            $db->query($sql);

                            $_SESSION['message'] = 'Adresa de livrare a fost adaugata';

                            header("Location: /account/myAddresses");
                        }

                        return $template->fetch('account/address/deliveryAdd.tpl');
                    }

                    if (isset($link[4]) && $link[4] === 'edit') {
                        if (isset($link[5]) && intval($link[5])) {
                            if (isset($_POST['edit_address_delivery'])) {
                                $sql = "UPDATE client_delivery_data SET 
                                        full_name = '" . $db->escape_string($_POST['full_name']) . "',
                                        phone_number = '" . $db->escape_string($_POST['phone_number']) . "',
                                        address = '" . $db->escape_string($_POST['address']) . "'
                                        WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                        active = 1 AND id = '" . intval($link[5]) . "'";
                                $db->query($sql);

                                $_SESSION['message'] = 'Adresa de livrare a fost editata';

                                header("Location: /account/myAddresses");
                            }

                            $sql = "SELECT * FROM client_delivery_data WHERE 
                                    client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                    active = 1 AND id = '" . intval($link[5]) . "'";
                            $delivery_data_courier = $db->select($sql);

                            if (!empty($delivery_data_courier)) {
                                $template->assign('delivery_data_courier', $delivery_data_courier[0]);

                                return $template->fetch('account/address/deliveryEdit.tpl');
                            }

                            return $template->fetch('404.tpl');
                        }

                        return $template->fetch('404.tpl');
                    }

                    if (isset($link[4]) && $link[4] === 'delete') {
                        if (isset($link[5]) && intval($link[5])) {
                            $sql = "UPDATE client_delivery_data SET active = 0 WHERE 
                                    client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                    active = 1 AND id = '" . intval($link[5]) . "'";
                            $db->query($sql);

                            $_SESSION['message'] = 'Adresa de livrare a fost stearsa';

                            header("Location: /account/myAddresses");
                        }

                        return $template->fetch('404.tpl');
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'addressIndividualPerson') {
                    if (isset($link[4]) && $link[4] === 'add') {
                        if (isset($_POST['add_address_individual_person'])) {
                            $sql = "INSERT INTO client_billing_data 
                                    (full_name, phone_number, address, type_person, client_id) VALUES (
                                    '" . $db->escape_string($_POST['full_name']) . "',
                                    '" . $db->escape_string($_POST['phone_number']) . "',
                                    '" . $db->escape_string($_POST['address']) . "',
                                    '1', '" . $_SESSION['client']['client_id'] . "')";
                            $db->query($sql);

                            $_SESSION['message'] = 'Adresa de facturare persoana fizica a fost adaugata';

                            header("Location: /account/myAddresses");
                        }

                        return $template->fetch('account/address/individualPersonAdd.tpl');
                    }

                    if (isset($link[4]) && $link[4] === 'edit') {
                        if (isset($link[5]) && intval($link[5])) {
                            if (isset($_POST['edit_address_individual_person'])) {
                                $sql = "UPDATE client_billing_data SET 
                                        full_name = '" . $db->escape_string($_POST['full_name']) . "',
                                        phone_number = '" . $db->escape_string($_POST['phone_number']) . "',
                                        address = '" . $db->escape_string($_POST['address']) . "'
                                        WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                        type_person = 1 AND active = 1 AND id = '" . intval($link[5]) . "'";
                                $db->query($sql);

                                $_SESSION['message'] = 'Adresa de facturare persoana fizica a fost editata';

                                header("Location: /account/myAddresses");
                            }

                            $sql = "SELECT * FROM client_billing_data WHERE 
                                    client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                    type_person = 1 AND active = 1 AND id = '" . intval($link[5]) . "'";
                            $billing_data_individual_person = $db->select($sql);

                            if (!empty($billing_data_individual_person)) {
                                $template->assign('billing_data_individual_person', $billing_data_individual_person[0]);

                                return $template->fetch('account/address/individualPersonEdit.tpl');
                            }

                            return $template->fetch('404.tpl');
                        }

                        return $template->fetch('404.tpl');
                    }

                    if (isset($link[4]) && $link[4] === 'delete') {
                        if (isset($link[5]) && intval($link[5])) {
                            $sql = "UPDATE client_billing_data SET active = 0 WHERE 
                                    client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                    type_person = 1 AND active = 1 AND id = '" . intval($link[5]) . "'";
                            $db->query($sql);

                            $_SESSION['message'] = 'Adresa de facturare persoana fizica a fost stearsa';

                            header("Location: /account/myAddresses");
                        }

                        return $template->fetch('404.tpl');
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'addressLegalPerson') {
                    if (isset($link[4]) && $link[4] === 'add') {
                        if (isset($_POST['add_address_legal_person'])) {
                            if ($_POST['vat_payer'] === '0') {
                                $unique_registration_code = $_POST['unique_registration_code'];
                            } else {
                                $unique_registration_code = $_POST['vat_payer'] . $_POST['unique_registration_code'];
                            }

                            $sql = "INSERT INTO client_billing_data 
                                    (full_name, unique_registration_code, address, type_person, client_id) VALUES (
                                    '" . $db->escape_string($_POST['full_name']) . "',
                                    '" . $db->escape_string($unique_registration_code) . "',
                                    '" . $db->escape_string($_POST['address']) . "',
                                    '2', '" . $_SESSION['client']['client_id'] . "')";
                            $db->query($sql);

                            $_SESSION['message'] = 'Adresa de facturare persoana juridica a fost adaugata';

                            header("Location: /account/myAddresses");
                        }

                        return $template->fetch('account/address/legalPersonAdd.tpl');
                    }

                    if (isset($link[4]) && $link[4] === 'edit') {
                        if (isset($link[5]) && intval($link[5])) {
                            if (isset($_POST['edit_address_legal_person'])) {
                                if ($_POST['vat_payer'] === '0') {
                                    $unique_registration_code = $_POST['unique_registration_code'];
                                } else {
                                    $unique_registration_code = $_POST['vat_payer'] . $_POST['unique_registration_code'];
                                }

                                $sql = "UPDATE client_billing_data SET 
                                        full_name = '" . $db->escape_string($_POST['full_name']) . "',
                                        unique_registration_code = '" . $db->escape_string($unique_registration_code) . "',
                                        address = '" . $db->escape_string($_POST['address']) . "'
                                        WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                        type_person = 2 AND active = 1 AND id = '" . intval($link[5]) . "'";
                                $db->query($sql);

                                $_SESSION['message'] = 'Adresa de facturare persoana juridica a fost editata';

                                header("Location: /account/myAddresses");
                            }

                            $sql = "SELECT * FROM client_billing_data WHERE 
                                    client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                    type_person = 2 AND active = 1 AND id = '" . intval($link[5]) . "'";
                            $billing_data_legal_person = $db->select($sql);

                            if (!empty($billing_data_legal_person)) {
                                $template->assign('billing_data_legal_person', $billing_data_legal_person[0]);

                                return $template->fetch('account/address/legalPersonEdit.tpl');
                            }

                            return $template->fetch('404.tpl');
                        }

                        return $template->fetch('404.tpl');
                    }

                    if (isset($link[4]) && $link[4] === 'delete') {
                        if (isset($link[5]) && intval($link[5])) {
                            $sql = "UPDATE client_billing_data SET active = 0 WHERE 
                                    client_id = '" . $_SESSION['client']['client_id'] . "' AND 
                                    type_person = 2 AND active = 1 AND id = '" . intval($link[5]) . "'";
                            $db->query($sql);

                            $_SESSION['message'] = 'Adresa de facturare persoana juridica a fost stearsa';

                            header("Location: /account/myAddresses");
                        }

                        return $template->fetch('404.tpl');
                    }

                    return $template->fetch('404.tpl');
                } else {
                    return $template->fetch('404.tpl');
                }
            }

            $sql = "SELECT * FROM client_billing_data WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND type_person = 1 AND active = 1";
            $billing_data_individual_person = $db->select($sql);

            $sql = "SELECT * FROM client_billing_data WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND type_person = 2 AND active = 1";
            $billing_data_legal_person = $db->select($sql);

            $sql = "SELECT * FROM client_delivery_data WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND active = 1";
            $delivery_data_courier = $db->select($sql);

            $template->assign('billing_data_individual_person', $billing_data_individual_person);
            $template->assign('billing_data_legal_person', $billing_data_legal_person);
            $template->assign('delivery_data_courier', $delivery_data_courier);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('account/myAddresses.tpl');

        }

        return $template->fetch('404.tpl');
    }

    if (isset($link[2]) && $link[2] === 'personalData') {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            if (isset($link[3]) && $link[3]) {
                if ($link[3] === 'edit') {
                    if (isset($_POST['edit_personal_data'])) {
                        $addressing = NULL;
                        if (!empty($_POST['addressing'])) {
                            $addressing = $db->escape_string($_POST['addressing']);
                        }

                        $sql = "UPDATE client SET 
                                first_name = '" . $db->escape_string($_POST['first_name']) . "',
                                last_name = '" . $db->escape_string($_POST['last_name']) . "',
                                phone_number = '" . $db->escape_string($_POST['phone_number']) . "',
                                addressing = '" . $addressing . "'
                                WHERE id = '" . $_SESSION['client']['client_id'] . "'";
                        $db->query($sql);

                        $_SESSION['message'] = 'Datele personale au fost actualizate';

                        $_SESSION['client']['first_name'] = $_POST['first_name'];
                        $_SESSION['client']['last_name'] = $_POST['last_name'];

                        header("Location: /account/personalData");
                    }

                    $sql = "SELECT * FROM client WHERE id = '" . $_SESSION['client']['client_id'] . "'";
                    $client = $db->query($sql)->fetch_object();

                    if (empty($client)) {
                        die('Something went wrong!');
                    }

                    $template->assign('client', $client);

                    return $template->fetch('account/personalData/personalDataEdit.tpl');
                }

                if ($link[3] === 'changePassword') {
                    if (isset($_POST['change_password'])) {
                        $errors = array();

                        $sql = "SELECT * FROM client WHERE id = '" . $_SESSION['client']['client_id'] . "'";
                        $client = $db->query($sql)->fetch_object();

                        if ($client->password !== md5($_POST['old_password'])) {
                            $errors[] = 'Vechea parola nu coincide!';
                        }
                        if ($_POST['password'] !== $_POST['confirm_password']) {
                            $errors[] = 'Parolele nu coincid!';
                        }

                        if(!empty($errors)) {
                            $template->assign('errors', $errors);
                        } else {
                            $sql = "UPDATE client SET 
                                    password = '" . md5($db->escape_string($_POST['password'])) . "'";
                            $db->query($sql);

                            $_SESSION['message'] = 'Parola contului a fost schimbata';

                            header("Location: /account/personalData");
                        }
                    }

                    return $template->fetch('account/personalData/personalDataChangePass.tpl');
                }
            }

            $sql = "SELECT * FROM client WHERE id = '" . $_SESSION['client']['client_id'] . "'";
            $client = $db->query($sql)->fetch_object();

            $template->assign('client', $client);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('account/personalData.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}
