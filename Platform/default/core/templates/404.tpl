<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Eroare 404 - Page not found
</h4>

<div class="row">
    <div class="col-md-12 mb-3 text-center">
        <h5>Aceasta pagina a fost mutata sau nu mai exista!</h5>
        <a class="btn btn-primary btn-custom" href="/" role="button">Intoarce-te la magazin</a>
    </div>
</div>
