<h2 class="mt-4 mb-3 border-bottom-5">
    Hei, invata online cu noi!
</h2>

<h3 class="mb-3 border-bottom-2">Cauta dupa clasa</h3>

<div class="row">
    {foreach from=$classes item=class}
        <div class="col-lg-3 col-md-4 col-sm-6">
            {include file='classBox.tpl'}
        </div>
    {/foreach}
</div>

<h3 class="mb-3 border-bottom-2">Cauta dupa materie</h3>

<div class="row">
    {foreach from=$materials item=material}
        <div class="col-lg-3 col-md-4 col-sm-6">
            {include file='materialBox.tpl'}
        </div>
    {/foreach}
</div>
