<?php

function NotFound404()
{
    global $template;

    return $template->fetch('404.tpl');
}
