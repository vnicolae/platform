<?php

function client()
{
    global $db;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_client'])) {
                if ($_POST['last_name']) {
                    $where .= " AND last_name LIKE '%" . $db->escape_string($_POST['last_name']) . "%'";
                }
                if ($_POST['first_name']) {
                    $where .= " AND first_name LIKE '%" . $db->escape_string($_POST['first_name']) . "%'";
                }
                if ($_POST['phone_number']) {
                    $where .= " AND phone_number LIKE '%" . $db->escape_string($_POST['phone_number']) . "%'";
                }
                if ($_POST['email']) {
                    $where .= " AND email LIKE '%" . $db->escape_string($_POST['email']) . "%'";
                }
            }

            $sql = "SELECT * FROM client {$where}";
            $clients = $db->select($sql);

            $template->assign('clients', $clients);

            return $template->fetch('admin/client/clientList.tpl');
        }

        if ($link[3] === 'view') {
            if (isset($link[4]) && intval($link[4])) {
                $where = "";
                if (isset($_POST['filter_order'])) {
                    if ($_POST['order_id']) {
                        $where .= " AND o.id = '" . intval($_POST['order_id']) . "'";
                    }
                    if ($_POST['order_date']) {
                        $where .= " AND o.timestamp >= '" . date('Y-m-d', strtotime($_POST['order_date'])) . "'";
                        $where .= " AND o.timestamp < '" . date('Y-m-d', strtotime($_POST['order_date'] . ' +1 day')) . "'";
                    }
                    if ($_POST['status']) {
                        if ($_POST['status'] === 'placed') {
                            $where .= " AND o.active = '1'";
                        } elseif ($_POST['status'] === 'processed') {
                            $where .= " AND o.active = '2'";
                        } elseif ($_POST['status'] === 'canceled') {
                            $where .= " AND o.active = '0'";
                        }
                    }
                }

                $sql = "SELECT * FROM client 
                        WHERE id = '" . intval($link[4]) . "'";
                $client = $db->query($sql)->fetch_object();

                if (empty($client)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT o.*, c.first_name, c.last_name
                        FROM `order` o 
                        JOIN client c ON c.id = o.client_id
                        WHERE o.client_id = '" . intval($link[4]) . "'
                        {$where}
                        ORDER BY o.timestamp DESC";
                $orders = $db->select($sql);

                foreach ($orders as $key => $order) {
                    $sql = "SELECT i.id, i.number, si.value as 'series_value'
                            FROM invoice i
                            JOIN series_invoice si ON i.series_id = si.id
                            WHERE i.active = 1 AND i.order_id = '" . intval($order['id']) . "'";
                    $orders[$key]['invoice'] = $db->query($sql)->fetch_assoc();
                }

                $template->assign('client', $client);
                $template->assign('orders', $orders);

                return $template->fetch('admin/client/clientDetail.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
