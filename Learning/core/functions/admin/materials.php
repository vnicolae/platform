<?php

function materials()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_material'])) {
                if ($_POST['material']) {
                    $where .= " AND c.id = '" . $_POST['material'] . "'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND c.active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND c.active = '0'";
                    }
                }
            }

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }
            if (isset($_SESSION['message-error'])) {
                $successMessageError = $_SESSION['message-error'];
                $template->assign('successMessageError', $successMessageError);
                unset($_SESSION['message-error']);
            }

            $sql = "SELECT * FROM material c {$where}";
            $materialsList = $database->select($sql);

            $template->assign('materialsList', $materialsList);
            $template->assign('materials', getAllMaterials());

            return $template->fetch('admin/material/materialList.tpl');
        }

        if ($link[3] === 'add') {
            if (isset($_POST['add_material'])) {
                $materialUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                $materialUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $materialUrl);

                $sql = "SELECT url FROM material
                        WHERE url = '" . $database->escape($materialUrl) . "'";

                if ($database->query($sql)->fetch_object()) {
                    $error = 'Link-ul materiei trebuie sa fie unic!';

                    $template->assign('error', $error);

                    return $template->fetch('admin/material/materialAdd.tpl');
                }

                $active = 1;
                if (!isset($_POST['active'])) {
                    $active = 0;
                }

                $sql = "INSERT INTO material 
                        (name, url, active) VALUES (
                        '" . $database->escape($_POST['name']) . "',
                        '" . $database->escape($_POST['url']) . "',
                        '" . intval($active) . "')";
                $database->query($sql);

                $_SESSION['message'] = 'Materia a fost adaugata.';

                header("Location: /admin/materials/list");
            }

            return $template->fetch('admin/material/materialAdd.tpl');
        }

        if ($link[3] === 'edit') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT * FROM material WHERE id =  '" . intval($link[4]) . "'";
                $materialEdit = $database->query($sql)->fetch_object();

                if (empty($materialEdit)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $template->assign('materialEdit', $materialEdit);

                if (isset($_POST['edit_material'])) {
                    $materialUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                    $materialUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $materialUrl);

                    $sql = "SELECT url FROM material
                            WHERE url = '" . $database->escape($materialUrl) . "'
                            AND id != '" . intval($link[4]) . "'";

                    if ($database->query($sql)->fetch_object()) {
                        $error = 'Link-ul materiei trebuie sa fie unic!';

                        $template->assign('error', $error);

                        return $template->fetch('admin/material/materialEdit.tpl');
                    }

                    $active = 1;
                    if (!isset($_POST['active'])) {
                        $active = 0;
                    }

                    $sql = "UPDATE material
                            SET name = '" . $database->escape($_POST['name']) . "',
                                url = '" . $database->escape($_POST['url']) . "',
                                active = '" . intval($active) . "'
                            WHERE id = '" . intval($link[4]) . "'";
                    $database->query($sql);

                    $_SESSION['message'] = 'Materia a fost editata.';

                    header("Location: /admin/materials/list");
                }

                return $template->fetch('admin/material/materialEdit.tpl');
            }
        }

        if ($link[3] === 'enable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE material SET active = 1
                        WHERE id = '" . intval($link[4]) . "' AND active = 0";
                $database->query($sql);

                $_SESSION['message'] = 'Materia a fost activata.';

                header("Location: /admin/materials/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'disable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE material SET active = 0
                        WHERE id = '" . intval($link[4]) . "' AND active = 1";
                $database->query($sql);

                $_SESSION['message'] = 'Materia a fost dezactivat.';

                header("Location: /admin/materials/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
