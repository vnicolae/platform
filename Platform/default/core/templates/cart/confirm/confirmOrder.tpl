<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Confirmare comanda
</h4>

<div class="row">
    <div class="col-md-12 mb-3 text-center">
        <h5>Comanda ta cu numarul #{if isset($orderId)}{$orderId}{/if} a fost inregistrata.</h5>
        <h5>Iti multumim!</h5>
        <a class="btn btn-primary btn-custom" href="/" role="button">Intoarce-te la magazin</a>
    </div>
</div>
