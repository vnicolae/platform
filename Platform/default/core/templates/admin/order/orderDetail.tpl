<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Detalii comanda numarul #{$order->id}
        {if $order->active == 1 && isset($smarty.session.admin.roles.add_invoice)}
            <a class="btn btn-primary float-right" href="/admin/invoice/add/{$order->id}" role="button">
                <i class="fas fa-pencil-alt"></i> Emite factura
            </a>
        {/if}
    </h4>

    <div class="row">
        <div class="col-md-12">
            {if empty($order)}
                <div class="border rounded p-3 mb-3">
                    <div class="text-center">
                        <h5>Aceasta comanda nu exista.</h5>
                    </div>
                </div>
            {else}
                <div class="border rounded p-3 mb-3">
                    {foreach from=$order->order_json->cart->products item=productCart}
                        <div class="row">
                            <div class="col-3">
                                <div class="text-center">
                                    <div class="overflow-hidden height-120">
                                        <img src="/images/product/{$productCart->image|default:'default-image.jpg'}"
                                             class="img-fluid width-120">
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="font-weight-bold">
                                    <a href="/product/{$productCart->url}" class="text-dark">
                                        {$productCart->name}
                                    </a>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="font-weight-bold">
                                    <strong>{$productCart->quantity} buc</strong>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="text-right">
                                    {assign var="totalProductPrice" value=$productCart->quantity * $productCart->price}
                                    <h4>{$totalProductPrice|string_format: "%.2f"} Lei</h4>
                                </div>
                            </div>
                        </div>
                        <hr>
                    {/foreach}

                    <div class="row">
                        <div class="col-7 border-right">
                            <h5>Desfasurator Comanda</h5>
                            <table class="table table-borderless table-paddingless">
                                <tr>
                                    <td>Cost produse</td>
                                    <td class="text-right">
                                        {$order->order_json->cart->totalPrice|string_format: "%.2f"} Lei
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cost livrare</td>
                                    <td class="text-right">
                                        {if $order->order_json->cart->deliveryPrice > 0}
                                            {$order->order_json->cart->deliveryPrice|string_format: "%.2f"} Lei
                                        {else}
                                            <strong class="text-success">GRATUIT</strong>
                                        {/if}
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-5">
                            <h5>Total</h5>
                            <h4>{$order->order_json->cart->totalPrice|string_format: "%.2f"} Lei</h4>
                        </div>
                    </div>
                </div>
                <div class="border rounded p-3 mb-3">
                    <h5>Date de facturare</h5>
                    {if $order->order_json->person->type_person == 'individualPerson'}
                        <div class="font-weight-bold">
                            Nume si numar de telefon:
                        </div>
                        <div>
                            <span class="person_full_name">{$order->order_json->person->full_name}</span> -
                            <span class="person_phone_number">{$order->order_json->person->phone_number}</span>
                        </div>
                        <div class="font-weight-bold">
                            Adresa:
                        </div>
                        <div>
                            <span class="person_address">{$order->order_json->person->address}</span>
                        </div>
                    {elseif $order->order_json->person->type_person == 'legalPerson'}
                        <div class="font-weight-bold">
                            Nume companie si CUI:
                        </div>
                        <div>
                            <span class="person_full_name">{$order->order_json->person->full_name}</span> -
                            <span class="person_phone_number">{$order->order_json->person->unique_registration_code}</span>
                        </div>
                        <div class="font-weight-bold">
                            Sediu central:
                        </div>
                        <div>
                            <span class="person_address">{$order->order_json->person->address}</span>
                        </div>
                    {/if}
                </div>
                <div class="border rounded p-3 mb-3">
                    <h5>Date de livrare</h5>
                    {if $order->order_json->delivery->type_delivery == 'courier'}
                        <div class="font-weight-bold">
                            Nume si numar de telefon:
                        </div>
                        <div>
                            <span class="person_full_name">{$order->order_json->delivery->full_name}</span> -
                            <span class="person_phone_number">{$order->order_json->delivery->phone_number}</span>
                        </div>
                        <div class="font-weight-bold">
                            Adresa:
                        </div>
                        <div>
                            <span class="person_address">{$order->order_json->delivery->address}</span>
                        </div>
                    {elseif $order->order_json->delivery->type_delivery == 'personal'}
                        Ridicare personala de la sediul nostru
                        <div class="font-weight-bold">
                            Strada Margeanului, Sectorul 5, Bucuresti
                        </div>
                    {/if}
                </div>
                <div class="border rounded p-3 mb-3">
                    <h5>Metoda de plata</h5>
                    {if $order->order_json->payment->type_payment == 'cash'}
                        Ramburs numerar
                        <div class="font-weight-bold">
                            Vei plati in momentul in care primesti comanda.
                        </div>
                    {elseif $order->order_json->payment->type_payment == 'order'}
                        Ordin de plata
                        <div class="font-weight-bold">
                            Dupa plasarea comenzii, vei primi prin email factura proforma cu toate detaliile de plata.
                        </div>
                    {/if}
                </div>
                {if !empty($order->order_json->note->note)}
                    <div class="border rounded p-3 mb-3">
                        <h5>Observatii</h5>
                        {$order->order_json->note->note}
                    </div>
                {/if}
            {/if}
        </div>
    </div>
</div>
