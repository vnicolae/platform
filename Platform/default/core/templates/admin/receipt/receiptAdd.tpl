<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Emite chitanta pentru factura nr. #{$invoice->id}
    </h4>

    <form class="needs-validation" method="POST" novalidate>
        <div class="border p-3">
            <div class="form-group row header-invoice font-weight-bold font-size-12 position-relative">
                <div class="col-md-5">
                    <table class="table table-borderless table-paddingless m-0">
                        <tbody>
                        <tr>
                            <td width="50%">Denumire furnizor</td>
                            <td width="50%">{$business->name}</td>
                        </tr>
                        <tr>
                            <td>Cod unic de identificare</td>
                            <td>{$business->unique_registration_code}</td>
                        </tr>
                        <tr>
                            <td>Numar registru comert</td>
                            <td>{$business->trade_register_code}</td>
                        </tr>
                        <tr>
                            <td>Capital social</td>
                            <td>{$business->social_capital}</td>
                        </tr>
                        <tr>
                            <td>Sediu central</td>
                            <td>{$business->address}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-group row text-center font-weight-bold font-size-18">
                <div class="col-sm-12">
                    CHITANTA
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4 m-auto">
                    <table class="table table-borderless table-padding-1 font-size-12 font-weight-bold m-0">
                        <tr>
                            <td class="align-middle" width="60%">Seria</td>
                            <td class="align-middle" width="40%">
                                <select name="series_receipt"
                                        class="form-control form-control-sm height-30 p-1 font-size-12" required>
                                    <option value=""></option>
                                    {foreach from=$seriesReceipts item=$seriesReceipt}
                                        <option name="series_receipt" value="{$seriesReceipt.id}">
                                            {$seriesReceipt.value}
                                        </option>
                                    {/foreach}
                                </select>
                                <div class="invalid-feedback">Introduceti Seria.</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle" width="60%">Nr. chitanta</td>
                            <td class="align-middle" width="40%">
                                <input type="text" name="number_receipt" id="number_receipt"
                                       class="form-control height-30 p-1 font-size-12" readonly required>
                                <div class="invalid-feedback">Introduceti Numarul.</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle" width="60%">Data (zi.luna.an)</td>
                            <td class="align-middle" width="40%">
                                <input type="text" name="release_date" id="release_date" data-date-format="dd.mm.yyyy"
                                       class="form-control datepicker height-30 p-1 font-size-12" readonly required>
                                <div class="invalid-feedback">Introduceti Data.</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-9">
                    Am primit de la <strong>{$order->order_json->person->full_name|upper}</strong>
                </div>
                <div class="col-sm-3">
                    Semnatura si stampila
                </div>
                <div class="col-sm-12">
                    Adresa <strong>{$order->order_json->person->address}</strong>
                </div>
                <div class="col-sm-12">
                    Suma de <strong>{$order->order_json->cart->totalPrice|string_format: "%.2f"} Lei</strong>,
                    adica <strong>{$totalPriceLetters}</strong>
                </div>
                <div class="col-sm-12">
                    Reprezentand CV. facturii seria <strong>{$invoice->series_value}</strong>
                    nr. <strong>{$invoice->number}</strong>
                    din data de <strong>{$invoice->release_date|date_format:"%d.%m.%Y"}</strong>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12 mt-3 text-center">
                {if $invoice->active == 1 && !isset($invoice->receipt.id) && isset($smarty.session.admin.roles.add_receipt)}
                    <button type="submit" class="btn btn-primary btn-custom" name="add_receipt">Salveaza</button>
                {/if}
                <a class="btn btn-danger btn-custom" href="/admin/receipt/list" role="button">Anuleaza</a>
            </div>
        </div>
    </form>
</div>
