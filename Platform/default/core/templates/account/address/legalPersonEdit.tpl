<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Editeaza adresa de facturare persoana juridica
</h4>

<form class="needs-validation" method="POST" novalidate autocomplete="off">
    <div class="form-group row">
        <div class="col-sm-12 font-weight-bold pt-2">Detalii companie</div>
        <label class="col-sm-6 col-form-label" for="full_name">
            Nume companie <span class="text-danger font-weight-bold">*</span>
            <input type="text" class="form-control" id="full_name"
                   placeholder="ex: Exemplu Com SRL" name="full_name"
                   value="{$billing_data_legal_person.full_name}" required>
            <div class="invalid-feedback">Introduceti un Nume companie valid.</div>
        </label>
        <label class="col-sm-6 col-form-label" for="unique_registration_code">
            Cod unic de inregistrare <span class="text-danger font-weight-bold">*</span>
            <div class="row">
                <label class="col-sm-4 col-form-label pt-0 pb-0">
                    <select class="form-control" name="vat_payer" required>
                        <option value="0" selected>-</option>
                        <option {if $billing_data_legal_person.unique_registration_code|substr:0:2 == 'RO'}selected{/if}
                                value="RO">RO
                        </option>
                    </select>
                </label>
                <label class="col-sm-8 col-form-label pt-0 pb-0">
                    <input type="text" class="form-control" id="unique_registration_code"
                           placeholder="ex: 1234567" name="unique_registration_code"
                            {if $billing_data_legal_person.unique_registration_code|substr:0:2 == 'RO'}
                                value="{$billing_data_legal_person.unique_registration_code|substr:2}"
                            {else}
                                value="{$billing_data_legal_person.unique_registration_code}"
                            {/if} required>
                    <div class="invalid-feedback">Introduceti un Cod fiscal valid.</div>
                </label>
            </div>
        </label>
    </div>

    <div class="form-group row">
        <div class="col-sm-12 font-weight-bold pt-2">Sediu central</div>
        <label class="col-sm-12 col-form-label" for="address">
            Adresa <span class="text-danger font-weight-bold">*</span>
            <input type="text" class="form-control" id="address"
                   placeholder="ex: Strada, numar, bloc, scara, etaj, apartament, judet, localitate"
                   name="address" value="{$billing_data_legal_person.address}" required>
            <div class="invalid-feedback">Introduceti o Adresa valida.</div>
        </label>
    </div>

    <div class="form-group row">
        <small class="col-sm-12">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary btn-custom" name="edit_address_legal_person">Salveaza</button>
        </div>
    </div>
</form>
