<?php

function teacher()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[2]) && $link[2]) {
        if ($link[2] === 'letter') {
            if (isset($link[3]) && $link[3]) {
                if (in_array($link[3], range('A', 'Z'))) {
                    $teachers = [];
                    $allTeachers = [];
                    foreach (range('A', 'Z') as $letter) {
                        $allTeachers[$letter] = getTeachersByLetter($letter);

                        if ($link[3] === $letter) {
                            $teachers[$letter] = $allTeachers[$letter];
                        }
                    }

                    $template->assign('teachers', $teachers);
                    $template->assign('allTeachers', $allTeachers);

                    return $template->fetch('teachers.tpl');
                }

                return $template->fetch('404.tpl');
            }

            return $template->fetch('404.tpl');
        }

        $sql = "SELECT * FROM client WHERE id = '" . intval($link[2]) . "' AND role = 5;";
        $teacher = $database->query($sql)->fetch_object();

        print_ra($teacher);
        if (!empty($teacher)) {
            if (isset($_POST['add_message'])) {
                $sql = "INSERT INTO message (participant_1, participant_2, message)
                        VALUES (
                            '" . $database->escape($_SESSION['client']['client_id']) . "',
                            '" . $database->escape($teacher->id) . "',
                            '" . $database->escape($_POST['message']) . "'
                        );";
                $database->query($sql);

                $_SESSION['message'] = 'Mesajul tau a fost trimis.';

                header("Location: /teacher/" . $teacher->id);
                exit;
            }

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            $teacher->lessons = getLessons($teacher->id);

            $template->assign('teacher', $teacher);

            return $template->fetch('teacher.tpl');
        }

        return $template->fetch('404.tpl');
    }

    $allTeachers = [];
    foreach (range('A', 'Z') as $letter) {
        $allTeachers[$letter] = getTeachersByLetter($letter);
    }

    $template->assign('teachers', $allTeachers);
    $template->assign('allTeachers', $allTeachers);

    return $template->fetch('teachers.tpl');
}
