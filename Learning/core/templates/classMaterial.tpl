<h2 class="mt-4 mb-3 border-bottom-5">
    {$class->name}
</h2>

<h6 class="mb-3 border-bottom-2">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
    {$class->name}
</h6>

<div class="row">
    {foreach from=$materials item=material}
        <div class="col-lg-3 col-md-4 col-sm-6">
            {include file='classMaterialBox.tpl'}
        </div>
    {/foreach}
</div>
