<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="{$CONF.sitepath}">

    <title>{$title|default:$meta_titlu|default:"Sistem"}</title>

    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/css/bootstrap-reboot.min.css">

    <script type="text/javascript" src="//{$CONF.sitepath}lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="//{$CONF.sitepath}lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//{$CONF.sitepath}lib/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="//{$CONF.sitepath}lib/index.css">

    <link rel="shortcut icon" href="//{$CONF.sitepath}images/admin/favicon.png" type="image/png">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
</head>

<body>
{include file="struct/header.tpl"}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {if empty($imagesCarousel)}

            {else}
                <div id="demo" class="carousel slide" data-ride="carousel">
                    {if count($imagesCarousel) > 1}
                        <ul class="carousel-indicators">
                            {foreach from=$imagesCarousel item=imageCarousel key=key name=imageCarousel}
                                <li data-target="#demo" data-slide-to="{$key}"
                                    class="{if $smarty.foreach.imageCarousel.first}active{/if}">
                                </li>
                            {/foreach}
                        </ul>
                    {/if}

                    <div class="carousel-inner">
                        {foreach from=$imagesCarousel item=imageCarousel name=imageCarousel}
                            <div class="carousel-item {if $smarty.foreach.imageCarousel.first}active{/if}">
                                <a {if $imageCarousel.link}href="{$imageCarousel.link}{/if}">
                                    <img src="/images/carousel/{$imageCarousel.name}"
                                         alt="{$imageCarousel.title}" width="1110" height="500">
                                    <div class="carousel-caption">
                                        <h3>{$imageCarousel.title}</h3>
                                        <p>{$imageCarousel.sub_title}</p>
                                    </div>
                                </a>
                            </div>
                        {/foreach}
                    </div>

                    {if count($imagesCarousel) > 1}
                        <a class="carousel-control-prev prev-slider" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next next-slider" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    {/if}
                </div>
            {/if}
        </div>
    </div>

    <div class="row" id="content-site-container">
        <div class="col-md-3">
            {include file="struct/categories.tpl"}
        </div>
        <div class="col-md-9">
            {$content}
        </div>
    </div>
</div>
{include file="struct/footer.tpl"}
