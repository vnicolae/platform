<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require_once 'config.php';
require_once $CONF['smarty_dir'] . 'Smarty.class.php';
require_once $CONF['dompdf_dir'] . 'lib/html5lib/Parser.php';
require_once $CONF['dompdf_dir'] . 'src/Autoloader.php';
require_once $CONF['phpoffice_dir'] . 'vendor/autoload.php';
Dompdf\Autoloader::register();

$link = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$link = explode("/", $link);

function includes($dir)
{
    $dir = $dir . "/";
    $handler = opendir($dir);

    while ($file = readdir($handler)) {
        if ($file != '.' && $file != '..') {
            $item[] = $file;
        }
    }

    closedir($handler);

    for ($i = 0; !empty($item[$i]); $i++) {
        is_file($dir . $item[$i]) ? require_once($dir . $item[$i]) : includes($dir . $item[$i]);
    }
}

includes($CONF['core_dir'] . "functions");
includes($CONF['core_dir'] . "classes");

$db = new DB();
$template = new template();
$template->assign('link', $link);

session_start();

if ($link[1] == '' || preg_match("@^\?@", $link[1])) {
    eval ($cmd = '$content = acasa();');
} else {
    if ($link[1] === 'admin') {
        die(admin());
    }
    if (in_array($link[1], array('ajax', 'createAccount', 'loginAccount', 'account', 'category', 'product', 'myCart'))) {
        eval ($cmd = '$content = ' . $link[1] . "();");
    } else {
        eval ($cmd = '$content = notFound404();');
    }
}

$template->assign('imagesCarousel', getImagesCarousel());
$template->assign('categories', getAllCategories());
$template->assign('logoSite', getLogoSite());

$template->assign('content', $content);
$template->assign('CONF', $CONF);

$template->displayTemplate('main.tpl');
