<?php

function account()
{
    global $CONF;
    global $link;
    global $database;
    global $template;

    if (isset($link[2]) && $link[2] === 'logout') {
        unset($_SESSION['client']);

        header("Location: /");
    }

    if (isset($link[2]) && $link[2] === 'myLessons') {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            if (isset($link[3]) && $link[3]) {
                if ($link[3] === 'view') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "SELECT *
                                FROM lesson
                                WHERE id =  '" . intval($link[4]) . "' AND active = 1 AND
                                    client_id = '" . $database->escape($_SESSION['client']['client_id']) . "';";
                        $lesson = $database->query($sql)->fetch_object();

                        if (empty($lesson)) {
                            return $template->fetch('404.tpl');
                        }

                        $sql = "SELECT * FROM file WHERE lesson_id =  '" . $lesson->id . "';";
                        $files = $database->select($sql);

                        $lesson->class = getClass($lesson->class_id);
                        $lesson->material = getMaterial($lesson->material_id);

                        $template->assign('lesson', $lesson);
                        $template->assign('files', $files);

                        return $template->fetch('account/lesson/lessonDetail.tpl');
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'add') {
                    $template->assign('classes', getAllClasses());
                    $template->assign('materials', getAllMaterials());

                    if (isset($_POST['add_lesson'])) {
                        $lessonUrl = strtolower(str_replace(' ', '-', $_POST['name']));
                        $lessonUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $lessonUrl);

                        $sql = "SELECT url FROM lesson
                                WHERE url = '" . $database->escape($lessonUrl) . "';";

                        if ($database->query($sql)->fetch_object()) {
                            $counter = 0;

                            do {
                                $counter += 1;
                                $lessonUrl = $lessonUrl . "-" . $counter;

                                $sql = "SELECT url FROM lesson
                                        WHERE url = '" . $database->escape($lessonUrl) . "';";
                            } while ($database->query($sql)->fetch_object());
                        }

                        $visible = 1;
                        if (!isset($_POST['visible'])) {
                            $visible = 0;
                        }

                        $sql = "INSERT INTO lesson (name, url, details, client_id, class_id, material_id, visible)
                                VALUES (
                                    '" . $database->escape($_POST['name']) . "',
                                    '" . $database->escape($lessonUrl) . "',
                                    '" . $database->escape($_POST['details']) . "',
                                    '" . $database->escape($_SESSION['client']['client_id']) . "',
                                    '" . $database->escape($_POST['class']) . "',
                                    '" . $database->escape($_POST['material']) . "',
                                    '" . intval($visible) . "'
                                );";
                        $lessonId = $database->query($sql, true);

                        if ($_FILES['files']['name']) {
                            foreach ($_FILES['files']['name'] as $key => $fileName) {
                                $explodeFileName = explode('.', $fileName);

                                $file_ext = strtolower(end($explodeFileName));

                                $extensions = array("jpeg", "jpg", "png", "pdf");
                                if (in_array($file_ext, $extensions) === false) {
                                    continue;
                                }

                                $hashName = sha1(time() . $fileName) . '.' . $file_ext;
                                move_uploaded_file(
                                    $_FILES['files']['tmp_name'][$key],
                                    $CONF['serverpath'] . "lib/file/lesson/" . $hashName
                                );

                                $sql = "INSERT INTO file (original_name, hash_name, lesson_id)
                                        VALUES (
                                            '" . $database->escape($fileName) . "',
                                            '" . $database->escape($hashName) . "',
                                            '" . intval($lessonId) . "'
                                        );";
                                $database->query($sql);
                            }
                        }

                        $_SESSION['message'] = 'Lectia a fost adaugata.';

                        header("Location: /account/myLessons");
                    }

                    return $template->fetch('account/lesson/lessonAdd.tpl');
                }

                if ($link[3] === 'edit') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "SELECT * FROM lesson WHERE id =  '" . intval($link[4]) . "' AND active = 1 AND
                                    client_id = '" . $database->escape($_SESSION['client']['client_id']) ."';";
                        $lesson = $database->query($sql)->fetch_object();

                        if (empty($lesson)) {
                            return $template->fetch('404.tpl');
                        }

                        $sql = "SELECT * FROM file WHERE lesson_id =  '" . $lesson->id . "'";
                        $files = $database->select($sql);

                        $template->assign('lesson', $lesson);
                        $template->assign('files', $files);
                        $template->assign('classes', getAllClasses());
                        $template->assign('materials', getAllMaterials());

                        if (isset($_POST['edit_lesson'])) {
                            $visible = 1;
                            if (!isset($_POST['visible'])) {
                                $visible = 0;
                            }

                            $sql = "UPDATE lesson
                                    SET name = '" . $database->escape($_POST['name']) . "',
                                        details = '" . $database->escape($_POST['details']) . "',
                                        class_id = '" . $database->escape($_POST['class']) . "',
                                        material_id = '" . $database->escape($_POST['material']) . "',
                                        visible = '" . intval($visible) . "'
                                    WHERE id = '" . intval($link[4]) . "' AND active = 1 AND
                                        client_id = '" . $_SESSION['client']['client_id'] . "';";
                            $database->query($sql);

                            if ($_FILES['files']['name']) {
                                foreach ($_FILES['files']['name'] as $key => $fileName) {
                                    $explodeFileName = explode('.', $fileName);

                                    $file_ext = strtolower(end($explodeFileName));

                                    $extensions = array("jpeg", "jpg", "png", "pdf");
                                    if (in_array($file_ext, $extensions) === false) {
                                        continue;
                                    }

                                    $hashName = sha1(time() . $fileName) . '.' . $file_ext;
                                    move_uploaded_file(
                                        $_FILES['files']['tmp_name'][$key],
                                        $CONF['serverpath'] . "lib/file/lesson/" . $hashName
                                    );

                                    $sql = "INSERT INTO file (original_name, hash_name, lesson_id)
                                            VALUES (
                                                '" . $database->escape($fileName) . "',
                                                '" . $database->escape($hashName) . "',
                                                '" . intval($link[4]) . "'
                                            );";
                                    $database->query($sql);
                                }
                            }

                            $_SESSION['message'] = 'Lectia a fost editata.';

                            header("Location: /account/myLessons");
                        }

                        return $template->fetch('account/lesson/lessonEdit.tpl');
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'enable') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "UPDATE lesson
                                SET visible = 1
                                WHERE id = '" . intval($link[4]) . "' AND visible = 0 AND
                                    client_id = '" . $database->escape($_SESSION['client']['client_id']) ."';";
                        $database->query($sql);

                        $_SESSION['message'] = 'Lectia a fost activata.';

                        header("Location: /account/myLessons");
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'disable') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "UPDATE lesson
                                SET visible = 0
                                WHERE id = '" . intval($link[4]) . "' AND visible = 1 AND
                                    client_id = '" . $database->escape($_SESSION['client']['client_id']) ."';";
                        $database->query($sql);

                        $_SESSION['message'] = 'Lectia a fost dezactivata';

                        header("Location: /account/myLessons");
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'delete') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "UPDATE lesson
                                SET active = 0
                                WHERE id = '" . intval($link[4]) . "' AND active = 1 AND
                                    client_id = '" . $database->escape($_SESSION['client']['client_id']) ."';";
                        $database->query($sql);

                        $_SESSION['message'] = 'Lectia a fost stearsa definitiv.';

                        header("Location: /account/myLessons");
                    }

                    return $template->fetch('404.tpl');
                }

                if ($link[3] === 'remove_favorite') {
                    if (isset($link[4]) && intval($link[4])) {
                        $sql = "DELETE FROM favorite
                                WHERE client_id = '" . $database->escape($_SESSION['client']['client_id']) . "' AND
                                    lesson_id = '" . intval($link[4]) . "';";
                        $database->query($sql);

                        $_SESSION['message'] = 'Lectia a fost eliminata din lista de favorite.';

                        header("Location: /account/myLessons");
                    }

                    return $template->fetch('404.tpl');
                }

                return $template->fetch('404.tpl');
            }

            if ($_SESSION['client']['role'] == 1) {
                $sql = "SELECT lesson.*
                        FROM lesson
                        INNER JOIN favorite ON favorite.lesson_id = lesson.id
                        WHERE favorite.client_id = '" . $_SESSION['client']['client_id'] . "' AND active = 1 AND visible = 1
                        ORDER BY created_at DESC;";
            } else {
                $sql = "SELECT * FROM lesson
                        WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND active = 1
                        ORDER BY created_at DESC;";
            }
            $lessons = $database->select($sql);

            if ($lessons) {
                foreach ($lessons as $key => $lesson) {
                    $lessons[$key]['class'] = getClass($lesson['class_id']);
                    $lessons[$key]['material'] = getMaterial($lesson['material_id']);
                }
            }

            $template->assign('lessons', $lessons);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('account/myLessons.tpl');
        }

        return $template->fetch('404.tpl');
    }

    if (isset($link[2]) && $link[2] === 'personalData') {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            if (isset($link[3]) && $link[3]) {
                if ($link[3] === 'edit') {
                    if (isset($_POST['edit_personal_data'])) {
                        $addressing = NULL;
                        if (!empty($_POST['addressing'])) {
                            $addressing = $database->escape($_POST['addressing']);
                        }

                        $queryAvatarName = '';

                        if ($_FILES['avatar']['name']) {
                            $fileName = $_FILES['avatar']['name'];
                            $explodeFileName = explode('.', $fileName);

                            $file_ext = strtolower(end($explodeFileName));

                            $extensions = array("jpeg", "jpg", "png");
                            if (in_array($file_ext, $extensions) === false) {
                                $error = 'Va rugam sa atasati un fisier de tipul imagine!';

                                $template->assign('error', $error);
                            } else {
                                $hashName = sha1(time() . $fileName) . '.' . $file_ext;
                                move_uploaded_file(
                                    $_FILES['avatar']['tmp_name'],
                                    $CONF['serverpath'] . "images/avatar/" . $hashName
                                );

                                $queryAvatarName = "avatar = '" . $database->escape($hashName) . "',";
                            }
                        }

                        $sql = "UPDATE client
                                SET first_name = '" . $database->escape($_POST['first_name']) . "',
                                    last_name = '" . $database->escape($_POST['last_name']) . "',
                                    phone_number = '" . $database->escape($_POST['phone_number']) . "',
                                    birth_date = '" . $database->escape(date('Y-m-d', strtotime($_POST['birth_date']))) . "',
                                    school = '" . $database->escape($_POST['school']) . "',
                                    class = '" . $database->escape($_POST['class']) . "',
                                    material = '" . $database->escape($_POST['material']) . "',
                                    addressing = '" . $addressing . "',
                                    {$queryAvatarName}
                                    description = '" . $database->escape($_POST['description']) . "'
                                WHERE id = '" . $_SESSION['client']['client_id'] . "';";
                        $database->query($sql);

                        $_SESSION['message'] = 'Datele personale au fost actualizate.';

                        $_SESSION['client']['first_name'] = $_POST['first_name'];
                        $_SESSION['client']['last_name'] = $_POST['last_name'];

                        header("Location: /account/personalData");
                    }

                    $sql = "SELECT * FROM client WHERE id = '" . $_SESSION['client']['client_id'] . "';";
                    $client = $database->query($sql)->fetch_object();

                    if (empty($client)) {
                        die('Something went wrong!');
                    }

                    $template->assign('client', $client);
                    $template->assign('classes', getAllClasses());
                    $template->assign('materials', getAllMaterials());

                    return $template->fetch('account/personalData/personalDataEdit.tpl');
                }

                if ($link[3] === 'changePassword') {
                    if (isset($_POST['change_password'])) {
                        $errors = array();

                        $sql = "SELECT * FROM client WHERE id = '" . $_SESSION['client']['client_id'] . "';";
                        $client = $database->query($sql)->fetch_object();

                        if ($client->password !== md5($_POST['old_password'])) {
                            $errors[] = 'Vechea parola nu coincide!';
                        }
                        if ($_POST['password'] !== $_POST['confirm_password']) {
                            $errors[] = 'Parolele nu coincid!';
                        }

                        if(!empty($errors)) {
                            $template->assign('errors', $errors);
                        } else {
                            $sql = "UPDATE client
                                    SET password = '" . md5($database->escape($_POST['password'])) . "';";
                            $database->query($sql);

                            $_SESSION['message'] = 'Parola contului a fost schimbata.';

                            header("Location: /account/personalData");
                        }
                    }

                    return $template->fetch('account/personalData/personalDataChangePass.tpl');
                }
            }

            $sql = "SELECT * FROM client WHERE id = '" . $_SESSION['client']['client_id'] . "';";
            $client = $database->query($sql)->fetch_object();

            $template->assign('client', $client);
            $template->assign('class', getClass($client->class));
            $template->assign('material', getMaterial($client->material));

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('account/personalData.tpl');
        }

        return $template->fetch('404.tpl');
    }

    if (isset($link[2]) && $link[2] === 'message') {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            if (isset($link[3]) && $link[3]) {
                if ($link[3] === 'view') {
                    if (isset($link[4]) && intval($link[4])) {
                        if (isset($_POST['add_message'])) {
                            $sql = "INSERT INTO message (participant_1, participant_2, message)
                                    VALUES (
                                        '" . $database->escape($_SESSION['client']['client_id']) . "',
                                        '" . $database->escape($link[4]) . "',
                                        '" . $database->escape($_POST['message']) . "'
                                    );";
                            $database->query($sql);

                            header("Location: /account/message/view/" . $link[4]);
                            exit;
                        }

                        $sql = "SELECT *
                                FROM message
                                WHERE
                                    (
                                        participant_1 = '" . intval($link[4]) . "' AND
                                        participant_2 = '" . $database->escape($_SESSION['client']['client_id']) . "'
                                    )
                                    OR
                                    (
                                        participant_1 = '" . $database->escape($_SESSION['client']['client_id']) . "' AND
                                        participant_2 = '" . intval($link[4]) . "'
                                    )
                                ORDER BY created_at ASC;";
                        $messages = $database->select($sql);

                        if (empty($messages)) {
                            return $template->fetch('404.tpl');
                        }

                        $sql = "SELECT *
                                FROM client
                                WHERE id = '" . intval($link[4]) . "';";
                        $participant = $database->query($sql)->fetch_object();

                        $sql = "UPDATE message
                                SET viewed = 1
                                WHERE
                                    (
                                        participant_1 = '" . intval($link[4]) . "' AND
                                        participant_2 = '" . $database->escape($_SESSION['client']['client_id']) . "'
                                    );";
                        $database->query($sql);

                        $template->assign('messages', $messages);
                        $template->assign('participant', $participant);

                        return $template->fetch('account/message/messageDetail.tpl');
                    }

                    return $template->fetch('404.tpl');
                }

                return $template->fetch('404.tpl');
            }

            $sql = "SELECT *
                    FROM message
                    WHERE
                        participant_1 = '" . $database->escape($_SESSION['client']['client_id']) . "' OR 
                        participant_2 = '" . $database->escape($_SESSION['client']['client_id']) . "'
                    ORDER BY created_at DESC;";
            $results = $database->select($sql);

            $conversations = [];
            foreach ($results as $result) {
                if ($result['participant_1'] === $_SESSION['client']['client_id']) {
                    if (!array_key_exists($result['participant_2'], $conversations)) {
                        $conversations[$result['participant_2']] = getClient($result['participant_2']);
                    }
                } elseif ($result['participant_2'] === $_SESSION['client']['client_id']) {
                    if (!array_key_exists($result['participant_1'], $conversations)) {
                        $conversations[$result['participant_1']] = getClient($result['participant_1']);
                    }
                }
            }

            $template->assign('conversations', $conversations);

            return $template->fetch('account/message/messageList.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}
