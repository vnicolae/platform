<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Editeaza banner #{$imageCarousel->id}
    </h4>

    {if isset($error)}
        <div class="alert alert-danger" role="alert">
            {$error}
        </div>
    {/if}

    <div class="mt-4">
        <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="image">
                    Imagine <span class="text-danger font-weight-bold">*</span>
                    <br><strong>1110x500</strong>
                </label>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image"
                               data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                               accept="image/*" onchange="loadFile(event)">
                        <label class="custom-file-label" for="image">
                            {if $imageCarousel->name}{$imageCarousel->name}{else}Alege imagine{/if}
                        </label>
                    </div>
                    <img {if $imageCarousel->name}src="/images/carousel/{$imageCarousel->name}"{/if}
                         id="preview_carousel" style="max-width: 100%;">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="title">
                    Titlu
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="title" name="title"
                           value="{$imageCarousel->title}">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="sub_title">
                    Subtitlu
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="sub_title" name="sub_title"
                           value="{$imageCarousel->sub_title}">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="link">
                    Link
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="link" name="link"
                           value="{$imageCarousel->link}">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="active">
                    Vizibil
                </label>
                <div class="col-sm-8">
                    <input type="checkbox" id="active" name="active" class="custom-control-input" value="1"
                           {if $imageCarousel->active == 1}checked{/if}>
                    <label class="custom-control-label position-relative vertical-webkit-middle" for="active">
                        <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                           title="Daca este bifat, imaginea este vizibila in slider"></i>
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    <button type="submit" class="btn btn-primary btn-custom" name="edit_carousel">Salveaza</button>
                    <a class="btn btn-danger btn-custom" href="/admin/carousel/list" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
