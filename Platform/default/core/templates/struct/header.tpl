<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom fixed-top position-relative">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="//{$CONF.sitepath}images/{$logoSite.logo_site}" style="width: 190px;">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarDropdown" aria-controls="navbarDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarDropdown"
             data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown my-account-dropdown">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-user-circle font-size-22 pr-1"></i> Contul meu
                    </a>
                    <div class="dropdown-menu my-account-dropdown-menu">
                        {if !isset($smarty.session.client)}
                            <a class="dropdown-item" href="/loginAccount">
                                <i class="fas fa-sign-in-alt"></i> Intra in cont
                            </a>
                            <a class="dropdown-item" href="/createAccount">
                                <i class="fas fa-user-plus"></i> Cont nou
                            </a>
                        {else}
                            <div class="text-nowrap p-1 pl-4 pr-4">
                                Salut, {$smarty.session.client.first_name} {$smarty.session.client.last_name}
                            </div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/account/myOrders">
                                <i class="fas fa-file-invoice"></i> Comenzile mele
                            </a>
                            <a class="dropdown-item" href="/account/myAddresses">
                                <i class="fas fa-address-card"></i> Adresele mele
                            </a>
                            <a class="dropdown-item" href="/account/personalData">
                                <i class="fas fa-user-cog"></i> Date personale
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/account/logout">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        {/if}
                    </div>
                </li>

                <li class="nav-item dropdown {if $link.1 != 'myCart'}my-cart-dropdown{/if}">
                    {assign var="htmlTotalQuantity" value=""}
                    {if isset($smarty.session.cart) && !empty($smarty.session.cart)}
                        {assign var="totalQuantity" value=0}
                        {foreach from=$smarty.session.cart item=productCart}
                            {assign var="totalQuantity" value=$totalQuantity + $productCart.quantityCart}
                        {/foreach}
                        {assign var="htmlTotalQuantity" value="<span class='cart-products-counter background-turquoise cursor-pointer text-white rounded-circle text-nowrap text-center font-weight-bold'>$totalQuantity</span>"}
                    {/if}

                    <div id="cart-products-counter">
                        {$htmlTotalQuantity}
                    </div>

                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-shopping-cart font-size-22 pr-1"></i> Cosul meu
                    </a>
                    <div class="dropdown-menu my-cart-dropdown-menu">
                        <div id="cart-info" class="nav-link min-width-250 p-2">
                            {if !isset($smarty.session.cart) || empty($smarty.session.cart)}
                                <div class="text-center w-100">
                                    <small>Nu ai niciun produs in cos</small>
                                </div>
                            {else}
                                <table class="small cart-info-table w-100">
                                    {assign var="totalPrice" value=0}
                                    {foreach from=$smarty.session.cart item=productCart}
                                        {if $productCart.is_discount}
                                            {assign var="totalProductPrice" value=$productCart.quantityCart * $productCart.new_price}
                                        {else}
                                            {assign var="totalProductPrice" value=$productCart.quantityCart * $productCart.price}
                                        {/if}
                                        {assign var="totalPrice" value=$totalPrice + $totalProductPrice}
                                        <tr class="align-top">
                                            <td class="width-30">{$productCart.quantityCart} x</td>
                                            <td>
                                                <a href="/product/{$productCart.url}" class="text-dark">{$productCart.name}</a>
                                            </td>
                                            <td class="width-70 text-right">
                                                {$totalProductPrice|string_format: "%.2f"} Lei
                                                <br>
                                                <a class="btn btn-sm delete-to-cart" role="button" title="Sterge produsul din cos" data-id="{$productCart.id}">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                    <tr class="border-top">
                                        <td colspan="2">Total: {$totalQuantity} produse</td>
                                        <td class="width-65 text-right"> {$totalPrice|string_format: "%.2f"} Lei</td>
                                    </tr>
                                </table>
                            {/if}
                        </div>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/myCart">
                            <i class="fas fa-shopping-basket"></i> Vezi cosul tau
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
