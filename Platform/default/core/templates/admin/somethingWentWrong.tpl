<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Sorry, Something went wrong!
    </h4>

    <div class="col-md-12 mb-3 text-center">
        <h5>Aceasta pagina a fost mutata sau nu mai exista!</h5>
        <a class="btn btn-primary btn-custom" href="/admin" role="button">Intoarce-te la pagina principala</a>
    </div>
</div>
