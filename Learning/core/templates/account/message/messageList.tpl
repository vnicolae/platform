<h2 class="mt-4 mb-3 border-bottom-5">
   Mesaje private
</h2>

<div class="row">
    <div class="col-md-12">
        {if isset($successMessage)}
            <div class="alert alert-success" role="alert">
                {$successMessage}
            </div>
        {/if}
    </div>

    <div class="col-md-12">
        <div class="border rounded-bottom p-3 mb-3">
            {if empty($conversations)}
                <div class="text-center">
                    <h5>Deocamdata nu ai conversatii incepute.</h5>
                </div>
            {else}
                {foreach from=$conversations item=conversation name=conversation}
                    <div class="row">
                        <div class="col-md-12 mb-3" style="display: inline-flex;">
                            <div class="mr-4">
                                <a href="/account/message/view/{$conversation->id}" title="Vezi detalii conversatie" class="text-dark">
                                    <img src="/images/avatar/{$conversation->avatar|default:'default-avatar.jpg'}" class="width-70">
                                </a>
                            </div>
                            <div>
                                <a href="/account/message/view/{$conversation->id}" title="Vezi detalii conversatie" class="text-dark">
                                    <h3>{$conversation->first_name}</h3>
                                </a>
                            </div>
                        </div>
                    </div>

                    {if not $smarty.foreach.conversation.last}
                        <hr>
                    {/if}
                {/foreach}
            {/if}
        </div>
    </div>
</div>

