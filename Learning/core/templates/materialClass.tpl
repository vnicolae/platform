<h2 class="mt-4 mb-3 border-bottom-5">
    {$material->name}
</h2>

<h6 class="mb-3 border-bottom-2">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
    {$material->name}
</h6>

<div class="row">
    {foreach from=$classes item=class}
        <div class="col-lg-3 col-md-4 col-sm-6">
            {include file='materialClassBox.tpl'}
        </div>
    {/foreach}
</div>
