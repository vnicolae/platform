<link rel="stylesheet" type="text/css" href="http://{$CONF.sitepath}lib/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="http://{$CONF.sitepath}lib/index.css">

<div class="border p-3 border-width-5">
    <div class="form-group row header-invoice font-weight-bold font-size-12 position-relative">
        <div class="col-sm-5">
            <table class="table table-borderless table-paddingless m-0">
                <tbody>
                <tr>
                    <td width="50%">Denumire furnizor</td>
                    <td width="50%">{$business->name}</td>
                </tr>
                <tr>
                    <td>Cod unic de identificare</td>
                    <td>{$business->unique_registration_code}</td>
                </tr>
                <tr>
                    <td>Numar registru comert</td>
                    <td>{$business->trade_register_code}</td>
                </tr>
                <tr>
                    <td>Capital social</td>
                    <td>{$business->social_capital}</td>
                </tr>
                <tr>
                    <td>Sediu central</td>
                    <td>{$business->address}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="form-group row text-center font-weight-bold font-size-18">
        <div class="col-sm-12">
            CHITANTA
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-4 m-auto">
            <table class="table table-borderless table-padding-1 font-size-12 font-weight-bold m-0">
                <tr>
                    <td class="align-middle" width="60%">Seria</td>
                    <td class="align-middle" width="40%">
                        {$receipt->series_value}
                    </td>
                </tr>
                <tr>
                    <td class="align-middle" width="60%">Nr. chitanta</td>
                    <td class="align-middle" width="40%">
                        {$receipt->number}
                    </td>
                </tr>
                <tr>
                    <td class="align-middle" width="60%">Data (zi.luna.an)</td>
                    <td class="align-middle" width="40%">
                        {$receipt->release_date|date_format:"%d.%m.%Y"}
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12">
            <table id="table-data" class="table table-padding-1 table-borderless font-size-12">
                <tr>
                    <td width="70%">
                        Am primit de la <strong>{$order->order_json->person->full_name|upper}</strong>
                        <br>
                        Adresa <strong>{$order->order_json->person->address}</strong>
                        <br>
                        Suma de <strong>{$order->order_json->cart->totalPrice|string_format: "%.2f"} Lei</strong>,
                        adica <strong>{$totalPriceLetters}</strong>
                        <br>
                        Reprezentand CV. facturii seria <strong>{$invoice->series_value}</strong>
                        nr. <strong>{$invoice->number}</strong>
                        din data de <strong>{$invoice->release_date|date_format:"%d.%m.%Y"}</strong>
                    </td>
                    <td width="30%">
                        Semnatura si stampila
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>