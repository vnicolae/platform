<h2 class="mt-4 mb-3 border-bottom-5">
    Editeaza lectia #{$lesson->name}
</h2>

{if isset($error)}
    <div class="alert alert-danger" role="alert">
        {$error}
    </div>
{/if}

<div class="mt-4">
    <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="name">
                Titlu lectie <span class="text-danger font-weight-bold">*</span>
            </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name"
                       value="{$lesson->name}" required>
                <div class="invalid-feedback">Introduceti un Titlu valid.</div>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="class">
                Clasa <span class="text-danger font-weight-bold">*</span>
            </label>
            <div class="col-sm-8">
                <select name="class" class="form-control select2" id="class" required>
                    <option value="" selected></option>
                    {foreach from=$classes item=class}
                        <option value="{$class.id}" {if $lesson->class_id eq $class.id}selected{/if}>
                            {$class.name}
                        </option>
                    {/foreach}
                </select>
                <div class="invalid-feedback">Alegeti o Clasa valida.</div>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="material">
                Materie <span class="text-danger font-weight-bold">*</span>
            </label>
            <div class="col-sm-8">
                <select name="material" class="form-control select2" id="material" required>
                    <option value="" selected></option>
                    {foreach from=$materials item=material}
                        <option value="{$material.id}" {if $lesson->material_id eq $material.id}selected{/if}>
                            {$material.name}
                        </option>
                    {/foreach}
                </select>
                <div class="invalid-feedback">Alegeti o Clasa valida.</div>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="visible">
                Vizibil
            </label>
            <div class="col-sm-8">
                <input type="checkbox" id="visible" name="visible" class="custom-control-input" value="1"
                       {if $lesson->visible}checked{/if}>
                <label class="custom-control-label position-relative vertical-webkit-middle" for="visible">
                    <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                       title="Daca este bifat, produsul este vizibil in site"></i>
                </label>
            </div>
        </div>
        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="details">
                Detalii
            </label>
            <div class="col-sm-8">
                <textarea class="form-control" id="details" name="details" rows="3">{$lesson->details}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="offset-1 col-sm-2 col-form-label" for="files">
                Materiale
            </label>
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="files" name="files[]"
                           data-file-types="image/jpeg,image/jpg,image/png,.pdf" lang="ro"
                           accept="image/*,.pdf" onchange="preview_files({$lesson->id});" multiple/>
                    <label class="custom-file-label" for="image">Alege imagine</label>
                </div>
                <div class="row" id="file_preview">
                    {foreach from=$files item=file}
                        <div class="col-md-12">
                            <i class="fas fa-trash-alt bg-danger text-light p-2 mr-2 mt-1 rounded-circle"
                               onClick='deletePreview(this, {$file.id}, {$lesson->id})'></i>
                            {$file.original_name}
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>

        <div class="form-group row">
            <small class="offset-3 col-sm-8">
                Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
            </small>
        </div>

        <div class="form-group row">
            <div class="offset-3 col-sm-8">
                <button type="submit" class="btn btn-primary btn-custom" name="edit_lesson">Salveaza</button>
                <a class="btn btn-danger btn-custom" href="/account/myLessons" role="button">Anuleaza</a>
            </div>
        </div>
    </form>
</div>
