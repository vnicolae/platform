<h2 class="mt-4 mb-3 border-bottom-5">
    {$participant->first_name} {$participant->last_name}
</h2>

<div class="row">
    <div class="col-md-12">
        <div class="border rounded-bottom p-3 mb-3">
            {foreach from=$messages item=message}
                <div class="row">
                    <div class="col-sm-8 {if $message.participant_1 == $smarty.session.client.client_id}offset-sm-4{/if}">
                        <div class="border rounded p-3 mb-3">
                            {if $message.participant_1 == $smarty.session.client.client_id}
                                <b>{$smarty.session.client.first_name}</b>
                            {else}
                                <b>{$participant->first_name}</b>
                            {/if}
                            <span style="float: right;">
                                <i class="far fa-clock"></i>
                                {$message.created_at|date_format:"%d.%m.%Y %H:%M"}
                            </span>
                            <br>

                            {$message.message|nl2br}
                        </div>
                    </div>
                </div>
            {/foreach}
            <hr class="mt-0">

            <form class="needs-validation" method="POST" novalidate="">
                <div class="form-group row">
                    <div class="offset-sm-4 col-sm-8">
                        <textarea class="form-control" id="message" name="message" rows="3" required=""></textarea>
                        <div class="invalid-feedback">Introduceti un Mesaj.</div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-sm-4 col-sm-8">
                        <button type="submit" class="btn btn-primary btn-custom" name="add_message">Trimite</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
