<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Lista chitante
    </h4>

    {if isset($successMessage)}
        <div class="alert alert-success" role="alert">
            {$successMessage}
        </div>
    {/if}

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_receipt').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_receipt" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_receipt)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-6 col-form-label" for="series">
                Serie chitanta
                <select name="series" class="form-control">
                    <option value="" selected></option>
                    {foreach from=$seriesReceipt item=series}
                    <option value="{$series.id}"
                            {if isset($smarty.post.series) && $smarty.post.series eq $series.id}selected{/if}>
                        {$series.value} - {$series.name}
                    </option>
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-6 col-form-label" for="number">
                Numar chitanta
                <input type="text" class="form-control" name="number" id="number"
                       value="{if isset($smarty.post.number)}{$smarty.post.number}{/if}">
            </label>
            <label class="col-sm-6 col-form-label" for="invoice_series">
                Serie factura
                <select name="invoice_series" class="form-control">
                    <option value="" selected></option>
                    {foreach from=$seriesInvoice item=series}
                    <option value="{$series.id}"
                            {if isset($smarty.post.invoice_series) && $smarty.post.invoice_series eq $series.id}selected{/if}>
                        {$series.value} - {$series.name}
                    </option>
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-6 col-form-label" for="invoice_number">
                Numar factura
                <input type="text" class="form-control" name="invoice_number" id="invoice_number"
                       value="{if isset($smarty.post.invoice_number)}{$smarty.post.invoice_number}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="last_name">
                Nume client
                <input type="text" class="form-control" name="last_name" id="last_name"
                       value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                       placeholder="ex: Popescu">
            </label>
            <label class="col-sm-4 col-form-label" for="first_name">
                Prenume client
                <input type="text" class="form-control" name="first_name" id="first_name"
                       value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                       placeholder="ex: Ion">
            </label>
            <label class="col-sm-4 col-form-label" for="order">
                Id comanda
                <input type="text" class="form-control" name="order" id="order"
                       value="{if isset($smarty.post.order)}{$smarty.post.order}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="user">
                Utilizator
                <select name="user" class="form-control">
                    <option value="" selected></option>
                    {foreach from=$users item=user}
                        <option value="{$user.id}"
                                {if isset($smarty.post.user) && $smarty.post.user eq $user.id}selected{/if}>
                            {$user.first_name} {$user.last_name}
                        </option>
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-4 col-form-label" for="receipt_date">
                Data chitanta
                <input type="text" class="datepicker form-control" name="receipt_date" id="receipt_date"
                       data-date-format="dd.mm.yyyy"
                       value="{if isset($smarty.post.receipt_date)}{$smarty.post.receipt_date}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="status">
                Status
                <select name="status" class="form-control">
                    <option value="" selected></option>
                    <option value="active"
                            {if isset($smarty.post.status) && $smarty.post.status eq "active"}selected{/if}>
                        Chitanta activa
                    </option>
                    <option value="inactive"
                            {if isset($smarty.post.status) && $smarty.post.status eq "inactive"}selected{/if}>
                        Chitanta inactiva
                    </option>
                </select>
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_receipt">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Serie Numar</th>
            <th scope="col">Serie Numar Factura</th>
            <th scope="col">Client</th>
            <th scope="col">ID comanda</th>
            <th scope="col">Data emitere</th>
            <th scope="col">Valoare totala</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $receipts}
            {foreach from=$receipts item=receipt}
                <tr>
                    <th scope="row">{$receipt.id}</th>
                    <td>{$receipt.series_value} {$receipt.number}</td>
                    <td>
                        <a href="/admin/invoice/view/{$receipt.invoice_number}"
                           class="text-dark p-2" data-toggle="tooltip" title="Vezi detalii factura">
                            {$receipt.invoice_series_value} {$receipt.invoice_number}
                        </a>
                    </td>
                    <td>
                        <a href="/admin/client/view/{$receipt.client_id}" class="text-dark"
                           data-toggle="tooltip" title="Vezi detalii client">
                            {$receipt.first_name} {$receipt.last_name}
                        </a>
                    </td>
                    <td>
                        <a href="/admin/order/view/{$receipt.order_id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Vezi detalii comanda">
                            {$receipt.order_id}
                        </a>
                    </td>
                    <td>{$receipt.release_date|date_format:"%d.%m.%Y"}</td>
                    <td>{$receipt.total_price} Lei</td>
                    <td>
                        {if $receipt.active == 1}
                            Activ
                        {else}
                            Inactive
                        {/if}
                    </td>
                    <td>
                        <a href="/admin/receipt/view/{$receipt.id}" class="text-dark"
                           data-toggle="tooltip" title="Vezi detalii chitanta" target="_blank">
                            <i class="fas fa-eye"></i>
                        </a>
                        {if $receipt.active == 1 && isset($smarty.session.admin.roles.cancel_receipt)}
                            <a href="/admin/receipt/cancel/{$receipt.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Anuleaza chitanta"
                               onclick="return confirm('Esti sigur ca vrei sa anulezi chitanta?')">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="9">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
