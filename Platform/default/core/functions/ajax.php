<?php

function ajax()
{
    global $db;
    global $link;
    global $template;

    if (isset($link[2]) && $link[2]) {
        // Add to Cart Menu
        if ($link[2] === 'addToCart') {
            if (isset($link[3]) && $link[3] && intval($link[3])) {
                $productId = $link[3];

                if (isset($_SESSION['cart'][$productId])) {
                    $_SESSION['cart'][$productId]['quantityCart']++;
                } else {
                    $sql = "SELECT * FROM product WHERE id = '{$productId}' AND active = 1";
                    $product = $db->select($sql)[0];

                    $product['quantityCart'] = 1;

                    if (!empty($product)) {
                        $_SESSION['cart'][$productId] = $product;
                    }
                }

                $result = getCartResult();

                echo $result;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Delete to Cart Menu
        if ($link[2] === 'deleteToCart') {
            if (isset($link[3]) && $link[3] && intval($link[3])) {
                unset($_SESSION['cart'][$link[3]]);

                $result = getCartResult();

                echo $result;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Synchronise Quantity to Cart Page
        if ($link[2] === 'syncQuantityToCartPage') {
            if (isset($link[3]) && $link[3] && intval($link[3])) {
                $productId = $link[3];

                if (isset($_POST['quantity']) && $_POST['quantity'] && intval($_POST['quantity'])) {
                    $_SESSION['cart'][$productId]['quantityCart'] = $_POST['quantity'];

                    $result = getCartPageResult($_POST['typeDelivery']);

                    echo $result;

                    exit;
                }

                return $template->fetch('404.tpl');
            }

            return $template->fetch('404.tpl');
        }

        // Delete Product to Cart Page
        if ($link[2] === 'deleteProductToCartPage') {
            if (isset($link[3]) && $link[3] && intval($link[3])) {
                unset($_SESSION['cart'][$link[3]]);

                $result = getCartPageResult($_POST['typeDelivery']);

                echo $result;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Synchronise Delivery Price
        if ($link[2] === 'syncDeliveryPrice') {
            if (isset($_POST['typeDelivery']) && $_POST['typeDelivery']) {
                $result = getDeliveryPrice($_POST['typeDelivery']);

                echo $result;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Return Number Of Invoice By Series Id
        if ($link[2] === 'getNumberOfInvoice') {
            if (isset($link[3])) {
                if ($link[3] && intval($link[3])) {
                    $sql = "SELECT number FROM invoice 
                            WHERE series_id = '" . intval($link[3]) . "' 
                            ORDER BY number DESC LIMIT 1";
                    $invoice = $db->query($sql)->fetch_object();

                    if(isset($invoice)) {
                        echo $invoice->number + 1;
                    } else {
                        echo 1;
                    }
                } else {
                    echo '';
                }

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Return Number Of Receipt By Series Id
        if ($link[2] === 'getNumberOfReceipt') {
            if (isset($link[3])) {
                if ($link[3] && intval($link[3])) {
                    $sql = "SELECT number FROM receipt 
                            WHERE series_id = '" . intval($link[3]) . "' 
                            ORDER BY number DESC LIMIT 1";
                    $receipt = $db->query($sql)->fetch_object();

                    if(isset($receipt)) {
                        echo $receipt->number + 1;
                    } else {
                        echo 1;
                    }
                } else {
                    echo '';
                }

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Return Url of Product
        if ($link[2] === 'getUrlProduct') {
            if (isset($_POST['productName']) && $_POST['productName']) {
                $productName = strtolower(str_replace(' ', '-', $_POST['productName']));
                $productName = preg_replace('/[^a-zA-Z0-9-]/', '', $productName);

                $sql = "SELECT url FROM product
                        WHERE url = '" . $db->escape_string($productName) . "'";

                if ($db->query($sql)->fetch_object()) {
                    $counter = 0;

                    do {
                        $counter += 1;
                        $productName = $productName . "-" . $counter;

                        $sql = "SELECT url FROM product
                                WHERE url = '" . $db->escape_string($productName) . "'";
                    } while ($db->query($sql)->fetch_object());
                }

                echo $productName;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Return Url of Category
        if ($link[2] === 'getUrlCategory') {
            if (isset($_POST['categoryName']) && $_POST['categoryName']) {
                $categoryName = strtolower(str_replace(' ', '-', $_POST['categoryName']));
                $categoryName = preg_replace('/[^a-zA-Z0-9-]/', '', $categoryName);

                $sql = "SELECT url FROM category
                        WHERE url = '" . $db->escape_string($categoryName) . "'";

                if ($db->query($sql)->fetch_object()) {
                    $counter = 0;

                    do {
                        $counter += 1;
                        $categoryName = $categoryName . "-" . $counter;

                        $sql = "SELECT url FROM category
                                WHERE url = '" . $db->escape_string($categoryName) . "'";
                    } while ($db->query($sql)->fetch_object());
                }

                echo $categoryName;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}

/**
 * @return string
 */
function getCartResult()
{
    if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
        $result =
            "<div class='width-170 text-center'>" .
                "<small>Nu ai niciun produs in cos</small>" .
            "</div>";
        $cartProductsCounter = "";
    } else {
        $totalQuantity = 0;
        $totalPrice = 0;

        $result = "<table class='small cart-info-table w-100'>";

        foreach ($_SESSION['cart'] as $productCart) {
            $totalQuantity += $productCart['quantityCart'];

            if ($productCart['is_discount']) {
                $totalProductPrice = $productCart['quantityCart'] * $productCart['new_price'];
            } else {
                $totalProductPrice = $productCart['quantityCart'] * $productCart['price'];
            }

            $totalPrice += $totalProductPrice;

            $result .=
                "<tr class='align-top'>" .
                    "<td class='width-30'>" . $productCart['quantityCart'] . " x</td>" .
                    "<td>" .
                        "<a href='/product/" . $productCart['url'] ."' class='text-dark'>" . $productCart['name'] . "</a>" .
                    "</td>" .
                    "<td class='width-70 text-right'>" .
                        number_format($totalProductPrice, 2) . " Lei " .
                        "<br>" .
                        "<a class='btn btn-sm delete-to-cart' title='Sterge produsul din cos' role='button' data-id='" . $productCart['id'] . "'>" .
                            "<i class='fas fa-times'></i>" .
                        "</a>" .
                    "</td>" .
                "</tr>";
        }

        $result .=
            "<tr class='border-top'>" .
                "<td colspan='2'>Total: " . $totalQuantity . " produse</td>" .
                "<td class='width-65 text-right'>" . number_format($totalPrice, 2) . " Lei</td>" .
            "</tr>";

        $result .= "</table>";

        $result .= "
            <script>
                $('a.delete-to-cart').click(function() {
                    $.ajax({
                        url: '/ajax/deleteToCart/' + $(this).attr('data-id'),
                        dataType: 'json',
                        success: function (result) {
                            $('#cart-info').html(result.result);
                            $('#cart-products-counter').html(result.cartProductsCounter);
                        }
                    });
                });
            </script>";

        $cartProductsCounter =
            "<span class='cart-products-counter background-turquoise cursor-pointer text-white rounded-circle text-nowrap text-center font-weight-bold'>" .
            $totalQuantity .
            "</span>";
    }

    return json_encode(array("result" => $result, "cartProductsCounter" => $cartProductsCounter));
}

/**
 * @param $typeDelivery
 *
 * @return string
 */
function getCartPageResult($typeDelivery)
{
    if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
        $result =
            "<div class='text-center'>" .
                "<h5>Nu ai niciun produs in cos</h5>" .
                "<a class='btn btn-primary btn-custom' href='/' role='button'>Intoarce-te la magazin</a>" .
            "</div>";
        $cartProductsCounter = "";
    } else {
        $totalQuantity = 0;
        $totalPrice = 0;

        $result = "";

        foreach ($_SESSION['cart'] as $productCart) {
            $result .=
                "<div class='row'>" .
                    "<div class='col-3'>" .
                        "<div class='text-center'>" .
                            "<div class='overflow-hidden height-120'>";
                                if ($productCart['image']) {
                                    $result .= "<img src='/images/product/" . $productCart['image'] . "' class='img-fluid width-120'>";
                                } else {
                                    $result .= "<img src='/images/product/default-image.jpg' class='img-fluid width-120'>";
                                }

                                $result .=
                            "</div>" .
                        "</div>" .
                    "</div>" .
                    "<div class='col-4'>" .
                        "<div class='font-weight-bold'>" .
                            "<a href='/product/" . $productCart['url'] . "' class='text-dark'>" .
                                $productCart['name'] .
                            "</a>" .
                        "</div>" .
                    "</div>" .
                    "<div class='col-2'>" .
                        "<div class='form-group row align-items-center'>" .
                            "<div class='col-8'>" .
                                "<select class='form-control form-control-sm sync-quantity-to-cart-page' data-id='" . $productCart['id'] . "'>";
                                    for ($i = 1; $i < 26; $i++) {
                                        if ($productCart['quantityCart'] == $i) {
                                            $result .= "<option value='" . $i . "' selected>" . $i . "</option>";
                                        } else {
                                            $result .= "<option value='" . $i . "'>" . $i . "</option>";
                                        }
                                    }

                                    $result .=
                                "</select>" .
                            "</div>" .
                            "<strong>buc</strong>" .
                        "</div>" .
                    "</div>" .
                    "<div class='col-3'>" .
                        "<div class='text-right'>";
                            $totalQuantity += $productCart['quantityCart'];

                            if ($productCart['is_discount']) {
                                $totalProductPrice = $productCart['quantityCart'] * $productCart['new_price'];
                                $totalProductOldPrice = $productCart['quantityCart'] * $productCart['price'];
                                $totalProductEconomy = $totalProductOldPrice - $totalProductPrice;

                                $result .=
                                    "<h4>" . number_format($totalProductPrice, 2) . " Lei</h4>" .
                                    "<h5>" .
                                        "<del>" . number_format($totalProductOldPrice, 2) . " Lei</del>" .
                                    "</h5>" .
                                    "<p>Economisesti:<br>" . number_format($totalProductEconomy, 2) . " Lei</p>";
                            } else {
                                $totalProductPrice = $productCart['quantityCart'] * $productCart['price'];

                                $result .= "<h4>" . number_format($totalProductPrice, 2) . " Lei</h4>";
                            }

                            $result .= "<br>" .
                                "<a class='btn btn-sm position-absolute bottom-0 right-0 pr-3 delete-product-to-cart-page'" .
                                    "title='Sterge produsul din cos' data-id='" . $productCart['id'] . "' role='button'>" .
                                    "Sterge" .
                                "</a>";

                            $totalPrice += $totalProductPrice;

                            $result .=
                        "</div>" .
                    "</div>" .
                "</div>" .
                "<hr>";
        }

        $result .=
            "<div class='row'>" .
                "<div class='col-7 border-right'>" .
                    "<h5>Sumar Comanda</h5>" .
                    "<table class='table table-borderless table-paddingless'>" .
                        "<tr>" .
                            "<td>Cost produse</td>" .
                            "<td class='text-right'>" . number_format($totalPrice, 2) . " Lei</td>" .
                        "</tr>" .
                        "<tr>" .
                            "<td>Cost livrare</td>" .
                            "<td class='text-right' id='deliveryPrice'>";
                                if ($typeDelivery == 'courier') {
                                    $deliveryPrice = number_format(10, 2) . " Lei";
                                    $result .= "<strong class='text-success'>" . $deliveryPrice . "</strong>";
                                } else {
                                    $deliveryPrice = 0;
                                    $result .= "<strong class='text-success'>GRATUIT</strong>";
                                }
                                $result .=
                            "</td>" .
                        "</tr>" .
                    "</table>" .
                "</div>" .
                "<div class='col-5'>" .
                    "<h5>Total</h5>" .
                    "<h4 id='totalPrice'>" .
                            number_format(doubleval($totalPrice) + doubleval($deliveryPrice), 2) . " Lei" .
                    "</h4>" .
                "</div>" .
            "</div>" .
        "</div>";

        $result .= "
            <script>
                $('select.sync-quantity-to-cart-page').change(function () {
                    var typeDelivery = 'personal';
                    if($('#courier').is(':checked')) {
                        var typeDelivery = 'courier';
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/syncQuantityToCartPage/' + $(this).attr('data-id'),
                        data: {
                            quantity: $(this).val(),
                            typeDelivery: typeDelivery
                        },
                        dataType: 'json',
                        success: function (result) {
                            $('#cart-page-info').html(result.result);
                            $('#cart-products-counter').html(result.cartProductsCounter);
                        }
                    });
                });
                $('a.delete-product-to-cart-page').click(function () {
                    var typeDelivery = 'personal';
                    if($('#courier').is(':checked')) {
                        var typeDelivery = 'courier';
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/deleteProductToCartPage/' + $(this).attr('data-id'),
                        data: {
                            typeDelivery: typeDelivery
                        },
                        dataType: 'json',
                        success: function (result) {
                            $('#cart-page-info').html(result.result);
                            $('#cart-products-counter').html(result.cartProductsCounter);
                            }
                    });
                });
            </script>";

        $cartProductsCounter =
            "<span class='cart-products-counter background-turquoise cursor-pointer text-white rounded-circle text-nowrap text-center font-weight-bold'>" .
            $totalQuantity .
            "</span>";
    }

    return json_encode(array("result" => $result, "cartProductsCounter" => $cartProductsCounter));
}

/**
 * @param string $typeDelivery
 *
 * @return string
 */
function getDeliveryPrice(string $typeDelivery)
{
    $totalPrice = 0;
    foreach ($_SESSION['cart'] as $productCart) {
        if ($productCart['is_discount']) {
            $totalProductPrice = $productCart['quantityCart'] * $productCart['new_price'];
        } else {
            $totalProductPrice = $productCart['quantityCart'] * $productCart['price'];
        }

        $totalPrice += $totalProductPrice;
    }
    $totalPrice = number_format($totalPrice, 2) . " Lei";

    $deliveryPrice = "<strong class='text-success' id='deliveryPrice'>GRATUIT</strong>";
    if ($typeDelivery === 'courier') {
        $deliveryPrice = number_format(10, 2) . " Lei";
        $totalPrice = number_format(doubleval($totalPrice) + doubleval($deliveryPrice), 2) . " Lei";
    }

    return json_encode(array("deliveryPrice" => $deliveryPrice, "totalPrice" => $totalPrice));
}
