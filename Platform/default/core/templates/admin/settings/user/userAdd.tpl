<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Adauga utilizator
    </h4>

    <div class="mt-4">
        <form class="needs-validation" method="POST" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="email">
                    Email <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" id="email" name="email" required>
                    <div class="invalid-feedback">Introduceti un Email valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="password">
                    Parola <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="password" name="password" required>
                    <div class="invalid-feedback">Introduceti o Parola valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="last_name">
                    Nume <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="last_name" name="last_name" required>
                    <div class="invalid-feedback">Introduceti un Nume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="first_name">
                    Prenume <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="first_name" name="first_name" required>
                    <div class="invalid-feedback">Introduceti un Prenume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="phone_number">
                    Telefon <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="phone_number" name="phone_number" required>
                    <div class="invalid-feedback">Introduceti un Telefon valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="cnp">
                    CNP <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="cnp" name="cnp" required>
                    <div class="invalid-feedback">Introduceti un CNP valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="ci_series">
                    Serie CI
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="ci_series" name="ci_series">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="ci_number">
                    Numar CI
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="ci_number" name="ci_number">
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <h4 class="pb-3 mt-4 mb-0">
                Drepturi
            </h4>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    {foreach from=$roles item=role}
                        <td>{$role.name}</td>
                    {/foreach}
                </tr>
                </thead>

                <tr>
                    {foreach from=$roles item=role}
                        <td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="{$role.key}" name="roles[{$role.key}]"
                                       class="custom-control-input" value="{$role.id}">
                                <label class="custom-control-label" for="{$role.key}">
                                    <i class="fas fa-info-circle" data-toggle="tooltip" title="{$role.description}"></i>
                                </label>
                            </div>
                        </td>
                    {/foreach}
                </tr>
            </table>

            <div class="form-group row">
                <div class="col-sm-12 text-center">
                    {if isset($smarty.session.admin.roles.access_settings)}
                        <button type="submit" class="btn btn-primary btn-custom" name="add_user">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/settings/user" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
