<?php

function categories()
{
    global $db;
    global $CONF;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_category'])) {
                if ($_POST['category']) {
                    $where .= " AND c.id = '" . $_POST['category'] . "'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND c.active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND c.active = '0'";
                    }
                }
            }

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }
            if (isset($_SESSION['message-error'])) {
                $successMessageError = $_SESSION['message-error'];
                $template->assign('successMessageError', $successMessageError);
                unset($_SESSION['message-error']);
            }

            $sql = "SELECT * FROM category c {$where}";
            $categoriesList = $db->select($sql);

            $template->assign('categoriesList', $categoriesList);
            $template->assign('categories', getAllCategories());

            return $template->fetch('admin/category/categoryList.tpl');
        }

        if ($link[3] === 'add') {
            $template->assign('categories', getAllParentsCategories());

            if (isset($_POST['add_category'])) {
                $categoryUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                $categoryUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $categoryUrl);

                $sql = "SELECT url FROM product
                        WHERE url = '" . $db->escape_string($categoryUrl) . "'";

                if ($db->query($sql)->fetch_object()) {
                    $error = 'Link-ul categoriei trebuie sa fie unic!';

                    $template->assign('error', $error);

                    return $template->fetch('admin/category/categoryAdd.tpl');
                }

                $active = 1;
                if (!isset($_POST['active'])) {
                    $active = 0;
                }
                $category = 0;
                if (isset($_POST['category'])) {
                    $category = $_POST['category'];
                }

                $sql = "INSERT INTO category 
                        (name, url, category_id, active) VALUES (
                        '" . $db->escape_string($_POST['name']) . "',
                        '" . $db->escape_string($_POST['url']) . "',
                        '" . intval($category) . "',
                        '" . intval($active) . "')";
                $db->query($sql);

                $_SESSION['message'] = 'Categoria a fost adaugata';

                header("Location: /admin/categories/list");
            }

            return $template->fetch('admin/category/categoryAdd.tpl');
        }

        if ($link[3] === 'edit') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT * FROM category WHERE id =  '" . intval($link[4]) . "'";
                $categoryEdit = $db->query($sql)->fetch_object();

                if (empty($categoryEdit)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $template->assign('categoryEdit', $categoryEdit);
                $template->assign('categories', getAllParentsCategories());

                if (isset($_POST['edit_category'])) {
                    $categoryUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                    $categoryUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $categoryUrl);

                    $sql = "SELECT url FROM category
                            WHERE url = '" . $db->escape_string($categoryUrl) . "'
                            AND id != '" . intval($link[4]) . "'";

                    if ($db->query($sql)->fetch_object()) {
                        $error = 'Link-ul categoriei trebuie sa fie unic!';

                        $template->assign('error', $error);

                        return $template->fetch('admin/category/categoryEdit.tpl');
                    }

                    $active = 1;
                    if (!isset($_POST['active'])) {
                        $active = 0;
                    }
                    $category = 0;
                    if (isset($_POST['category'])) {
                        $category = $_POST['category'];
                    }

                    $sql = "UPDATE category SET
                            name = '" . $db->escape_string($_POST['name']) . "',
                            url = '" . $db->escape_string($_POST['url']) . "',
                            category_id = '" . intval($category) . "',
                            active = '" . intval($active) . "'
                            WHERE id = '" . intval($link[4]) . "'";
                    $db->query($sql);

                    $_SESSION['message'] = 'Categoria a fost editata';

                    header("Location: /admin/categories/list");
                }

                return $template->fetch('admin/category/categoryEdit.tpl');
            }
        }

        if ($link[3] === 'enable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE category SET active = 1 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 0";
                $db->query($sql);

                $_SESSION['message'] = 'Categoria a fost activata';

                header("Location: /admin/categories/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'disable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE category SET active = 0 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 1";
                $db->query($sql);

                $_SESSION['message'] = 'Categoria a fost dezactivat';

                header("Location: /admin/categories/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
