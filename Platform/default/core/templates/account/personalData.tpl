<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Date personale
</h4>

<div class="row">
    <div class="col-md-12">
        {if isset($successMessage)}
            <div class="alert alert-success" role="alert">
                {$successMessage}
            </div>
        {/if}
    </div>

    <div class="col-md-12">
        <div class="border rounded-bottom p-3 mb-3">
            <div class="row">
                <div class="col-4">
                    <h6>Email</h6>
                </div>
                <div class="col-8">
                    {$client->email}
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <h6>Forma de adresare</h6>
                </div>
                <div class="col-8">
                    {if isset($client->addressing)}
                        {$client->addressing}
                    {else}
                        -
                    {/if}
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <h6>Nume</h6>
                </div>
                <div class="col-8">
                    {$client->last_name}
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <h6>Prenume</h6>
                </div>
                <div class="col-8">
                    {$client->first_name}
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <h6>Telefon</h6>
                </div>
                <div class="col-8">
                    {$client->phone_number}
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a class="btn btn-primary btn-custom" href="/account/personalData/edit" role="button">Editeaza datele</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pt-2">
                    <a class="btn btn-primary btn-custom" href="/account/personalData/changePassword" role="button">Schimba parola</a>
                </div>
            </div>
        </div>
    </div>
</div>