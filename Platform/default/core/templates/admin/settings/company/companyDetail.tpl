<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Date companie
        {if isset($smarty.session.admin.roles.access_settings)}
            <a class="btn btn-primary float-right" href="/admin/settings/company/edit" role="button">
                <i class="fas fa-pencil-alt"></i> Editeaza datele
            </a>
        {/if}
    </h4>

    <div class="border rounded-bottom p-3 mb-3">
        {if isset($successMessage)}
            <div class="alert alert-success" role="alert">
                {$successMessage}
            </div>
        {/if}

        <div class="row">
            <div class="col-4">
                <h6>Nume companie</h6>
            </div>
            <div class="col-8">
                {$business->name}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Adresa completa</h6>
            </div>
            <div class="col-8">
                {$business->address}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Cod unic de identificare</h6>
            </div>
            <div class="col-8">
                {$business->unique_registration_code}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Numar registru comert</h6>
            </div>
            <div class="col-8">
                {$business->trade_register_code}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Capital social</h6>
            </div>
            <div class="col-8">
                {$business->social_capital}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>Nume banca</h6>
            </div>
            <div class="col-8">
                {$business->bank_name}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <h6>IBAN</h6>
            </div>
            <div class="col-8">
                {$business->iban}
            </div>
        </div>
    </div>
</div>
