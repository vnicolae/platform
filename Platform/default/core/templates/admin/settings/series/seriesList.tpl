<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Serii pentru factura
        {if isset($smarty.session.admin.roles.access_settings)}
            <a class="btn btn-primary float-right" href="/admin/settings/series/add-invoice" role="button">
                <i class="fas fa-plus"></i> Adauga serie factura
            </a>
        {/if}
    </h4>

    {if isset($successMessageInvoice)}
        <div class="alert alert-success" role="alert">
            {$successMessageInvoice}
        </div>
    {/if}

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Denumire</th>
            <th scope="col">Valoare</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $seriesInvoices}
            {foreach from=$seriesInvoices item=seriesInvoice}
                <tr>
                    <th scope="row">{$seriesInvoice.id}</th>
                    <td>{$seriesInvoice.name}</td>
                    <td>{$seriesInvoice.value}</td>
                    <td>
                        {if $seriesInvoice.active == 1}
                            Activ
                        {else}
                            Inactiv
                        {/if}
                    </td>
                    <td>
                        {if isset($smarty.session.admin.roles.access_settings)}
                            {if $seriesInvoice.active == 1}
                                <a href="/admin/settings/series/disable-invoice/{$seriesInvoice.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Dezactiveaza seria"
                                   onclick="return confirm('Esti sigur ca vrei sa dezactivezi seria?')">
                                    <i class="fas fa-lock"></i>
                                </a>
                                <a href="/admin/settings/series/edit-invoice/{$seriesInvoice.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Editeaza seria">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            {else}
                                <a href="/admin/settings/series/enable-invoice/{$seriesInvoice.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Activeaza seria"
                                   onclick="return confirm('Esti sigur ca vrei sa activezi seria?')">
                                    <i class="fas fa-unlock"></i>
                                </a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="5">Niciun rezultat</td>
            </tr>
        {/if}
    </table>

    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Serii pentru chitanta
        {if isset($smarty.session.admin.roles.access_settings)}
            <a class="btn btn-primary float-right" href="/admin/settings/series/add-receipt" role="button">
                <i class="fas fa-plus"></i> Adauga serie chitanta
            </a>
        {/if}
    </h4>

    {if isset($successMessageReceipt)}
        <div class="alert alert-success" role="alert">
            {$successMessageReceipt}
        </div>
    {/if}

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Denumire</th>
            <th scope="col">Valoare</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $seriesReceipts}
            {foreach from=$seriesReceipts item=seriesReceipt}
                <tr>
                    <th scope="row">{$seriesReceipt.id}</th>
                    <td>{$seriesReceipt.name}</td>
                    <td>{$seriesReceipt.value}</td>
                    <td>
                        {if $seriesReceipt.active == 1}
                            Activ
                        {else}
                            Inactiv
                        {/if}
                    </td>
                    <td>
                        {if isset($smarty.session.admin.roles.access_settings)}
                            {if $seriesReceipt.active == 1}
                                <a href="/admin/settings/series/disable-receipt/{$seriesReceipt.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Dezactiveaza seria"
                                   onclick="return confirm('Esti sigur ca vrei sa dezactivezi seria?')">
                                    <i class="fas fa-lock"></i>
                                </a>
                                <a href="/admin/settings/series/edit-receipt/{$seriesReceipt.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Editeaza seria">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            {else}
                                <a href="/admin/settings/series/enable-receipt/{$seriesReceipt.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Activeaza seria"
                                   onclick="return confirm('Esti sigur ca vrei sa activezi seria?')">
                                    <i class="fas fa-unlock"></i>
                                </a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="5">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
