<h2 class="mt-4 mb-3 border-bottom-5">
    Profesori
</h2>

<h6 class="mb-3 border-bottom-2">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    {if $link.1 == 'teacher'}
        {if isset($link.2) && $link.2 == 'letter'}
            <a class="text-dark" href="/teacher">Profesori</a> <i class="fas fa-angle-right"></i>
            Litera {$link.3}
        {else}
            Profesori
        {/if}
    {/if}
</h6>

<div class="row">
    <div class="col-md-3">
        {foreach from=$allTeachers key=letter item=teachersLetter}
            {if !empty($teachersLetter)}
                <div class="col-md-12 list_litera">
                    <a class="text-dark" href="/teacher/letter/{$letter}">{$letter}</a>
                </div>
                {assign var="curNumber" value=1}
                {foreach from=$teachersLetter item=teacher}
                    {if $curNumber <= 2}
                        <div>
                            <a class="text-dark" href="/teacher/{$teacher.id}"
                               title="{$teacher.last_name} {$teacher.first_name}">
                                {$teacher.last_name} {$teacher.first_name}
                            </a>
                        </div>
                        {assign var="curNumber" value=$curNumber+1}
                    {/if}
                {/foreach}
                <div class="list_litera_toti">
                    <a class="text-dark" href="/teacher/letter/{$letter}">
                        vezi toti
                    </a>
                </div>
            {/if}
        {/foreach}
    </div>

    <div class="col-md-9">
        <div class="row">
            {if empty($teachers)}
                <div class="col-md-12">
                    Ne pare rau, nu avem profesori in aceasta categorie.
                </div>
            {else}
                {foreach from=$teachers key=letter item=teachersLetter}
                    {if !empty($teachersLetter)}
                        <div class="col-md-12">
                            <div class="list_litera litera">{$letter}</div>
                        </div>
                        {assign var="curNumber" value=1}
                        {foreach from=$teachersLetter item=teacher}
                            {if $curNumber <= 4 || (isset($link.2) && $link.2 == 'letter')}
                                <div class="col-md-3">
                                    {include file='teacherBox.tpl'}
                                </div>
                                {assign var="curNumber" value=$curNumber+1}
                            {/if}
                        {/foreach}
                    {/if}
                {/foreach}
            {/if}
        </div>
    </div>
</div>
