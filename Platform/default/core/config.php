<?php

$CONF['sitepath'] = $_SERVER['HTTP_HOST'] . '/';

$CONF['serverpath'] = dirname(__DIR__) .'/';

$CONF['lib_dir'] = $CONF['serverpath'] . 'lib/';

$CONF['core_dir'] = $CONF['serverpath'] . 'core/';

$CONF['smarty_dir'] = $CONF['serverpath'] . 'core/includes/smarty/libs/';

$CONF['dompdf_dir'] = $CONF['serverpath'] . 'core/includes/dompdf/';

$CONF['phpoffice_dir'] = $CONF['serverpath'] . 'core/includes/phpoffice/';
