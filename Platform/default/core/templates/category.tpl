<h4 class="pb-3 mt-3 mb-3 border-bottom">
    {$category.name}
</h4>

<div class="row">
    {if empty($products)}
        <div class=" item col-md-12">
            Ne pare rau, nu avem produse pentru aceasta categorie.
        </div>
    {else}
        {foreach from=$products item=product}
            <div class=" item col-md-4">
                {include file='productDetail.tpl'}
            </div>
        {/foreach}
    {/if}
</div>
