<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Editeaza Cota TVA
    </h4>

    <div class="mt-4">
        <form class="needs-validation" method="POST" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="name">
                    Denumire <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name"
                           value="{$vat->name}" required>
                    <div class="invalid-feedback">Introduceti o Denumire valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="value">
                    Cota TVA <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="value" name="value"
                           value="{$vat->value}" required>
                    <div class="invalid-feedback">Introduceti o Cota TVA valida.</div>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    {if isset($smarty.session.admin.roles.access_settings)}
                        <button type="submit" class="btn btn-primary btn-custom" name="edit_vat">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/settings/vat" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
