<?php

function lesson()
{
    global $link;
    global $database;
    global $template;
    global $CONF;

    if (isset($link[2]) && $link[2]) {
        if ($link[2] === 'download') {
            if (isset($link[3]) && $link[3]) {
                $filename = $CONF['serverpath'] . 'lib/file/lesson/' . $link[3];

                if (file_exists($filename)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header("Cache-Control: no-cache, must-revalidate");
                    header("Expires: 0");
                    header('Content-Disposition: attachment; filename="' . $link[3] . '"');
                    header('Content-Length: ' . filesize($filename));
                    header('Pragma: public');

                    flush();

                    readfile($filename);

                    die;
                }

                return $template->fetch('404.tpl');
            }

            return $template->fetch('404.tpl');
        }

        if ($link[2] === 'clasa') {
            $classUrl = $database->escape($link[3]);

            $sql = "SELECT * FROM class WHERE url = '{$classUrl}' AND active = 1;";
            $class = $database->query($sql)->fetch_object();

            if (!empty($class)) {
                $page = 1;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                }

                $sql = "SELECT * FROM lesson
                        WHERE visible = 1 AND active = 1 AND class_id = '" . $class->id . "'
                        LIMIT " . ($page - 1) * $CONF['item_per_page'] . ",  " . $CONF['item_per_page'] . ";";
                $lessons = $database->select($sql);

                $sql = "SELECT COUNT(id) AS total FROM lesson
                        WHERE visible = 1 AND active = 1 AND class_id = '" . $class->id . "';";
                $result = $database->query($sql)->fetch_object();

                $template->assign('title', 'Lectii - Clasa ' . $class->name);
                $template->assign('lessons', $lessons);
                $template->assign('page', $page);
                $template->assign('total', $result->total);
                $template->assign('number_page', ceil($result->total / $CONF['item_per_page']));
                $template->assign('redirect_url', $_SERVER['REDIRECT_URL']);
                $template->assign('class', $class);

                return $template->fetch('lessonsFilter.tpl');
            }
        }

        if ($link[2] === 'material') {
            $materialUrl = $database->escape($link[3]);

            $sql = "SELECT * FROM material WHERE url = '{$materialUrl}' AND active = 1;";
            $material = $database->query($sql)->fetch_object();

            if (!empty($material)) {
                $page = 1;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                }

                $sql = "SELECT * FROM lesson
                        WHERE visible = 1 AND active = 1 AND material_id = '" . $material->id . "'
                        LIMIT " . ($page - 1) * $CONF['item_per_page'] . ",  " . $CONF['item_per_page'] . ";";
                $lessons = $database->select($sql);

                $sql = "SELECT COUNT(id) AS total FROM lesson
                        WHERE visible = 1 AND active = 1 AND material_id = '" . $material->id . "';";
                $result = $database->query($sql)->fetch_object();

                $template->assign('title', 'Lectii - Materia ' . $material->name);
                $template->assign('lessons', $lessons);
                $template->assign('page', $page);
                $template->assign('total', $result->total);
                $template->assign('number_page', ceil($result->total / $CONF['item_per_page']));
                $template->assign('redirect_url', $_SERVER['REDIRECT_URL']);
                $template->assign('material', $material);

                return $template->fetch('lessonsFilter.tpl');
            }
        }

        if (!isset($_GET['search']) && !isset($_GET['page'])) {
            $lessonUrl = $database->escape($link[2]);

            $sql = "SELECT * FROM lesson WHERE url = '{$lessonUrl}' AND visible = 1 AND active = 1;";
            $lesson = $database->query($sql)->fetch_object();

            if (!empty($lesson)) {
                if (isset($_POST['add_review'])) {
                    $sql = "INSERT INTO comment (client_id, lesson_id, rating, review)
                        VALUES (
                            '" . $database->escape($_SESSION['client']['client_id']) . "',
                            '" . $database->escape($lesson->id) . "',
                            '" . $database->escape($_POST['rating']) . "',
                            '" . $database->escape($_POST['review']) . "'
                        );";
                    $database->query($sql, true);

                    header("Location: /lesson/" . $lesson->url);
                }

                if (isset($_POST['add_favorite'])) {
                    $sql = "INSERT INTO favorite (client_id, lesson_id)
                        VALUES (
                            '" . $database->escape($_SESSION['client']['client_id']) . "',
                            '" . $database->escape($lesson->id) . "'
                        );";
                    $database->query($sql, true);

                    header("Location: /lesson/" . $lesson->url);
                }

                if (isset($_POST['remove_favorite'])) {
                    $sql = "DELETE FROM favorite
                        WHERE client_id = '" . $database->escape($_SESSION['client']['client_id']) . "' AND
                            lesson_id = '" . $database->escape($lesson->id) . "';";
                    $database->query($sql);

                    header("Location: /lesson/" . $lesson->url);
                }

                $lesson->class = getClass($lesson->class_id);
                $lesson->material = getMaterial($lesson->material_id);
                $lesson->client = getClient($lesson->client_id);
                $lesson->comments = getComments($lesson->id);

                $lesson->rating = 0;
                $lesson->average = 0;
                if ($lesson->comments) {
                    foreach ($lesson->comments as $comment) {
                        $lesson->rating += $comment['rating'];
                    }

                    $lesson->average = $lesson->rating / count($lesson->comments);
                }

                $sql = "SELECT * FROM file WHERE lesson_id = '" . $lesson->id . "';";
                $files = $database->select($sql);

                $isFavorite = 0;
                if (isset($_SESSION['client']) && !empty($_SESSION['client']) && $_SESSION['client']['role'] == 1) {
                    $sql = "SELECT * FROM favorite
                        WHERE client_id = '" . $database->escape($_SESSION['client']['client_id']) . "' AND
                            lesson_id = '" . $database->escape($lesson->id) . "';";
                    $favorite = $database->query($sql)->fetch_object();

                    if ($favorite) {
                        $isFavorite = 1;
                    }
                }

                $template->assign('lesson', $lesson);
                $template->assign('files', $files);
                $template->assign('isFavorite', $isFavorite);

                return $template->fetch('lesson.tpl');
            }

            return $template->fetch('404.tpl');
        }
    }

    $where = '';
    if (isset($_GET['search'])) {
        $where = "AND (
            name LIKE '%" . $database->escape($_GET['search']) . "%' OR
            details LIKE '%" . $database->escape($_GET['search']) . "%'
        )";
    }
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }

    $sql = "SELECT * FROM lesson WHERE visible = 1 AND active = 1 {$where}
         LIMIT " . ($page - 1) * $CONF['item_per_page'] . ",  " . $CONF['item_per_page'] . ";";
    $lessons = $database->select($sql);

    $sql = "SELECT COUNT(id) AS total FROM lesson WHERE visible = 1 AND active = 1 {$where}";
    $result = $database->query($sql)->fetch_object();

    $template->assign('title', 'Lectii');
    $template->assign('lessons', $lessons);
    $template->assign('page', $page);
    $template->assign('total', $result->total);
    $template->assign('number_page', ceil($result->total / $CONF['item_per_page']));
    $template->assign('redirect_url', $_SERVER['REDIRECT_URL']);
    $template->assign('classes', getAllClasses());
    $template->assign('materials', getAllMaterials());

    return $template->fetch('lessons.tpl');
}
