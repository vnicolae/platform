<?php

function createAccount()
{
    global $database;
    global $template;

    if (!isset($_SESSION['client']) || empty($_SESSION['client'])) {
        $errors = array();

        if (isset($_POST['create_account'])) {
            if ($_POST['password'] !== $_POST['confirm_password']) {
                $errors[] = 'Parolele nu coincid!';
            }

            $sql = "SELECT * FROM client WHERE email = '" . $database->escape($_POST['email']) . "';";
            $client = $database->query($sql)->fetch_object();

            if (!empty($client)) {
                $errors[] = 'E-mail este deja folosit!';
            }

            if (!empty($errors)) {
                $template->assign('errors', $errors);
            } else {
                $sql = "INSERT INTO client (email, password, first_name, last_name, birth_date, role) 
                        VALUES (
                            '" . $database->escape($_POST['email']) . "',
                            '" . md5($database->escape($_POST['password'])) . "',
                            '" . $database->escape($_POST['first_name']) . "',
                            '" . $database->escape($_POST['last_name']) . "',
                            '" . $database->escape(date('Y-m-d', strtotime($_POST['birth_date']))) . "',
                            '" . $database->escape($_POST['role']) . "'
                        );";
                $client_id = $database->query($sql, true);

                $_SESSION['client']['client_id'] = $client_id;
                $_SESSION['client']['email'] = $_POST['email'];
                $_SESSION['client']['first_name'] = $_POST['first_name'];
                $_SESSION['client']['last_name'] = $_POST['last_name'];
                $_SESSION['client']['role'] = $_POST['role'];

                header("Location: /");
            }
        }

        return $template->fetch('createAccount.tpl');
    }

    return $template->fetch('404.tpl');
}
