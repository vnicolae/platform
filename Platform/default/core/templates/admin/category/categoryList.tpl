<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Lista categorii
        {if isset($smarty.session.admin.roles.add_category)}
            <a class="btn btn-primary float-right" href="/admin/categories/add" role="button">
                <i class="fas fa-plus"></i> Adauga categorie
            </a>
        {/if}
    </h4>

    {if isset($successMessage)}
        <div class="alert alert-success" role="alert">
            {$successMessage}
        </div>
    {/if}
    {if isset($successMessageError)}
        <div class="alert alert-danger" role="alert">
            {$successMessageError}
        </div>
    {/if}

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_category').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_category" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_category)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-6 col-form-label" for="category">
                Categorie
                <select name="category" class="form-control select2">
                    <option value="" selected></option>
                    {foreach from=$categories item=category}
                        <option value="{$category.id}"
                                {if isset($smarty.post.category) && $smarty.post.category eq $category.id}selected{/if}>
                            {$category.name}
                        </option>
                        {if isset($category.sub_categories)}
                            {foreach from=$category.sub_categories item=subCategory}
                                <option value="{$subCategory.id}"
                                        {if isset($smarty.post.category) && $smarty.post.category eq $subCategory.id}selected{/if}>
                                    &nbsp;&nbsp;&nbsp;&nbsp;{$subCategory.name}
                                </option>
                            {/foreach}
                        {/if}
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-6 col-form-label" for="status">
                Status
                <select name="status" class="form-control">
                    <option value="" selected></option>
                    <option value="active"
                            {if isset($smarty.post.status) && $smarty.post.status eq "active"}selected{/if}>
                        Categorie activa
                    </option>
                    <option value="inactive"
                            {if isset($smarty.post.status) && $smarty.post.status eq "inactive"}selected{/if}>
                        Categorie inactiva
                    </option>
                </select>
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_category">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Categorie</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $categoriesList}
            {assign var="curNumber" value=1}
            {foreach from=$categoriesList item=category}
                <tr>
                    <th scope="row">{$curNumber++}</th>
                    <td>{$category.name}</td>
                    <td>
                        {if $category.active == 1}
                            Activ
                        {else}
                            Inactiv
                        {/if}
                    </td>
                    <td>
                        {if isset($smarty.session.admin.roles.cancel_category)}
                            {if $category.active == 1}
                                <a href="/admin/categories/disable/{$category.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Dezactiveaza categoria"
                                   onclick="return confirm('Esti sigur ca vrei sa dezactivezi categoria?')">
                                    <i class="fas fa-lock"></i>
                                </a>
                            {else}
                                <a href="/admin/categories/enable/{$category.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Activeaza categoria"
                                   onclick="return confirm('Esti sigur ca vrei sa activezi categoria?')">
                                    <i class="fas fa-unlock"></i>
                                </a>
                            {/if}
                        {/if}
                        {if isset($smarty.session.admin.roles.edit_category)}
                            <a href="/admin/categories/edit/{$category.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Editeaza categoria">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="6">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
