<?php

function teachers()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $where = "WHERE role = 5";
            if (isset($_POST['filter_teacher'])) {
                if ($_POST['last_name']) {
                    $where .= " AND last_name LIKE '%" . $database->escape($_POST['last_name']) . "%'";
                }
                if ($_POST['first_name']) {
                    $where .= " AND first_name LIKE '%" . $database->escape($_POST['first_name']) . "%'";
                }
                if ($_POST['phone_number']) {
                    $where .= " AND phone_number LIKE '%" . $database->escape($_POST['phone_number']) . "%'";
                }
                if ($_POST['email']) {
                    $where .= " AND email LIKE '%" . $database->escape($_POST['email']) . "%'";
                }
            }

            $sql = "SELECT * FROM client {$where};";
            $teachers = $database->select($sql);

            $template->assign('teachers', $teachers);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/teacher/teacherList.tpl');
        }

        if ($link[3] === 'view') {
            if (isset($link[4]) && intval($link[4])) {
                $where = "";
                if (isset($_POST['filter_lesson'])) {
                    if ($_POST['class']) {
                        $where .= " AND class_id = '" . intval($_POST['class']) . "'";
                    }
                    if ($_POST['material']) {
                        $where .= " AND material_id = '" . intval($_POST['material']) . "'";
                    }
                    if ($_POST['created_at']) {
                        $where .= " AND created_at >= '" . date('Y-m-d', strtotime($_POST['created_at'])) . "'";
                        $where .= " AND created_at < '" . date('Y-m-d', strtotime($_POST['created_at'] . ' +1 day')) . "'";
                    }
                    if ($_POST['updated_at']) {
                        $where .= " AND updated_at >= '" . date('Y-m-d', strtotime($_POST['updated_at'])) . "'";
                        $where .= " AND updated_at < '" . date('Y-m-d', strtotime($_POST['updated_at'] . ' +1 day')) . "'";
                    }
                    if ($_POST['visible']) {
                        if ($_POST['visible'] === 'visible') {
                            $where .= " AND visible = '1'";
                        } elseif ($_POST['visible'] === 'invisible') {
                            $where .= " AND visible = '0'";
                        }
                    }
                }

                $sql = "SELECT * FROM client 
                        WHERE id = '" . intval($link[4]) . "';";
                $teacher = $database->query($sql)->fetch_object();

                if (empty($teacher)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT *
                        FROM lesson
                        WHERE client_id = '" . intval($link[4]) . "' AND active = 1
                        {$where}
                        ORDER BY created_at DESC;";
                $lessons = $database->select($sql);

                if ($lessons) {
                    foreach ($lessons as $key => $lesson) {
                        $lessons[$key]['class'] = getClass($lesson['class_id']);
                        $lessons[$key]['material'] = getMaterial($lesson['material_id']);
                    }
                }

                $template->assign('teacher', $teacher);
                $template->assign('lessons', $lessons);
                $template->assign('classes', getAllClasses());
                $template->assign('materials', getAllMaterials());

                return $template->fetch('admin/teacher/teacherDetail.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'block') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE client
                        SET block = 1
                        WHERE id = '" . intval($link[4]) . "' AND block = 0;";
                $database->query($sql);

                $_SESSION['message'] = 'Profesorul a fost blocat.';

                header("Location: /admin/teachers/list");
            }

            return $template->fetch('404.tpl');
        }

        if ($link[3] === 'unblock') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE client
                        SET block = 0
                        WHERE id = '" . intval($link[4]) . "' AND block = 1;";
                $database->query($sql);

                $_SESSION['message'] = 'Profesorul a fost deblocat.';

                header("Location: /admin/teachers/list");
            }

            return $template->fetch('404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
