<?php

function loginAccount()
{
    global $db;
    global $template;

    if (!isset($_SESSION['client']) || empty($_SESSION['client'])) {
        $errors = array();

        if (isset($_POST['login_account'])) {
            $sql = "SELECT * FROM client WHERE email = '" . $db->escape_string($_POST['email']) . "'";
            $client = $db->query($sql)->fetch_object();

            if (empty($client)) {
                $errors[] = 'E-mail incorect!';
            } elseif (md5($_POST['password']) != $client->password) {
                $errors[] = 'Parola incorecta!';
            }

            if (!empty($errors)) {
                $template->assign('errors', $errors);

                return $template->fetch('loginAccount.tpl');
            } else {
                $_SESSION['client']['client_id'] = $client->id;
                $_SESSION['client']['email'] = $client->email;
                $_SESSION['client']['first_name'] = $client->first_name;
                $_SESSION['client']['last_name'] = $client->last_name;

                if (isset($_GET['ref']) && $_GET['ref'] === 'myCart') {
                    header("Location: /myCart");
                    exit;
                }

                header("Location: /");
            }
        }

        return $template->fetch('loginAccount.tpl');
    }

    return $template->fetch('404.tpl');
}