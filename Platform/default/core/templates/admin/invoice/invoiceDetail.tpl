<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Detalii factura nr. #{$invoice->number} pentru comanda #{$order->id}
        <a href="/lib/file/invoice/Factura-{$invoice->series_value}-{$invoice->number}.pdf"
           class="text-dark p-2" data-toggle="tooltip" title="Vizualizeaza factura" target="_blank">
            <i class="fas fa-file-pdf"></i>
        </a>
        {if $invoice->active == 1 && !isset($invoice->receipt->id) && isset($smarty.session.admin.roles.add_receipt)}
            <a class="btn btn-primary float-right" href="/admin/receipt/add/{$invoice->id}" role="button">
                <i class="fas fa-pencil-alt"></i> Emite chitanta
            </a>
        {/if}
    </h4>

    <div class="border p-3">
        {if $invoice->reference_id == 0}
            {include file="admin/invoice/invoiceDetailContent.tpl"}
        {else}
            {include file="admin/invoice/invoiceCancelDetailContent.tpl"}
        {/if}
    </div>
</div>
