<?php

function products()
{
    global $db;
    global $CONF;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_product'])) {
                if ($_POST['name']) {
                    $where .= " AND p.name LIKE '%" . $db->escape_string($_POST['name']) . "%'";
                }
                if ($_POST['category']) {
                    $where .= " AND (p.category_id = '" . $_POST['category'] . "' OR 
                                c.category_id = '" . $_POST['category'] ."')";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND p.active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND p.active = '0'";
                    }
                }
            }

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }
            if (isset($_SESSION['message-error'])) {
                $successMessageError = $_SESSION['message-error'];
                $template->assign('successMessageError', $successMessageError);
                unset($_SESSION['message-error']);
            }

            $sql = "SELECT p.*, c.name AS 'category_name' FROM product p 
                    JOIN category c ON p.category_id = c.id
                    LEFT JOIN category pc ON c.category_id = pc.id
                    {$where}
                    ORDER BY p.timestamp DESC";
            $products = $db->select($sql);

            $template->assign('products', $products);
            $template->assign('categories', getAllCategories());

            return $template->fetch('admin/product/productList.tpl');
        }

        if ($link[3] === 'add') {
            $template->assign('categories', getAllCategories());
            $template->assign('vats', getAllVats());

            if (isset($_POST['add_product'])) {
                $productUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                $productUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $productUrl);

                $sql = "SELECT url FROM product
                        WHERE url = '" . $db->escape_string($productUrl) . "'";

                if ($db->query($sql)->fetch_object()) {
                    $error = 'Link-ul produsului trebuie sa fie unic!';

                    $template->assign('error', $error);

                    return $template->fetch('admin/product/productAdd.tpl');
                }

                if (!$_FILES['image']['name']) {
                    $error = 'Va rugam sa atasati un fisier de tipul imagine!';

                    $template->assign('error', $error);

                    return $template->fetch('admin/product/productAdd.tpl');
                }

                $fileName = $_FILES['image']['name'];
                $explodeFileName = explode('.', $fileName);

                $file_ext = strtolower(end($explodeFileName));

                $extensions = array("jpeg", "jpg", "png");
                if (in_array($file_ext, $extensions) === false) {
                    $error = 'Fisierul atasat nu este de tipul imagine!';

                    $template->assign('error', $error);

                    return $template->fetch('admin/product/productAdd.tpl');
                }

                move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    $CONF['serverpath'] . "images/product/" . $fileName
                );

                $active = 1;
                if (!isset($_POST['active'])) {
                    $active = 0;
                }

                $isDiscount = 1;
                if (!isset($_POST['discount'])) {
                    $isDiscount = 0;
                }

                $sql = "INSERT INTO product 
                        (name, url, price, stock, image, details, is_discount, new_price, category_id, vat_id, active) VALUES (
                        '" . $db->escape_string($_POST['name']) . "',
                        '" . $db->escape_string($_POST['url']) . "',
                        '" . $db->escape_string($_POST['price']) . "',
                        '" . $db->escape_string($_POST['stock']) . "',
                        '" . $db->escape_string($fileName) . "',
                        '" . $db->escape_string($_POST['details']) . "',
                        '" . intval($isDiscount) . "',
                        '" . $db->escape_string($_POST['new_price']) . "',
                        '" . $db->escape_string($_POST['category']) . "',
                        '" . $db->escape_string($_POST['vat']) . "',
                        '" . intval($active) . "')";
                $productId = $db->query($sql, true);

                if ($_FILES['images']['name']) {
                    foreach ($_FILES['images']['name'] as $key => $imageName) {
                        $explodeFileName = explode('.', $imageName);

                        $file_ext = strtolower(end($explodeFileName));

                        $extensions = array("jpeg", "jpg", "png");
                        if (in_array($file_ext, $extensions) === false) {
                            continue;
                        }

                        move_uploaded_file(
                            $_FILES['images']['tmp_name'][$key],
                            $CONF['serverpath'] . "images/product/" . $imageName
                        );

                        $sql = "INSERT INTO image (name, product_id) VALUES (
                        '" . $db->escape_string($imageName) . "',
                        '" . intval($productId) . "')";
                        $db->query($sql);
                    }
                }

                $_SESSION['message'] = 'Produsul a fost adaugat';

                header("Location: /admin/products/list");
            }

            return $template->fetch('admin/product/productAdd.tpl');
        }

        if ($link[3] === 'edit') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT * FROM product WHERE id =  '" . intval($link[4]) . "'";
                $product = $db->query($sql)->fetch_object();

                if (empty($product)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $sql = "SELECT * FROM image WHERE product_id =  '" . $product->id . "'";
                $images = $db->select($sql);

                $template->assign('product', $product);
                $template->assign('images', $images);
                $template->assign('categories', getAllCategories());
                $template->assign('vats', getAllVats());

                if (isset($_POST['edit_product'])) {
                    $queryImageName = '';

                    $productUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                    $productUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $productUrl);

                    $sql = "SELECT url FROM product
                            WHERE url = '" . $db->escape_string($productUrl) . "'
                            AND id != '" . intval($link[4]) . "'";

                    if ($db->query($sql)->fetch_object()) {
                        $error = 'Link-ul produsului trebuie sa fie unic!';

                        $template->assign('error', $error);

                        return $template->fetch('admin/product/productEdit.tpl');
                    }

                    if ($_FILES['image']['name']) {
                        $fileName = $_FILES['image']['name'];
                        $explodeFileName = explode('.', $fileName);

                        $file_ext = strtolower(end($explodeFileName));

                        $extensions = array("jpeg", "jpg", "png");
                        if (in_array($file_ext, $extensions) === false) {
                            $error = 'Va rugam sa atasati un fisier de tipul imagine!';

                            $sql = "SELECT * FROM image_carousel WHERE id =  '" . intval($link[4]) . "'";
                            $imageCarousel = $db->query($sql)->fetch_object();

                            $template->assign('imageCarousel', $imageCarousel);
                            $template->assign('error', $error);

                            return $template->fetch('admin/product/productEdit.tpl');
                        }

                        move_uploaded_file(
                            $_FILES['image']['tmp_name'],
                            $CONF['serverpath'] . "images/product/" . $fileName
                        );

                        $queryImageName = "image = '" . $db->escape_string($fileName) . "',";
                    }

                    $active = 1;
                    if (!isset($_POST['active'])) {
                        $active = 0;
                    }

                    $isDiscount = 1;
                    if (!isset($_POST['discount'])) {
                        $isDiscount = 0;
                    }

                    $sql = "UPDATE product SET
                            name = '" . $db->escape_string($_POST['name']) . "',
                            url = '" . $db->escape_string($_POST['url']) . "',
                            price = '" . $db->escape_string($_POST['price']) . "',
                            stock = '" . $db->escape_string($_POST['stock']) . "',
                            $queryImageName
                            details = '" . $db->escape_string($_POST['details']) . "',
                            is_discount = '" . intval($isDiscount) . "',
                            new_price = '" . $db->escape_string($_POST['new_price']) . "',
                            category_id = '" . $db->escape_string($_POST['category']) . "',
                            vat_id = '" . $db->escape_string($_POST['vat']) . "',
                            active = '" . intval($active) . "'
                            WHERE id = '" . intval($link[4]) . "'";
                    $db->query($sql);

                    if ($_FILES['images']['name']) {
                        $sql = "DELETE FROM image WHERE product_id = '" . intval($link[4]) . "'";
                        $db->query($sql);

                        foreach ($_FILES['images']['name'] as $key => $imageName) {
                            $explodeFileName = explode('.', $imageName);

                            $file_ext = strtolower(end($explodeFileName));

                            $extensions = array("jpeg", "jpg", "png");
                            if (in_array($file_ext, $extensions) === false) {
                                continue;
                            }

                            move_uploaded_file(
                                $_FILES['images']['tmp_name'][$key],
                                $CONF['serverpath'] . "images/product/" . $imageName
                            );

                            $sql = "INSERT INTO image (name, product_id) VALUES (
                            '" . $db->escape_string($imageName) . "',
                            '" . intval($link[4]) . "')";
                            $db->query($sql);
                        }
                    }

                    $_SESSION['message'] = 'Produsul a fost editat';

                    header("Location: /admin/products/list");
                }

                return $template->fetch('admin/product/productEdit.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'enable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE product SET active = 1 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 0";
                $db->query($sql);

                $_SESSION['message'] = 'Produsul a fost activata';

                header("Location: /admin/products/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'disable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE product SET active = 0 WHERE 
                        id = '" . intval($link[4]) . "' AND active = 1";
                $db->query($sql);

                $_SESSION['message'] = 'Produsul a fost dezactivat';

                header("Location: /admin/products/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
