<h4 class="pb-3 mt-4 mb-3 border-bottom">
    Admini
    <a class="btn btn-primary float-right btn-custom" href="/admin/settings/user/add" role="button">
        <i class="fas fa-plus"></i> Adauga admin
    </a>
</h4>

{if isset($successMessage)}
    <div class="alert alert-success" role="alert">
        {$successMessage}
    </div>
{/if}

<div class="col-sm-12 text-center mb-3">
    <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
       onclick="$('#filter_user').toggle('slow');">Filtre</a>
</div>

<form id="filter_user" method="POST" novalidate=""
      style="display:{if isset($smarty.post.filter_user)}block{else}none{/if}">
    <div class="form-group row">
        <label class="col-sm-6 col-form-label" for="last_name">
            Nume
            <input type="text" class="form-control" name="last_name" id="last_name"
                   value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                   placeholder="ex: Popescu">
        </label>
        <label class="col-sm-6 col-form-label" for="first_name">
            Prenume
            <input type="text" class="form-control" name="first_name" id="first_name"
                   value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                   placeholder="ex: Ion">
        </label>
        <label class="col-sm-4 col-form-label" for="email">
            Email
            <input type="text" class="form-control" name="email" id="email"
                   value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}">
        </label>
        <label class="col-sm-4 col-form-label" for="phone_number">
            Telefon
            <input type="text" class="form-control" name="phone_number" id="phone_number"
                   value="{if isset($smarty.post.phone_number)}{$smarty.post.phone_number}{/if}">
        </label>
        <label class="col-sm-4 col-form-label" for="status">
            Status
            <select class="form-control" name="status" id="status">
                <option value="" selected></option>
                <option value="active"
                        {if isset($smarty.post.status) && $smarty.post.status eq "active"}selected{/if}>
                    Admin activ
                </option>
                <option value="inactive"
                        {if isset($smarty.post.status) && $smarty.post.status eq "inactive"}selected{/if}>
                    Admin inactiv
                </option>
            </select>
        </label>

        <div class="col-sm-12 mt-2 text-center">
            <button type="submit" class="btn btn-success btn-custom" name="filter_user">Filtreaza</button>
        </div>
    </div>
</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nume</th>
        <th scope="col">Email</th>
        <th scope="col">Telefon</th>
        <th scope="col">Status</th>
        <th scope="col">Actiuni</th>
    </tr>
    </thead>

    {if $users}
        {foreach from=$users item=user}
            <tr>
                <th scope="row">{$user.id}</th>
                <td>{$user.first_name} {$user.last_name}</td>
                <td>{$user.email}</td>
                <td>{$user.phone_number}</td>
                <td>{if $user.active == 1}Activ{else}Inactiv{/if}</td>
                <td>
                    <a href="/admin/settings/user/view/{$user.id}" class="text-dark p-2"
                       data-toggle="tooltip" title="Vizualizeaza adminul">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="/admin/settings/user/edit/{$user.id}" class="text-dark p-2"
                       data-toggle="tooltip" title="Editeaza adminul">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    {if $user.active == 1}
                        <a href="/admin/settings/user/disable/{$user.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Dezactiveaza adminul"
                           onclick="return confirm('Esti sigur ca vrei sa dezactivezi adminul?')">
                            <i class="fas fa-lock"></i>
                        </a>
                    {else}
                        <a href="/admin/settings/user/enable/{$user.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Activeaza adminul"
                           onclick="return confirm('Esti sigur ca vrei sa activezi adminul?')">
                            <i class="fas fa-unlock"></i>
                        </a>
                    {/if}
                </td>
            </tr>
        {/foreach}
    {else}
        <tr class="text-center">
            <td colspan="6">Niciun rezultat</td>
        </tr>
    {/if}
</table>
