<h4 class="pb-3 mt-4 mb-3 border-bottom">
    Lista lectii
</h4>

{if isset($successMessage)}
    <div class="alert alert-success" role="alert">
        {$successMessage}
    </div>
{/if}

<div class="col-sm-12 text-center mb-3">
    <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
       onclick="$('#filter_lesson').toggle('slow');">Filtre</a>
</div>

<form id="filter_lesson" method="POST" novalidate=""
      style="display:{if isset($smarty.post.filter_lesson)}block{else}none{/if}">
    <div class="form-group row">
        <label class="col-sm-6 col-form-label" for="last_name">
            Nume
            <input type="text" class="form-control" name="last_name" id="last_name"
                   value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                   placeholder="ex: Popescu">
        </label>

        <label class="col-sm-6 col-form-label" for="first_name">
            Prenume
            <input type="text" class="form-control" name="first_name" id="first_name"
                   value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                   placeholder="ex: Ion">
        </label>

        <label class="col-sm-4 col-form-label" for="class">
            Clasa
            <select name="class" class="form-control select2" required>
                <option value="" selected></option>
                {foreach from=$classes item=class}
                    <option value="{$class.id}" {if isset($smarty.post.class) && $smarty.post.class eq $class.id}selected{/if}>
                        {$class.name}
                    </option>
                {/foreach}
            </select>
        </label>

        <label class="col-sm-4 col-form-label" for="material">
            Materie
            <select name="material" class="form-control select2" required>
                <option value="" selected></option>
                {foreach from=$materials item=material}
                    <option value="{$material.id}" {if isset($smarty.post.material) && $smarty.post.material eq $material.id}selected{/if}>
                        {$material.name}
                    </option>
                {/foreach}
            </select>
        </label>

        <label class="col-sm-4 col-form-label" for="visible">
            Vizibil
            <select name="visible" class="form-control">
                <option value="" selected></option>
                <option value="visible" {if isset($smarty.post.visible) && $smarty.post.visible eq "visible"}selected{/if}>
                    Da
                </option>
                <option value="invisible" {if isset($smarty.post.visible) && $smarty.post.visible eq "invisible"}selected{/if}>
                    Nu
                </option>
            </select>
        </label>

        <label class="col-sm-4 col-form-label" for="email">
            Data creare
            <input type="text" class="datepicker form-control" name="created_at" id="created_at"
                   data-date-format="dd.mm.yyyy"
                   value="{if isset($smarty.post.created_at)}{$smarty.post.created_at}{/if}">
        </label>
        <label class="col-sm-4 col-form-label" for="email">
            Data modificare
            <input type="text" class="datepicker form-control" name="updated_at" id="updated_at"
                   data-date-format="dd.mm.yyyy"
                   value="{if isset($smarty.post.updated_at)}{$smarty.post.updated_at}{/if}">
        </label>
        <label class="col-sm-4 col-form-label" for="active">
            Status
            <select class="form-control" name="active" id="active">
                <option value="" selected></option>
                <option value="active" {if isset($smarty.post.active) && $smarty.post.active eq "active"}selected{/if}>
                    Activ
                </option>
                <option value="inactive" {if isset($smarty.post.active) && $smarty.post.active eq "inactive"}selected{/if}>
                    Inative
                </option>
            </select>
        </label>

        <div class="col-sm-12 mt-2 text-center">
            <button type="submit" class="btn btn-success btn-custom" name="filter_lesson">Filtreaza</button>
        </div>
    </div>
</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Profesor</th>
        <th scope="col">Nume</th>
        <th scope="col">Clasa</th>
        <th scope="col">Materie</th>
        <th scope="col">Data</th>
        <th scope="col">Vizibil</th>
        <th scope="col">Status</th>
        <th scope="col">Actiuni</th>
    </tr>
    </thead>

    {if $lessons}
        {foreach from=$lessons item=lesson}
            <tr>
                <th scope="row">{$lesson.id}</th>
                <td>
                    <a href="/admin/teachers/view/{$lesson.client_id}" class="text-dark"
                       data-toggle="tooltip" title="Vezi detalii client">{$lesson.first_name} {$lesson.last_name}</a>
                </td>
                <td>{$lesson.name}</td>
                <td>{$lesson.class->name}</td>
                <td>{$lesson.material->name}</td>
                <td>{$lesson.created_at|date_format:"%d.%m.%Y %H:%M"}</td>
                <td>{if $lesson.visible == 1}Da{else}Nu{/if}</td>
                <td>{if $lesson.active == 1}Activ{else}Inactiv{/if}</td>
                <td>
                    <a href="/admin/lessons/view/{$lesson.id}" class="text-dark p-2"
                       data-toggle="tooltip" title="Vezi detalii lectie">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
        {/foreach}
    {else}
        <tr class="text-center">
            <td colspan="9">Niciun rezultat</td>
        </tr>
    {/if}
</table>
