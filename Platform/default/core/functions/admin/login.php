<?php

function login()
{
    global $db;
    global $CONF;
    global $template;

    $error = '';
    if (isset($_POST['enter'])) {
        $email = $db->escape_string($_POST['email']);
        $password = $db->escape_string($_POST['password']);

        $sql = "SELECT * FROM user WHERE email = '{$email}'";
        $account = $db->query($sql)->fetch_object();

        if (empty($account)) {
            $error = 'E-mail incorect!';
        } elseif (md5($password) != $account->password) {
            $error = 'Parola incorecta!';
        } elseif ($account->active != 1) {
            $error = 'Acces limitat!';
        }

        if ($error) {
            $template->assign('error', $error);
        } else {
            $_SESSION['admin']['user_id'] = intval($account->id);
            $_SESSION['admin']['email'] = $account->email;
            $_SESSION['admin']['first_name'] = $account->first_name;
            $_SESSION['admin']['last_name'] = $account->last_name;
            $_SESSION['admin']['type'] = intval($account->type);
            $_SESSION['admin']['business_id'] = intval($account->business_id);
            $_SESSION['admin']['roles'] = getUserRoles($account->id);

            header("Location: /admin");
        }
    }

    $template->assign('logoSite', getLogoSite());
    $template->assign('CONF', $CONF);

    return $template->fetch('admin/login.tpl');
}
