<?php

class template extends Smarty
{
    function generateTemplate()
    {
        global $CONF;

        $this->template_dir = $CONF['core_dir'] . $this->template_dir;
        $this->compile_dir = $CONF['core_dir'] . 'tmp/' . $this->compile_dir;
        $this->config_dir = $CONF['core_dir'] . $this->config_dir;
        $this->cache_dir = $CONF['core_dir'] . 'tmp/' . $this->cache_dir;
        $this->caching = FALSE;

        $this->assign('CONF', $CONF);
    }

    function displayTemplate($page, $cache_id = null, $compile_id = null)
    {
        $res = $this->fetch($page);

        $res = str_replace('href="#', 'href="' . $_SERVER['REQUEST_URI'] . '#', $res);
        $res = str_replace('action=""', 'action="' . $_SERVER['REQUEST_URI'] . '"', $res);

        print $res;
    }
}

?>