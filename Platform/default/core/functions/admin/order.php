<?php

function order()
{
    global $db;
    global $link;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'view') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT * FROM `order` WHERE id = '" . intval($link[4]) . "'";
                $order = $db->query($sql)->fetch_object();

                if (empty($order)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $order->order_json = json_decode($order->order_json);

                $template->assign('order', $order);

                return $template->fetch('admin/order/orderDetail.tpl');
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'cancel') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE `order` SET active = 0
                        WHERE active = 1 AND id = '" . intval($link[4]) . "'";
                $db->query($sql);

                $_SESSION['message'] = 'Comanda cu numarul #' . intval($link[4]) . ' a fost anulata';

                header("Location: /admin");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    $where = "WHERE 1";
    if (isset($_POST['filter_order'])) {
        if ($_POST['last_name']) {
            $where .= " AND c.last_name LIKE '%" . $db->escape_string($_POST['last_name']) . "%'";
        }
        if ($_POST['first_name']) {
            $where .= " AND c.first_name LIKE '%" . $db->escape_string($_POST['first_name']) . "%'";
        }
        if ($_POST['order_id']) {
            $where .= " AND o.id = '" . intval($_POST['order_id']) . "'";
        }
        if ($_POST['order_date']) {
            $where .= " AND o.timestamp >= '" . date('Y-m-d', strtotime($_POST['order_date'])) . "'";
            $where .= " AND o.timestamp < '" . date('Y-m-d', strtotime($_POST['order_date'] . ' +1 day')) . "'";
        }
        if ($_POST['status']) {
            if ($_POST['status'] === 'placed') {
                $where .= " AND o.active = '1'";
            } elseif ($_POST['status'] === 'processed') {
                $where .= " AND o.active = '2'";
            } elseif ($_POST['status'] === 'canceled') {
                $where .= " AND o.active = '0'";
            }
        }
    }

    $sql = "SELECT o.*, c.first_name, c.last_name
            FROM `order` o
            JOIN client c ON c.id = o.client_id
            {$where}
            ORDER BY o.timestamp DESC";
    $orders = $db->select($sql);

    foreach ($orders as $key => $order) {
        $sql = "SELECT i.id, i.number, si.value as 'series_value'
                FROM invoice i
                JOIN series_invoice si ON i.series_id = si.id
                WHERE i.active = 1 AND i.order_id = {$order['id']}";
        $orders[$key]['invoice'] = $db->query($sql)->fetch_assoc();
    }

    if(isset($_SESSION['message'])) {
        $successMessage = $_SESSION['message'];
        $template->assign('successMessage', $successMessage);
        unset($_SESSION['message']);
    }

    $template->assign('orders', $orders);

    return $template->fetch('admin/order/orderList.tpl');
}
