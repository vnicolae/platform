<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Lista comenzi
    </h4>

    {if isset($successMessage)}
        <div class="alert alert-success" role="alert">
            {$successMessage}
        </div>
    {/if}

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_order').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_order" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_order)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-6 col-form-label" for="last_name">
                Nume
                <input type="text" class="form-control" name="last_name" id="last_name"
                       value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                       placeholder="ex: Popescu">
            </label>
            <label class="col-sm-6 col-form-label" for="first_name">
                Prenume
                <input type="text" class="form-control" name="first_name" id="first_name"
                       value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                       placeholder="ex: Ion">
            </label>
            <label class="col-sm-4 col-form-label" for="order_id">
                Id comanda
                <input type="text" class="form-control" name="order_id" id="order_id"
                       value="{if isset($smarty.post.order_id)}{$smarty.post.order_id}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="order_date">
                Data comanda
                <input type="text" class="datepicker form-control" name="order_date" id="order_date"
                       data-date-format="dd.mm.yyyy"
                       value="{if isset($smarty.post.order_date)}{$smarty.post.order_date}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="status">
                Status
                <select class="form-control" name="status" id="status">
                    <option value="" selected></option>
                    <option value="placed"
                            {if isset($smarty.post.status) && $smarty.post.status eq "placed"}selected{/if}>
                        Comanda plasata
                    </option>
                    <option value="processed"
                            {if isset($smarty.post.status) && $smarty.post.status eq "processed"}selected{/if}>
                        Comanda procesata
                    </option>
                    <option value="canceled"
                            {if isset($smarty.post.status) && $smarty.post.status eq "canceled"}selected{/if}>
                        Comanda anulata
                    </option>
                </select>
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_order">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Client</th>
            <th scope="col">Total</th>
            <th scope="col">Data</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $orders}
            {foreach from=$orders item=order}
                <tr>
                    <th scope="row">{$order.id}</th>
                    <td>
                        <a href="/admin/client/view/{$order.client_id}" class="text-dark"
                           data-toggle="tooltip" title="Vezi detalii client">
                            {$order.first_name} {$order.last_name}
                        </a>
                    </td>
                    <td>{$order.total_price} Lei</td>
                    <td>{$order.timestamp|date_format:"%d.%m.%Y %H:%M"}</td>
                    <td>
                        {if $order.active == 1}
                            Comanda plasata
                        {elseif $order.active == 2}
                            Comanda procesata
                        {else}
                            Comanda anulata
                        {/if}
                    </td>
                    <td>
                        <a href="/admin/order/view/{$order.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Vezi detalii comanda">
                            <i class="fas fa-eye"></i>
                        </a>
                        {if $order.active == 1 && !isset($order.invoice.id) && isset($smarty.session.admin.roles.add_invoice)}
                            <a href="/admin/invoice/add/{$order.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Emite factura">
                                <i class="fas fa-file-invoice"></i>
                            </a>
                        {elseif $order.active == 2}
                            <a href="/lib/file/invoice/Factura-{$order.invoice.series_value}-{$order.invoice.number}.pdf"
                               class="text-dark p-2" data-toggle="tooltip" title="Vizualizeaza factura" target="_blank">
                                <i class="fas fa-file-pdf"></i>
                            </a>
                        {/if}
                        {if $order.active == 1 && !isset($order.invoice.id) && isset($smarty.session.admin.roles.cancel_order)}
                            <a href="/admin/order/cancel/{$order.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Anuleaza comanda"
                               onclick="return confirm('Esti sigur ca vrei sa anulezi comanda?')">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="6">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
