<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Editeaza adresa de facturare persoana fizica
</h4>

<form class="needs-validation" method="POST" novalidate autocomplete="off">
    <div class="form-group row">
        <div class="col-sm-12 font-weight-bold pt-2">Persoana de contact</div>
        <label class="col-sm-6 col-form-label" for="full_name">
            Nume si Prenume <span class="text-danger font-weight-bold">*</span>
            <input type="text" class="form-control" id="full_name"
                   placeholder="ex: Popescu Ion" name="full_name"
                   value="{$billing_data_individual_person.full_name}" required>
            <div class="invalid-feedback">Introduceti un Nume si Prenume valid.</div>
        </label>
        <label class="col-sm-6 col-form-label" for="phone_number">
            Telefon <span class="text-danger font-weight-bold">*</span>
            <input type="text" class="form-control" id="phone_number"
                   placeholder="ex: 07xxxxxxxx" name="phone_number"
                   value="{$billing_data_individual_person.phone_number}" required>
            <div class="invalid-feedback">Introduceti un Telefon valid.</div>
        </label>
    </div>

    <div class="form-group row">
        <div class="col-sm-12 font-weight-bold pt-2">Adresa de facturare</div>
        <label class="col-sm-12 col-form-label" for="address">
            Adresa <span class="text-danger font-weight-bold">*</span>
            <input type="text" class="form-control" id="address"
                   placeholder="ex: Strada, numar, bloc, scara, etaj, apartament, judet, localitate"
                   value="{$billing_data_individual_person.address}" name="address" required>
            <div class="invalid-feedback">Introduceti o Adresa valida.</div>
        </label>
    </div>

    <div class="form-group row">
        <small class="col-sm-12">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary btn-custom" name="edit_address_individual_person">Salveaza</button>
        </div>
    </div>
</form>
