<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Editeaza serie factura
    </h4>

    <div class="mt-4">
        <form class="needs-validation" method="POST" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="name">
                    Denumire <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name"
                           value="{$seriesInvoice->name}" required>
                    <div class="invalid-feedback">Introduceti o Denumire valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="value">
                    Serie <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="value" name="value"
                           value="{$seriesInvoice->value}" required>
                    <div class="invalid-feedback">Introduceti o Serie valida.</div>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    {if isset($smarty.session.admin.roles.access_settings)}
                        <button type="submit" class="btn btn-primary btn-custom" name="edit_series_invoice">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/settings/series" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
