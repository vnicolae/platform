<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Cote TVA
        {if isset($smarty.session.admin.roles.access_settings)}
            <a class="btn btn-primary float-right" href="/admin/settings/vat/add" role="button">
                <i class="fas fa-plus"></i> Adauga cota TVA
            </a>
        {/if}
    </h4>

    {if isset($successMessage)}
        <div class="alert alert-success" role="alert">
            {$successMessage}
        </div>
    {/if}

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Denumire</th>
            <th scope="col">Valoare</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $vats}
            {foreach from=$vats item=vat}
                <tr>
                    <th scope="row">{$vat.id}</th>
                    <td>{$vat.name}</td>
                    <td>{$vat.value}</td>
                    <td>
                        {if $vat.active == 1}
                            Activ
                        {else}
                            Inactiv
                        {/if}
                    </td>
                    <td>
                        {if isset($smarty.session.admin.roles.access_settings)}
                            {if $vat.active == 1}
                                <a href="/admin/settings/vat/disable/{$vat.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Dezactiveaza cota TVA"
                                   onclick="return confirm('Esti sigur ca vrei sa dezactivezi cota TVA?')">
                                    <i class="fas fa-lock"></i>
                                </a>
                                <a href="/admin/settings/vat/edit/{$vat.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Editeaza cota TVA">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            {else}
                                <a href="/admin/settings/vat/enable/{$vat.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Activeaza cota TVA"
                                   onclick="return confirm('Esti sigur ca vrei sa activezi cota TVA?')">
                                    <i class="fas fa-unlock"></i>
                                </a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="5">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
