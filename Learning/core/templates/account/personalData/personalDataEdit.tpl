<h2 class="mt-4 mb-3 border-bottom-5">
    Editeaza date personale
</h2>

<form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate autocomplete="off">
    {if isset($errors) && empty($error)}
        <div class="form-group row">
            <div class="offset-4 col-sm-7">
                <div class="alert alert-danger mb-0" role="alert">
                    {foreach from=$errors item=error}
                        <li class="list-group">{$error}</li>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}

    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="addressing">
            Forma de adresare
        </label>
        <div class="col-sm-7">
            <div class="custom-control custom-radio custom-control-inline m-2">
                <input type="radio" id="Dl" name="addressing" class="custom-control-input" value="Dl."
                       {if $client->addressing == 'Dl.'}checked{/if}>
                <label class="custom-control-label" for="Dl">Dl.</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline m-2">
                <input type="radio" id="Dna" name="addressing" class="custom-control-input" value="Dna."
                       {if $client->addressing == 'Dna.'}checked{/if}>
                <label class="custom-control-label" for="Dna">Dna.</label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="last_name">
            Nume <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="form-control" id="last_name" name="last_name"
                   value="{$client->last_name}" required>
            <div class="invalid-feedback">Introduceti un Nume valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="first_name">
            Prenume <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="form-control" id="first_name" name="first_name"
                   value="{$client->first_name}" required>
            <div class="invalid-feedback">Introduceti un Prenume valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="phone_number">
            Telefon
        </label>
        <div class="col-sm-7">
            <input type="tel" class="form-control" id="phone_number" name="phone_number"
                   value="{$client->phone_number}">
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="birth_date">
            Data nasterii <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="datepicker form-control" name="birth_date" id="birth_date"
                   value="{$client->birth_date|date_format:"%d.%m.%Y"}"
                   data-date-format="dd.mm.yyyy" required>
            <div class="invalid-feedback">Introduceti o Data nasterii valida.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="school">
            Unitatea de invatamant
        </label>
        <div class="col-sm-7">
            <input type="tel" class="form-control" name="school" id="school"
                   value="{$client->school}">
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="class">
            Clasa
        </label>
        <div class="col-sm-7">
            <select name="class" class="form-control select2" id="class">
                <option value="" selected></option>
                {foreach from=$classes item=class}
                    <option value="{$class.id}"
                            {if isset($client->class) && $client->class eq $class.id}selected{/if}>
                        {$class.name}
                    </option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="material">
            {if $smarty.session.client.role == 1}Materia preferata{else}Materia predata{/if}
        </label>
        <div class="col-sm-7">
            <select name="material" class="form-control select2" id="material">
                <option value="" selected></option>
                {foreach from=$materials item=material}
                    <option value="{$material.id}"
                            {if isset($client->material) && $client->material eq $material.id}selected{/if}>
                        {$material.name}
                    </option>
                {/foreach}
            </select>
        </div>
    </div>

    {if $smarty.session.client.role == 5}
        <div class="form-group row">
            <label class="offset-1 col-sm-3 col-form-label" for="description">
                Descriere
            </label>
            <div class="col-sm-7">
                <textarea class="form-control" id="description" name="description" rows="3">{$client->description}</textarea>
            </div>
        </div>
    {/if}

    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="avatar">
            Avatar
        </label>
        <div class="col-sm-7">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="avatar" name="avatar"
                       data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                       accept="image/*" onchange="previewImage(event)">
                <label class="custom-file-label" for="avatar">
                    {if $client->avatar}{$client->avatar}{else}Alege avatar{/if}
                </label>
            </div>
            <img {if $client->avatar}src="/images/avatar/{$client->avatar}"{/if}
                 id="preview-image" style="max-width: 100%;">
        </div>
    </div>

    <div class="form-group row">
        <small class="offset-4 col-sm-7">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-7">
            <button type="submit" class="btn btn-primary btn-custom" name="edit_personal_data">Salveaza</button>
        </div>
    </div>
</form>
