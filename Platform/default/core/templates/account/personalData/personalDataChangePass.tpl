<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Schimba parola contului tau
</h4>

<form class="needs-validation" method="POST" novalidate autocomplete="off">
    {if isset($errors) && empty($error)}
        <div class="form-group row">
            <div class="offset-4 col-sm-7">
                <div class="alert alert-danger mb-0" role="alert">
                    {foreach from=$errors item=error}
                        <li class="list-group">{$error}</li>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}

    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="old_password">
            Parola veche <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="password" class="form-control" id="old_password" name="old_password" required>
            <div class="invalid-feedback">Introduceti parola veche.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="password">
            Parola <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="password" class="form-control" id="password" name="password" required>
            <div class="invalid-feedback">Introduceti o Parola valida.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="confirm_password">
            Confirmare parola <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
            <div class="invalid-feedback">Introduceti Confirmarea parolei.</div>
        </div>
    </div>

    <div class="form-group row">
        <small class="offset-4 col-sm-7">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-7">
            <button type="submit" class="btn btn-primary btn-custom" name="change_password">Salveaza</button>
        </div>
    </div>
</form>
