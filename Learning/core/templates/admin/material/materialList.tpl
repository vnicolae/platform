<h4 class="pb-3 mt-4 mb-3 border-bottom">
    Lista materii
    <a class="btn btn-primary float-right btn-custom" href="/admin/materials/add" role="button">
        <i class="fas fa-plus"></i> Adauga materie
    </a>
</h4>

{if isset($successMessage)}
    <div class="alert alert-success" role="alert">
        {$successMessage}
    </div>
{/if}
{if isset($successMessageError)}
    <div class="alert alert-danger" role="alert">
        {$successMessageError}
    </div>
{/if}

<div class="col-sm-12 text-center mb-3">
    <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
       onclick="$('#filter_material').toggle('slow');">Filtre</a>
</div>

<form id="filter_material" method="POST" novalidate=""
      style="display:{if isset($smarty.post.filter_material)}block{else}none{/if}">
    <div class="form-group row">
        <label class="col-sm-6 col-form-label" for="material">
            Materie
            <select name="material" class="form-control select2">
                <option value="" selected></option>
                {foreach from=$materials item=material}
                    <option value="{$material.id}" {if isset($smarty.post.material) && $smarty.post.material eq $material.id}selected{/if}>
                        {$material.name}
                    </option>
                {/foreach}
            </select>
        </label>
        <label class="col-sm-6 col-form-label" for="status">
            Status
            <select name="status" class="form-control">
                <option value="" selected></option>
                <option value="active" {if isset($smarty.post.status) && $smarty.post.status eq "active"}selected{/if}>
                    Materie activa
                </option>
                <option value="inactive" {if isset($smarty.post.status) && $smarty.post.status eq "inactive"}selected{/if}>
                    Materie inactiva
                </option>
            </select>
        </label>

        <div class="col-sm-12 mt-2 text-center">
            <button type="submit" class="btn btn-success btn-custom" name="filter_material">Filtreaza</button>
        </div>
    </div>
</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Materia</th>
        <th scope="col">Status</th>
        <th scope="col">Actiuni</th>
    </tr>
    </thead>

    {if $materialsList}
        {assign var="curNumber" value=1}
        {foreach from=$materialsList item=material}
            <tr>
                <th scope="row">{$curNumber++}</th>
                <td>{$material.name}</td>
                <td>{if $material.active == 1}Activ{else}Inactiv{/if}</td>
                <td>
                    <a href="/admin/materials/edit/{$material.id}" class="text-dark p-2"
                       data-toggle="tooltip" title="Editeaza materia">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    {if $material.active == 1}
                        <a href="/admin/materials/disable/{$material.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Dezactiveaza materia"
                           onclick="return confirm('Esti sigur ca vrei sa dezactivezi materia?')">
                            <i class="fas fa-lock"></i>
                        </a>
                    {else}
                        <a href="/admin/materials/enable/{$material.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Activeaza materia"
                           onclick="return confirm('Esti sigur ca vrei sa activezi materia?')">
                            <i class="fas fa-unlock"></i>
                        </a>
                    {/if}
                </td>
            </tr>
        {/foreach}
    {else}
        <tr class="text-center">
            <td colspan="6">Niciun rezultat</td>
        </tr>
    {/if}
</table>
