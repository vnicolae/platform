<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="//{$CONF.sitepath}lib/images/logo_header.png" style="width: 190px;">
        </a>

        <div>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item font-weight-bold">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
