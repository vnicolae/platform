<div class="border rounded p-3 mb-3">
    <h5 class="pb-1 mb-3 border-bottom">
        Adrese de facturare persoana juridica
        <a class="btn-sm btn-primary margin-top-minus-5 float-right" role="button"
           href="/account/myAddresses/addressLegalPerson/add">
            <i class="fas fa-plus"></i> Adauga adresa
        </a>
    </h5>

    {if empty($billing_data_legal_person)}
        <div class="text-center">
            <h5>Nu ati salvat nicio adresa de livrare.</h5>
        </div>
    {else}
        {foreach from=$billing_data_legal_person item=legal_person name=legal_person}
            <div class="row">
                <div class="col-9">
                    <div class="font-weight-bold">
                        Nume companie si CUI:
                    </div>
                    <div>
                        <span class="person_full_name">{$legal_person.full_name}</span> -
                        <span class="person_phone_number">{$legal_person.unique_registration_code}</span>
                    </div>
                    <div class="font-weight-bold">
                        Sediu central:
                    </div>
                    <div>
                        <span class="person_address">{$legal_person.address}</span>
                    </div>
                </div>

                <div class="col-3 m-auto text-right">
                    <a href="/account/myAddresses/addressLegalPerson/edit/{$legal_person.id}"
                       class="text-dark p-2" data-toggle="tooltip" title="Modifica adresa">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a href="/account/myAddresses/addressLegalPerson/delete/{$legal_person.id}"
                       class="text-dark p-2" data-toggle="tooltip" title="Sterge adresa"
                       onclick="return confirm('Esti sigur ca vrei sa stergi adresa de facturare persoana juridica?')">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </div>
            </div>

            {if not $smarty.foreach.legal_person.last}
                <hr>
            {/if}
        {/foreach}
    {/if}
</div>
