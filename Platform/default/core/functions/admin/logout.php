<?php

function logout()
{
    unset($_SESSION['admin']);

    header("Location: /admin");
}
