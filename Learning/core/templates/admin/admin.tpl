<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="{$CONF.sitepath}">

    <title>{$title|default:$meta_titlu|default:"e-Learning"}</title>

    <link type="text/css" rel="stylesheet" href="//{$CONF.sitepath}lib/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="//{$CONF.sitepath}lib/css/bootstrap-grid.min.css">
    <link type="text/css" rel="stylesheet" href="//{$CONF.sitepath}lib/css/bootstrap-reboot.min.css">

    <script type="text/javascript" src="//{$CONF.sitepath}lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="//{$CONF.sitepath}lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//{$CONF.sitepath}lib/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <link type="text/css" rel="stylesheet" href="//{$CONF.sitepath}lib/index.css">

    <link type="image/png" rel="shortcut icon" href="//{$CONF.sitepath}images/admin/favicon.png">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
</head>

<body>

{if isset($smarty.session.admin.user_id)}
    {include file="admin/struct/header.tpl"}
{/if}

<div class="container" id="content-admin">
    {$content}
</div>

</body>

<script type="text/javascript" src="//{$CONF.sitepath}lib/index.js"></script>

</html>
