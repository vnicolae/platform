<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom fixed-top position-relative">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="//{$CONF.sitepath}images/logo_header.png" style="width: 190px;">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarDropdown" aria-controls="navbarDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarDropdown"
             data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link cursor-pointer font-size-18" href="/lesson/">
                        <i class="fas fa-chalkboard"></i> Lectii
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link cursor-pointer font-size-18" href="/teacher/">
                        <i class="fas fa-user-tie"></i> Profesori
                    </a>
                </li>
                <li class="nav-item dropdown my-account">
                    {if isset($smarty.session.client)}
                        <div id="messages-counter">
                            <span class="messages-counter background-green cursor-pointer text-white rounded-circle text-nowrap text-center font-weight-bold">{$messagesCounter}</span>
                        </div>
                    {/if}

                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-user-circle font-size-22 pr-1"></i> Contul meu
                    </a>

                    <div class="dropdown-menu my-account-section">
                        {if !isset($smarty.session.client)}
                            <a class="dropdown-item" href="/loginAccount">
                                <i class="fas fa-sign-in-alt"></i> Intra in cont
                            </a>
                            <a class="dropdown-item" href="/createAccount">
                                <i class="fas fa-user-plus"></i> Cont nou
                            </a>
                        {else}
                            <div class="text-nowrap p-1 pl-4 pr-4">
                                Salut, {$smarty.session.client.first_name}
                            </div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/account/myLessons">
                                {if $smarty.session.client.role == 1}
                                    <i class="fas fa-heart"></i> Lectiile preferate
                                {else}
                                    <i class="fas fa-book"></i> Lectiile mele
                                {/if}
                            </a>
                            <a class="dropdown-item" href="/account/message">
                                <i class="fas fa-mail-bulk"></i>
                                Mesaje
                                <span class="messages-counter-text background-green cursor-pointer text-white rounded-circle text-nowrap text-center font-weight-bold">
                                    {$messagesCounter}
                                </span>
                            </a>
                            <a class="dropdown-item" href="/account/personalData">
                                <i class="fas fa-user-cog"></i> Date personale
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/account/logout">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        {/if}
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
