<div class="container" id="content-platform-container">
    <form method="POST" novalidate="">
        <h4 class="pb-3 mt-4 mb-3 border-bottom">
            Rapoarte
            <button type="submit" class="btn btn-primary float-right ml-2" name="export_pdf">
                <i class="fas fa-file-pdf"></i> Export PDF
            </button>
            <button type="submit" class="btn btn-success float-right ml-2" name="export_excel">
                <i class="fas fa-file-excel"></i> Export Excel
            </button>
        </h4>

        <div class="col-sm-12 text-center mb-3">
            <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
               onclick="$('#filter_raport').toggle('slow');">Arata filtre</a>
        </div>

        <div id="filter_raport" style="display:{if isset($smarty.post.filter_raport)}block{else}none{/if}">
            <div class="form-group row">
                <label class="col-sm-6 col-form-label" for="invoice_series">
                    Serie factura
                    <select name="invoice_series" class="form-control">
                        <option value="" selected></option>
                        {foreach from=$seriesInvoice item=series}
                            <option value="{$series.id}"
                                    {if isset($smarty.post.invoice_series) && $smarty.post.invoice_series eq $series.id}selected{/if}>
                                {$series.value} - {$series.name}
                            </option>
                        {/foreach}
                    </select>
                </label>
                <label class="col-sm-6 col-form-label" for="invoice_number">
                    Numar factura
                    <input type="text" class="form-control" name="invoice_number" id="invoice_number"
                           value="{if isset($smarty.post.invoice_number)}{$smarty.post.invoice_number}{/if}">
                </label>
                <label class="col-sm-4 col-form-label" for="last_name">
                    Nume client
                    <input type="text" class="form-control" name="last_name" id="last_name"
                           value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                           placeholder="ex: Popescu">
                </label>
                <label class="col-sm-4 col-form-label" for="first_name">
                    Prenume client
                    <input type="text" class="form-control" name="first_name" id="first_name"
                           value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                           placeholder="ex: Ion">
                </label>
                <label class="col-sm-4 col-form-label" for="email">
                    Email client
                    <input type="text" class="form-control" name="email" id="email"
                           value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}">
                </label>
                <label class="col-sm-4 col-form-label" for="order">
                    Id comanda
                    <input type="text" class="form-control" name="order" id="order"
                           value="{if isset($smarty.post.order)}{$smarty.post.order}{/if}">
                </label>
                <label class="col-sm-4 col-form-label" for="min_date">
                    Data minima
                    <input type="text" class="datepicker form-control" name="min_date" id="min_date"
                           data-date-format="dd.mm.yyyy"
                           value="{if isset($smarty.post.min_date)}{$smarty.post.min_date}{/if}">
                </label>
                <label class="col-sm-4 col-form-label" for="max_date">
                    Data maxima
                    <input type="text" class="datepicker form-control" name="max_date" id="max_date"
                           data-date-format="dd.mm.yyyy"
                           value="{if isset($smarty.post.max_date)}{$smarty.post.max_date}{/if}">
                </label>
                <label class="col-sm-4 col-form-label" for="user">
                    Utilizator
                    <select name="user" class="form-control">
                        <option value="" selected></option>
                        {foreach from=$users item=user}
                            <option value="{$user.id}"
                                    {if isset($smarty.post.user) && $smarty.post.user eq $user.id}selected{/if}>
                                {$user.first_name} {$user.last_name}
                            </option>
                        {/foreach}
                    </select>
                </label>
                <label class="col-sm-4 col-form-label" for="min_due_date">
                    Data scadenta minima
                    <input type="text" class="datepicker form-control" name="min_due_date" id="min_due_date"
                           data-date-format="dd.mm.yyyy"
                           value="{if isset($smarty.post.min_due_date)}{$smarty.post.min_due_date}{/if}">
                </label>
                <label class="col-sm-4 col-form-label" for="max_due_date">
                    Data scadenta maxima
                    <input type="text" class="datepicker form-control" name="max_due_date" id="max_due_date"
                           data-date-format="dd.mm.yyyy"
                           value="{if isset($smarty.post.max_due_date)}{$smarty.post.max_due_date}{/if}">
                </label>

                <div class="col-sm-12 mt-2">
                    <button type="submit" class="btn btn-primary btn-custom" name="filter_raport">Filtreaza</button>
                </div>
            </div>
        </div>
    </form>

    {include file="admin/raport/raportTable.tpl"}
</div>
