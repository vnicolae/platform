$(document).ready(function () {
    // Enable Carousel Controls
    $(".prev-slider").click(function () {
        $("#demo").carousel("prev");
    });

    $(".next-slider").click(function () {
        $("#demo").carousel("next");
    });
});

$("li.my-account").hover(function () {
    const section = $(this).find('.my-account-section');
    section.stop(true, true).delay(100).fadeIn(300);
}, function () {
    const section = $(this).find('.my-account-section');
    section.stop(true, true).delay(100).fadeOut(300);
});

// Disable form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$('.datepicker').datepicker({
    weekStart: 1,
    daysOfWeekHighlighted: "6,0",
    autoclose: true,
    todayHighlight: true
});

// Tooltip
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
const previewImage = function (e) {
    const file = e.target.files[0];
    const output = document.getElementById('preview-image');
    output.src = URL.createObjectURL(file);
};

function deletePreview(ele, i, lessonId) {
    try {
        $(ele).parent().remove();
        // window.filesToUpload.splice(i, 1);

        $.ajax({
            type: 'POST',
            url: "/ajax/deleteFileFromLesson",
            data: {
                fileId: i,
                lessonId: lessonId,
            },
            success: function (result) {
                console.log(result);
            }
        });
    } catch (e) {
        console.log(e.message);
    }
}

function preview_files(lessonId) {
    var total_file = document.getElementById("files").files.length;
    var preview = '';
    for (var i = 0; i < total_file; i++) {
        $('#file_preview').append(
            "<div class='col-md-12'>" +
            "<i class='fas fa-trash-alt bg-danger text-light p-2 mr-2 mt-1 rounded-circle' onClick='deletePreview(this, " + i + ", " + lessonId + ")'></i>" +
            event.target.files[i].name +
            "</div>"
        );
    }
}

// Return Url of Class
$('input[name="name"].class-name').change(function () {
    $.ajax({
        type: 'POST',
        url: "/ajax/getUrlClass",
        data: {
            className: $(this).val(),
        },
        success: function (result) {
            $('input[name="url"].class-url').val(result);
        }
    });
});

// Return Url of Material
$('input[name="name"].material-name').change(function () {
    $.ajax({
        type: 'POST',
        url: "/ajax/getUrlMaterial",
        data: {
            materialName: $(this).val(),
        },
        success: function (result) {
            $('input[name="url"].material-url').val(result);
        }
    });
});
