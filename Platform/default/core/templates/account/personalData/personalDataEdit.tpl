<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Editeaza date personale
</h4>

<form class="needs-validation" method="POST" novalidate autocomplete="off">
    {if isset($errors) && empty($error)}
        <div class="form-group row">
            <div class="offset-4 col-sm-7">
                <div class="alert alert-danger mb-0" role="alert">
                    {foreach from=$errors item=error}
                        <li class="list-group">{$error}</li>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}

    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="addressing">
            Forma de adresare
        </label>
        <div class="col-sm-7">
            <div class="custom-control custom-radio custom-control-inline m-2">
                <input type="radio" id="Dl" name="addressing" class="custom-control-input" value="Dl."
                       {if $client->addressing == 'Dl.'}checked{/if}>
                <label class="custom-control-label" for="Dl">Dl.</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline m-2">
                <input type="radio" id="Dna" name="addressing" class="custom-control-input" value="Dna."
                       {if $client->addressing == 'Dna.'}checked{/if}>
                <label class="custom-control-label" for="Dna">Dna.</label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="last_name">
            Nume <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="form-control" id="last_name" name="last_name"
                   value="{$client->last_name}" required>
            <div class="invalid-feedback">Introduceti un Nume valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="first_name">
            Prenume <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="form-control" id="first_name" name="first_name"
                   value="{$client->first_name}" required>
            <div class="invalid-feedback">Introduceti un Prenume valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="phone_number">
            Telefon <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="tel" class="form-control" id="phone_number" name="phone_number"
                   value="{$client->phone_number}" required>
            <div class="invalid-feedback">Introduceti un Telefon valid.</div>
        </div>
    </div>

    <div class="form-group row">
        <small class="offset-4 col-sm-7">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-7">
            <button type="submit" class="btn btn-primary btn-custom" name="edit_personal_data">Salveaza</button>
        </div>
    </div>
</form>
