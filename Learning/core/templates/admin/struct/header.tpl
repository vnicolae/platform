<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    <div class="container">
        <a class="navbar-brand" href="/admin">
            <img src="//{$CONF.sitepath}images/admin/logo_header.png" style="width: 190px;">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarDropdown" aria-controls="navbarDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarDropdown"
             data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown my-account">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-user-graduate"></i> Clienti
                    </a>
                    <div class="dropdown-menu my-account-section">
                        <a class="dropdown-item" href="/admin/students/list">
                            <i class="fas fa-child"></i> Elevi
                        </a>
                        <a class="dropdown-item" href="/admin/teachers/list">
                            <i class="fas fa-user-tie"></i> Profesori
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown my-account">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-cogs"></i> Setari
                    </a>
                    <div class="dropdown-menu my-account-section">
                        <a class="dropdown-item" href="/admin/settings/user">
                            <i class="fas fa-users"></i> Admini
                        </a>
                        <a class="dropdown-item" href="/admin/classes/list">
                            <i class="fas fa-graduation-cap"></i> Clase
                        </a>
                        <a class="dropdown-item" href="/admin/materials/list">
                            <i class="fas fa-chalkboard"></i> Materii
                        </a>
                        <a class="dropdown-item" href="/admin/carousel/list">
                            <i class="fas fa-images"></i> Carusel
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown my-account">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-user-circle font-size-22 pr-1"></i> Contul meu
                    </a>
                    <div class="dropdown-menu my-account-section">
                        <div class="text-nowrap p-1 pl-4 pr-4">
                            Salut, {$smarty.session.admin.first_name}
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/admin/logout">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>