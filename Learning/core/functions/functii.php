<?php

/**
 * @param $data
 */
function print_ra($data)
{
    echo "<pre>";

    print_r($data);

    echo "</pre>";
}

/**
 * @return array
 */
function getAllCategories()
{
    global $database;

    $sql = "SELECT * FROM category where category_id = 0 AND active = 1";
    $categories = $database->select($sql);

    foreach ($categories as $key => $category) {
        $sql = "SELECT * FROM category where category_id = {$category['id']} AND active = 1";
        $subCategories = $database->select($sql);

        if ((count($subCategories))) {
            $categories[$key]['sub_categories'] = $subCategories;
        }
    }

    return $categories;
}

/**
 * @return array
 */
function getAllClasses()
{
    global $database;

    $sql = "SELECT * FROM class WHERE active = 1;";
    $classes = $database->select($sql);

    return $classes;
}

/**
 * @param $classId
 * @return object
 */
function getClass($classId)
{
    global $database;

    if (!$classId) {
        return false;
    }

    $sql = "SELECT * FROM class WHERE active = 1 AND id = '{$classId}';";
    $class = $database->query($sql)->fetch_object();

    return $class;
}

/**
 * @return array
 */
function getAllMaterials()
{
    global $database;

    $sql = "SELECT * FROM material WHERE active = 1;";
    $materials = $database->select($sql);

    return $materials;
}

/**
 * @param $materialId
 * @return object
 */
function getMaterial($materialId)
{
    global $database;

    if (!$materialId) {
        return false;
    }

    $sql = "SELECT * FROM material WHERE active = 1 AND id = '{$materialId}';";
    $material = $database->query($sql)->fetch_object();

    return $material;
}

/**
 * @param $clientId
 * @return object
 */
function getClient($clientId)
{
    global $database;

    if (!$clientId) {
        return false;
    }

    $sql = "SELECT * FROM client WHERE id = '{$clientId}';";
    $client = $database->query($sql)->fetch_object();

    return $client;
}

/**
 * @param $teacherId
 * @return array
 */
function getLessons($teacherId)
{
    global $database;

    if (!$teacherId) {
        return false;
    }

    $sql = "SELECT *
            FROM lesson
            WHERE client_id = '{$teacherId}' AND visible = 1 AND active = 1;";
    $lessons = $database->select($sql);

    return $lessons;
}

/**
 * @param $lessonId
 * @return array
 */
function getComments($lessonId)
{
    global $database;

    if (!$lessonId) {
        return false;
    }

    $sql = "SELECT comment.*, client.first_name, client.last_name
            FROM comment
            INNER JOIN client ON client.id = comment.client_id
            WHERE comment.lesson_id = '{$lessonId}';";
    $comments = $database->select($sql);

    return $comments;
}

/**
 * @param $letter
 * @return array
 */
function getTeachersByLetter($letter)
{
    global $database;

    $sql = "SELECT * FROM client WHERE role = 5 AND last_name LIKE '" . $letter . "%' ORDER BY last_name;";
    $materials = $database->select($sql);

    return $materials;
}

/**
 * @return array
 */
function getImagesCarousel()
{
    global $database;

    $sql = "SELECT * FROM image_carousel WHERE active = 1";
    $imagesCarousel = $database->select($sql);

    return $imagesCarousel;
}

/**
 * @param $clientId
 * @return object
 */
function getUnreadMessages($clientId)
{
    global $database;

    if (!$clientId) {
        return false;
    }

    $sql = "SELECT COUNT(id) AS count FROM message WHERE participant_2 = '{$clientId}' AND viewed = 0;";
    $count = $database->query($sql)->fetch_object();

    return $count->count;
}
