<h4 class="pb-3 mt-4 mb-3 border-bottom">
    Admin #{$user->first_name} {$user->last_name}
    <a class="btn btn-primary float-right btn-custom" href="/admin/settings/user/edit/{$user->id}" role="button">
        <i class="fas fa-pencil-alt"></i> Editeaza adminul
    </a>
</h4>

<div class="border rounded-bottom p-3 mb-3">
    <div class="row">
        <div class="col-4">
            <h6>Email</h6>
        </div>
        <div class="col-8">
            {$user->email}
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <h6>Nume</h6>
        </div>
        <div class="col-8">
            {$user->last_name}
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <h6>Prenume</h6>
        </div>
        <div class="col-8">
            {$user->first_name}
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <h6>Telefon</h6>
        </div>
        <div class="col-8">
            {$user->phone_number}
        </div>
    </div>
</div>
