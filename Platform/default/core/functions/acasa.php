<?php
function acasa()
{
    global $template;

    $template->assign('products', getAllProducts());

    return $template->fetch('home.tpl');
}
