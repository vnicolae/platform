<h4 class="pb-3 mt-3 mb-3 border-bottom">
   {$product.name}
</h4>

<h6 class="pb-1 mb-3 border-bottom">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    <a class="text-dark" href="/category/{$product.category_url}">{$product.category_name}</a> <i class="fas fa-angle-right"></i>
    {$product.name}
</h6>

<div class="row">
    <div class="col-md-6">
        <div id="product" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
                <li data-target="#product" data-slide-to="0" class="active"></li>

                {assign var="curNumber" value=1}
                {foreach from=$product.images item=image}
                    <li data-target="#product" data-slide-to="{$curNumber++}"></li>
                {/foreach}
            </ul>
            <div class="carousel-inner max-height-350 mb-3">
                <div class="carousel-item active">
                    <img src="/images/product/{$product.image|default:'default-image.jpg'}" class="img-fluid">
                </div>
                {foreach from=$product.images item=image}
                    <div class="carousel-item">
                        <img src="/images/product/{$image.name}" class="img-fluid">
                    </div>
                {/foreach}
            </div>
            <a class="carousel-control-prev prev-product" data-slide="prev">
                <i class="fas fa-angle-left"></i>
            </a>
            <a class="carousel-control-next next-product" data-slide="next">
                <i class="fas fa-angle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-md-6">
        <div class="text-center mt-4">
            {if $product.is_discount}
                <h5><del>{$product.price} Lei</del></h5>
                <h4>{$product.new_price} Lei</h4>
            {else}
                <h5>&nbsp;</h5>
                <h4>{$product.price} Lei</h4>
            {/if}

            <a class="btn btn-danger btn-custom font-weight-bold text-white" id="add-to-cart" role="button" data-id="{$product.id}">
                <i class="fas fa-cart-plus"></i> Adauga in cos
            </a>
        </div>
    </div>

    <div class="col-md-12">
        <h5>Detalii</h5>
        <p>{$product.details}</p>
    </div>
</div>
