<h2 class="mt-4 mb-3 border-bottom-5">
    {if $smarty.session.client.role == 1}Lectiile preferate{else}Lectiile mele{/if}

    {if $smarty.session.client.role == 5}
        <a class="btn btn-primary float-right btn-custom" role="button" href="/account/myLessons/add">
            <i class="fas fa-plus"></i> Adauga lectie
        </a>
    {/if}
</h2>

<div class="row">
    <div class="col-md-12">
        {if isset($successMessage)}
            <div class="alert alert-success" role="alert">
                {$successMessage}
            </div>
        {/if}
    </div>

    <div class="col-md-12">
        {include file="account/lesson/lessonList.tpl"}
    </div>
</div>
