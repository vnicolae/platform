<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Lista produse
        {if isset($smarty.session.admin.roles.add_product)}
            <a class="btn btn-primary float-right" href="/admin/products/add" role="button">
                <i class="fas fa-plus"></i> Adauga produs
            </a>
        {/if}
    </h4>

    {if isset($successMessage)}
        <div class="alert alert-success" role="alert">
            {$successMessage}
        </div>
    {/if}
    {if isset($successMessageError)}
        <div class="alert alert-danger" role="alert">
            {$successMessageError}
        </div>
    {/if}

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_product').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_product" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_product)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" for="name">
                Nume produs
                <input type="text" class="form-control" name="name" id="name"
                       value="{if isset($smarty.post.name)}{$smarty.post.name}{/if}">
            </label>
            <label class="col-sm-6 col-form-label" for="category">
                Categorie
                <select name="category" class="form-control select2">
                    <option value="" selected></option>
                    {foreach from=$categories item=category}
                        <option value="{$category.id}"
                                {if isset($smarty.post.category) && $smarty.post.category eq $category.id}selected{/if}>
                            {$category.name}
                        </option>
                        {if isset($category.sub_categories)}
                            {foreach from=$category.sub_categories item=subCategory}
                                <option value="{$subCategory.id}"
                                        {if isset($smarty.post.category) && $smarty.post.category eq $subCategory.id}selected{/if}>
                                    &nbsp;&nbsp;&nbsp;&nbsp;{$subCategory.name}
                                </option>
                            {/foreach}
                        {/if}
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-6 col-form-label" for="status">
                Status
                <select name="status" class="form-control">
                    <option value="" selected></option>
                    <option value="active"
                            {if isset($smarty.post.status) && $smarty.post.status eq "active"}selected{/if}>
                        Produs activ
                    </option>
                    <option value="inactive"
                            {if isset($smarty.post.status) && $smarty.post.status eq "inactive"}selected{/if}>
                        Produs inactiv
                    </option>
                </select>
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_product">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Imagine</th>
            <th scope="col">Produs</th>
            <th scope="col">Categorie</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $products}
            {assign var="curNumber" value=1}
            {foreach from=$products item=product}
                <tr>
                    <th scope="row">{$curNumber++}</th>
                    <td>
                        <img src="/images/product/{$product.image}" alt="{$product.title}"
                             width="100" height="100">
                    </td>
                    <td>{$product.name}</td>
                    <td>{$product.category_name}</td>
                    <td>
                        {if $product.active == 1}
                            Activ
                        {else}
                            Inactiv
                        {/if}
                    </td>
                    <td>
                        {if isset($smarty.session.admin.roles.cancel_product)}
                            {if $product.active == 1}
                                <a href="/admin/products/disable/{$product.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Dezactiveaza produsul"
                                   onclick="return confirm('Esti sigur ca vrei sa dezactivezi produsul?')">
                                    <i class="fas fa-lock"></i>
                                </a>
                            {else}
                                <a href="/admin/products/enable/{$product.id}" class="text-dark p-2"
                                   data-toggle="tooltip" title="Activeaza produsul"
                                   onclick="return confirm('Esti sigur ca vrei sa activezi produsul?')">
                                    <i class="fas fa-unlock"></i>
                                </a>
                            {/if}
                        {/if}
                        {if isset($smarty.session.admin.roles.edit_product)}
                            <a href="/admin/products/edit/{$product.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Editeaza produsul">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="6">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
