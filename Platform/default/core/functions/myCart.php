<?php

function myCart()
{
    global $db;
    global $link;
    global $template;

    if (!isset($link[2]) || !$link[2]) {
        if (isset($_SESSION['client']) && !empty($_SESSION['client'])) {
            $sql = "SELECT * FROM client_billing_data WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND type_person = 1 AND active = 1";
            $billing_data_individual_person = $db->select($sql);

            $sql = "SELECT * FROM client_billing_data WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND type_person = 2 AND active = 1";
            $billing_data_legal_person = $db->select($sql);

            $sql = "SELECT * FROM client_delivery_data WHERE client_id = '" . $_SESSION['client']['client_id'] . "' AND active = 1";
            $delivery_data_courier = $db->select($sql);

            $template->assign('billing_data_individual_person', $billing_data_individual_person);
            $template->assign('billing_data_legal_person', $billing_data_legal_person);
            $template->assign('delivery_data_courier', $delivery_data_courier);
        }

        if (isset($_POST['send_order'])) {
            $order = array();

            // Type of Person
            if ($_POST['typePerson'] === 'individual-person') {
                if ($_POST['billingData'] === 'new-individual-person') {
                    $sql = "INSERT INTO client_billing_data (full_name, phone_number, address, type_person, client_id)
                            VALUES ('" . $db->escape_string($_POST['individual-person']['full_name']) . "',
                                    '" . $db->escape_string($_POST['individual-person']['phone_number']) . "',
                                    '" . $db->escape_string($_POST['individual-person']['address']) . "',
                                    '1', '" . $_SESSION['client']['client_id'] . "'
                            )";
                    $billing_data_individual_person_id = $db->query($sql, true);
                } else {
                    $billing_data_individual_person_id = $_POST['billingData'];
                }

                $sql = "SELECT * FROM client_billing_data WHERE id = '" . $billing_data_individual_person_id . "'";
                $client_billing_data = $db->query($sql)->fetch_object();

                $order['person'] = $client_billing_data;
                $order['person']->type_person = 'individualPerson';
            } elseif ($_POST['typePerson'] === 'legal-person') {
                if ($_POST['billingData'] === 'new-legal-person') {
                    if ($_POST['legal-person']['vat_payer'] === '0') {
                        $unique_registration_code = $_POST['legal-person']['unique_registration_code'];
                    } else {
                        $unique_registration_code = $_POST['legal-person']['vat_payer'] . $_POST['legal-person']['unique_registration_code'];
                    }

                    $sql = "INSERT INTO client_billing_data (full_name, unique_registration_code, address, type_person, client_id)
                            VALUES ('" . $db->escape_string($_POST['legal-person']['full_name']) . "',
                                    '" . $db->escape_string($unique_registration_code) . "',
                                    '" . $db->escape_string($_POST['legal-person']['address']) . "',
                                    '2', '" . $_SESSION['client']['client_id'] . "'
                            )";
                    $billing_data_legal_person_id = $db->query($sql, true);
                } else {
                    $billing_data_legal_person_id = $_POST['billingData'];
                }

                $sql = "SELECT * FROM client_billing_data WHERE id = '" . $billing_data_legal_person_id . "'";
                $client_billing_data = $db->query($sql)->fetch_object();

                $order['person'] = $client_billing_data;
                $order['person']->type_person = 'legalPerson';
            } else {
                die('Something went wrong!');
            }

            // Type of Delivery
            if ($_POST['typeDelivery'] === 'personal') {
                $order['delivery'] = new stdClass();
                $order['delivery']->type_delivery = 'personal';
                $order['delivery']->price = 0;
            } elseif ($_POST['typeDelivery'] === 'courier') {
                if ($_POST['deliveryData'] === 'new-courier') {
                    $sql = "INSERT INTO client_delivery_data (full_name, phone_number, address, client_id)
                            VALUES ('" . $db->escape_string($_POST['courier']['full_name']) . "',
                                    '" . $db->escape_string($_POST['courier']['phone_number']) . "',
                                    '" . $db->escape_string($_POST['courier']['address']) . "',
                                    '" . $_SESSION['client']['client_id'] . "'
                            )";
                    $delivery_data_courier_id = $db->query($sql, true);
                } else {
                    $delivery_data_courier_id = $_POST['deliveryData'];
                }

                $sql = "SELECT * FROM client_delivery_data WHERE id = '" . $delivery_data_courier_id . "'";
                $client_delivery_data = $db->query($sql)->fetch_object();

                $order['delivery'] = $client_delivery_data;
                $order['delivery']->type_delivery = 'courier';
                $order['delivery']->price = number_format(10, 2);
            } else {
                die('Something went wrong!');
            }

            // Type of Payment
            $payment = $_POST['paymentData'];
            $order['payment'] = new stdClass();
            $order['payment']->type_payment = $payment;

            // Note
            $note = $_POST['note'];
            $order['note'] = new stdClass();
            $order['note']->note = $note;

            // Client
            $order['client'] = new stdClass();
            $order['client']->client_id = $_SESSION['client']['client_id'];
            $order['client']->email = $_SESSION['client']['email'];
            $order['client']->first_name = $_SESSION['client']['first_name'];
            $order['client']->last_name = $_SESSION['client']['last_name'];

            // Cart
            $order['cart'] = new stdClass();
            $totalProducts = 0;
            $totalProductsPrice = 0;
            foreach ($_SESSION['cart'] as $productCart) {
                $product = new stdClass();
                $product->id = $productCart['id'];
                $product->name = $productCart['name'];
                $product->url = $productCart['url'];
                $product->image = $productCart['image'];
                $product->quantity = $productCart['quantityCart'];
                if ($productCart['is_discount']) {
                    $product->is_discount = $productCart['is_discount'];
                    $product->price = number_format($productCart['new_price'], 2);
                } else {
                    $product->price = number_format($productCart['price'], 2);
                }

                $sql = "SELECT name, value FROM vat WHERE id = '" . $productCart['vat_id'] . "'";
                $product->vat = $db->query($sql)->fetch_object();

                $totalProducts += $product->quantity;
                $totalProductsPrice += $product->price * $product->quantity;

                $order['cart']->products[] = $product;
            }

            $totalPrice = $totalProductsPrice + $order['delivery']->price;
            $order['cart']->totalProducts = $totalProducts;
            $order['cart']->totalProductsPrice = number_format($totalProductsPrice, 2);
            $order['cart']->deliveryPrice = number_format($order['delivery']->price, 2);
            $order['cart']->totalPrice = number_format($totalPrice, 2);

            $sql = "INSERT INTO `order` (total_price, client_id, order_json)
                    VALUES ('" . number_format($totalPrice, 2) ."',
                            '" . $_SESSION['client']['client_id'] ."',
                            '" . $db->escape_string(json_encode($order)) ."'
                    )";

            if ($orderId = $db->query($sql, true)) {
                unset($_SESSION['cart']);

                $template->assign('orderId', $orderId);

                return $template->fetch('cart/confirm/confirmOrder.tpl');
            }

            die('Something went wrong!');
        }

        return $template->fetch('cart/myCart.tpl');
    }

    return $template->fetch('404.tpl');
}
