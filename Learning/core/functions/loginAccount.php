<?php

function loginAccount()
{
    global $database;
    global $template;

    if (!isset($_SESSION['client']) || empty($_SESSION['client'])) {
        $errors = array();

        if (isset($_POST['login_account'])) {
            $sql = "SELECT * FROM client WHERE email = '" . $database->escape($_POST['email']) . "';";
            $client = $database->query($sql)->fetch_object();

            if (empty($client)) {
                $errors[] = 'E-mail incorect!';
            } elseif (md5($_POST['password']) != $client->password) {
                $errors[] = 'Parola incorecta!';
            }

            if ($client->block == 1) {
                $errors[] = 'Ne cerem scuze, dar acest cont este blocat!';
            }

            if (!empty($errors)) {
                $template->assign('errors', $errors);

                return $template->fetch('loginAccount.tpl');
            } else {
                $_SESSION['client']['client_id'] = $client->id;
                $_SESSION['client']['email'] = $client->email;
                $_SESSION['client']['first_name'] = $client->first_name;
                $_SESSION['client']['last_name'] = $client->last_name;
                $_SESSION['client']['role'] = $client->role;

                header("Location: /");
            }
        }

        return $template->fetch('loginAccount.tpl');
    }

    return $template->fetch('404.tpl');
}
