<?php

function product()
{
    global $db;
    global $link;
    global $template;

    if (isset($link[2]) && $link[2]) {
        $productUrl = $db->escape_string($link[2]);

        $sql = "SELECT p.*, c.name category_name, c.url category_url FROM product p
                INNER JOIN category c ON c.id = p.category_id
                WHERE p.url = '{$productUrl}' AND p.active = 1 AND c.active = 1";
        $product = $db->select($sql);

        if (!empty($product)) {
            $sql = "SELECT * FROM image WHERE product_id = '{$product[0]['id']}' AND active = 1";
            $product[0]['images'] = $db->select($sql);

            $template->assign('product', $product[0]);

            return $template->fetch('product.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}
