<div class="container" id="content-admin">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Detalii lectie numarul #{$lesson->id}
    </h4>

    <div class="row">
        <div class="col-md-12">
            {if empty($lesson)}
                <div class="border rounded p-3 mb-3">
                    <div class="text-center">
                        <h5>Aceasta lectie nu exista.</h5>
                    </div>
                </div>
            {else}
                <div class="border rounded-bottom p-3 mb-3">
                    <div class="row">
                        <div class="col-4">
                            <h6>Titlu</h6>
                        </div>
                        <div class="col-8">
                            <a href="/lesson/{$lesson->url}" title="{$lesson->name}" class="text-dark" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                            {$lesson->name}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <h6>Clasa</h6>
                        </div>
                        <div class="col-8">
                            <a href="/clasa/{$lesson->class->url}/" title="{$lesson->class->name}" class="text-dark" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                            {$lesson->class->name}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <h6>Materie</h6>
                        </div>
                        <div class="col-8">
                            <a href="/material/{$lesson->material->url}/" title="{$lesson->material->name}" class="text-dark" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                            {$lesson->material->name}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <h6>Vizibil</h6>
                        </div>
                        <div class="col-8">
                            {if $lesson->visible}Da{else}Nu{/if}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <h6>Detalii</h6>
                        </div>
                        <div class="col-8">
                            {$lesson->details|nl2br}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <h6>Materiale</h6>
                        </div>
                        <div class="col-8">
                            {if empty($lesson)}
                                -
                            {else}
                                {foreach from=$files item=file}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="/lib/file/lesson/{$file.hash_name}" title="{$file.original_name}" class="text-dark" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                                            {$file.original_name}
                                        </div>
                                    </div>
                                {/foreach}
                            {/if}
                        </div>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>
