<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Detalii chitanta nr. #{$receipt->number} pentru factura nr. #{$invoice->id}
        <a href="/lib/file/receipt/Chitanta-{$receipt->series_value}-{$receipt->number}.pdf"
           class="text-dark p-2" data-toggle="tooltip" title="Vizualizeaza chitanta" target="_blank">
            <i class="fas fa-file-pdf"></i>
        </a>
    </h4>

    <div class="border p-3">
        {include file="admin/receipt/receiptDetailContent.tpl"}
    </div>
</div>
