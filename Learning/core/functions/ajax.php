<?php

function ajax()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[2]) && $link[2]) {
        // Return Url of Class
        if ($link[2] === 'getUrlClass') {
            if (isset($_POST['className']) && $_POST['className']) {
                $className = strtolower(str_replace(' ', '-', $_POST['className']));
                $className = preg_replace('/[^a-zA-Z0-9-]/', '', $className);

                $sql = "SELECT url FROM class
                        WHERE url = '" . $database->escape($className) . "'";

                if ($database->query($sql)->fetch_object()) {
                    $counter = 0;

                    do {
                        $counter += 1;
                        $className = $className . "-" . $counter;

                        $sql = "SELECT url FROM class
                                WHERE url = '" . $database->escape($className) . "'";
                    } while ($database->query($sql)->fetch_object());
                }

                echo $className;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Return Url of Material
        if ($link[2] === 'getUrlMaterial') {
            if (isset($_POST['materialName']) && $_POST['materialName']) {
                $materialName = strtolower(str_replace(' ', '-', $_POST['materialName']));
                $materialName = preg_replace('/[^a-zA-Z0-9-]/', '', $materialName);

                $sql = "SELECT url FROM material
                        WHERE url = '" . $database->escape($materialName) . "'";

                if ($database->query($sql)->fetch_object()) {
                    $counter = 0;

                    do {
                        $counter += 1;
                        $materialName = $materialName . "-" . $counter;

                        $sql = "SELECT url FROM material
                                WHERE url = '" . $database->escape($materialName) . "'";
                    } while ($database->query($sql)->fetch_object());
                }

                echo $materialName;

                exit;
            }

            return $template->fetch('404.tpl');
        }

        // Delete File From Lesson
        if ($link[2] === 'deleteFileFromLesson') {
            if (
                isset($_POST['fileId']) && $_POST['fileId'] &&
                isset($_POST['lessonId']) && $_POST['lessonId']
            ) {
                $sql = "DELETE FROM file
                        WHERE id = '" . intval($_POST['fileId']) . "' AND
                            lesson_id = '" . intval($_POST['lessonId']) . "';";
                $database->query($sql);

                exit;
            }

            return $template->fetch('404.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}
