<?php

function login()
{
    global $CONF;
    global $database;
    global $template;

    $error = '';
    if (isset($_POST['enter'])) {
        $email = $database->escape($_POST['email']);
        $password = $database->escape($_POST['password']);

        $sql = "SELECT * FROM user WHERE email = '{$email}'";
        $account = $database->query($sql)->fetch_object();

        if (empty($account)) {
            $error = 'E-mail incorect!';
        } elseif (md5($password) != $account->password) {
            $error = 'Parola incorecta!';
        } elseif ($account->active != 1) {
            $error = 'Acces limitat!';
        }

        if ($error) {
            $template->assign('error', $error);
        } else {
            $_SESSION['admin']['user_id'] = intval($account->id);
            $_SESSION['admin']['email'] = $account->email;
            $_SESSION['admin']['first_name'] = $account->first_name;
            $_SESSION['admin']['last_name'] = $account->last_name;

            header("Location: /admin");
        }
    }

    $template->assign('CONF', $CONF);

    return $template->fetch('admin/login.tpl');
}
