<div class="border rounded-bottom p-3 mb-3">
    {if empty($lessons)}
        <div class="text-center">
            {if $smarty.session.client.role == 1}
                <h5>Deocamdata nu ai lecti preferate.</h5>
            {else}
                <h5>Deocamdata nu ai lectii adaugate.</h5>
            {/if}

            {if $smarty.session.client.role == 1}
                <a class="btn btn-primary" role="button" href="/lesson">
                    <i class="fas fa-chalkboard"></i> Vezi lectii
                </a>
            {else}
                <a class="btn btn-primary" role="button" href="/account/myLessons/add">
                    <i class="fas fa-chalkboard"></i> Adauga lectie
                </a>
            {/if}
        </div>
    {else}
        {foreach from=$lessons item=lesson name=lesson}
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-3 border-bottom-2">{$lesson.name}</h3>
                </div>

                <div class="col-2 m-auto">
                    <i class="far fa-clock"></i>
                    {$lesson.created_at|date_format:"%d.%m.%Y %H:%M"}
                </div>

                <div class="col-2 m-auto">{$lesson.class->name}</div>

                <div class="col-2 m-auto">{$lesson.material->name}</div>
                {if $smarty.session.client.role == 1}
                    <div class="col-3 m-auto text-right">
                        <a href="/lesson/{$lesson.url}"
                           class="text-dark p-2" data-toggle="tooltip" title="Vezi lectia">
                            <i class="fas fa-eye"></i>
                        </a>

                        <a href="/account/myLessons/remove_favorite/{$lesson.id}"
                           class="text-dark p-2" data-toggle="tooltip" title="Elimina lectia"
                           onclick="return confirm('Esti sigur ca vrei sa elimini lectia de la favorite?')">
                            <i class="fas fa-heart-broken"></i>
                        </a>
                    </div>
                {else}
                    <div class="col-2 m-auto">{if $lesson.visible == 1}Lectie vizibila{else}Lectie invizibila{/if}</div>

                    <div class="col-3 m-auto text-right">
                        <a href="/account/myLessons/view/{$lesson.id}"
                           class="text-dark p-2" data-toggle="tooltip" title="Vezi detalii lectie">
                            <i class="fas fa-eye"></i>
                        </a>

                        <a href="/account/myLessons/edit/{$lesson.id}"
                           class="text-dark p-2" data-toggle="tooltip" title="Editeaza lectie">
                            <i class="fas fa-pencil-alt"></i>
                        </a>

                        {if $lesson.visible == 1}
                            <a href="/account/myLessons/disable/{$lesson.id}"
                               class="text-dark p-2" data-toggle="tooltip" title="Dezactiveaza lectia"
                               onclick="return confirm('Esti sigur ca vrei sa dezactivezi lectia?')">
                                <i class="fas fa-lock"></i>
                            </a>
                        {else}
                            <a href="/account/myLessons/enable/{$lesson.id}"
                               class="text-dark p-2" data-toggle="tooltip" title="Activeaza lectia"
                               onclick="return confirm('Esti sigur ca vrei sa activezi lectia?')">
                                <i class="fas fa-unlock"></i>
                            </a>
                        {/if}

                        {if $lesson.active == 1}
                            <a href="/account/myLessons/delete/{$lesson.id}"
                               class="text-dark p-2" data-toggle="tooltip" title="Sterge definitiv lectia"
                               onclick="return confirm('Esti sigur ca vrei sa stergi lectia?')">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        {/if}
                    </div>
                {/if}
            </div>

            {if not $smarty.foreach.lesson.last}
                <hr>
            {/if}
        {/foreach}
    {/if}
</div>
