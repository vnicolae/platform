<?php

function business()
{
    global $CONF;
    global $template;
    global $link;
    global $db;

    if (isset($link[2]) && $link[2] == 'add') {
        if (isset($_POST['save'])) {
            $businessName = $db->escape_string($_POST['name']);
            $adminLastName = $db->escape_string($_POST['admin_last_name']);
            $adminFirstName = $db->escape_string($_POST['admin_first_name']);
            $adminEmail = $db->escape_string($_POST['admin_email']);
            $adminPassword = md5($_POST['admin_password']);

            $sql = "INSERT INTO business (name, admin_name, admin_email) 
                    VALUES ('{$businessName}', '{$adminFirstName} {$adminLastName}', '{$adminEmail}')";
            $db->query($sql);

            $_SESSION['message'] = 'Noul business a fost adaugat!';

            createNewBusinessScript($businessName, $adminFirstName, $adminLastName, $adminEmail, $adminPassword);

            header("Location: //" . $CONF['sitepath']);
        }

        return $template->fetch('admin/addBusiness.tpl');
    }

    return $template->fetch('404.tpl');
}
