<h2 class="mt-4 mb-3 border-bottom-5">
    Eroare 404 - Page not found
</h2>

<div class="row">
    <div class="col-md-12 mb-3 text-center">
        <h5>Aceasta pagina a fost mutata sau nu mai exista!</h5>
        <a class="btn btn-primary btn-custom" href="/" role="button">Intoarce-te la invatat</a>
    </div>
</div>
