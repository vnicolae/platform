<?php

function settings()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'user') {
            if (isset($link[4]) && $link[4]) {
                if ($link[4] === 'add') {
                    if (isset($_POST['add_user'])) {
                        $sql = "INSERT INTO user 
                                (email, password, first_name, last_name, phone_number) 
                                VALUES (
                                    '" . $database->escape($_POST['email']) . "',
                                    '" . md5($database->escape($_POST['password'])) . "',
                                    '" . $database->escape($_POST['first_name']) . "',
                                    '" . $database->escape($_POST['last_name']) . "',
                                    '" . $database->escape($_POST['phone_number']) . "'
                                )";
                        $database->query($sql, true);

                        $_SESSION['message'] = 'Adminul a fost adaugat.';

                        header("Location: /admin/settings/user");
                    }

                    return $template->fetch('admin/settings/user/userAdd.tpl');
                }

                if ($link[4] === 'edit') {
                    if (isset($link[5]) && intval($link[5])) {
                        if (isset($_POST['edit_user'])) {
                            $sql = "UPDATE user
                                    SET first_name = '" . $database->escape($_POST['first_name']) . "',
                                        last_name = '" . $database->escape($_POST['last_name']) . "',
                                        phone_number = '" . $database->escape($_POST['phone_number']) . "'
                                    WHERE id = '" . intval($link[5]) . "' AND active = 1;";
                            $database->query($sql);

                            $_SESSION['message'] = 'Datele adminului au fost actualizate.';

                            header("Location: /admin/settings/user");
                        }

                        $sql = "SELECT * FROM user WHERE id = '" . intval($link[5]) . "';";
                        $user = $database->query($sql)->fetch_object();

                        if (empty($user)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $template->assign('user', $user);

                        return $template->fetch('admin/settings/user/userEdit.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'view') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "SELECT * FROM user WHERE id = '" . intval($link[5]) . "';";
                        $user = $database->query($sql)->fetch_object();

                        if (empty($user)) {
                            return $template->fetch('admin/somethingWentWrong.tpl');
                        }

                        $template->assign('user', $user);

                        return $template->fetch('admin/settings/user/userDetail.tpl');
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'enable') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE user SET active = 1
                                WHERE id = '" . intval($link[5]) . "' AND active = 0;";
                        $database->query($sql);

                        $_SESSION['message'] = 'Adminul a fost activat.';

                        header("Location: /admin/settings/user");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                if ($link[4] === 'disable') {
                    if (isset($link[5]) && intval($link[5])) {
                        $sql = "UPDATE user SET active = 0
                                WHERE id = '" . intval($link[5]) . "' AND active = 1;";
                        $database->query($sql);

                        $_SESSION['message'] = 'Adminul a fost dezactivat.';

                        header("Location: /admin/settings/user");
                    }

                    return $template->fetch('admin/404.tpl');
                }

                return $template->fetch('admin/404.tpl');
            }

            $where = "WHERE 1";
            if (isset($_POST['filter_user'])) {
                if ($_POST['last_name']) {
                    $where .= " AND last_name LIKE '%" . $database->escape($_POST['last_name']) . "%'";
                }
                if ($_POST['first_name']) {
                    $where .= " AND first_name LIKE '%" . $database->escape($_POST['first_name']) . "%'";
                }
                if ($_POST['email']) {
                    $where .= " AND email LIKE '%" . $database->escape($_POST['email']) . "%'";
                }
                if ($_POST['phone_number']) {
                    $where .= " AND phone_number LIKE '%" . $database->escape($_POST['phone_number']) . "%'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND active = '0'";
                    }
                }
            }

            $sql = "SELECT * FROM user
                    {$where};";
            $users = $database->select($sql);

            $template->assign('users', $users);

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }

            return $template->fetch('admin/settings/user/userList.tpl');
        }
    }

    return $template->fetch('admin/404.tpl');
}
