<?php

function admin()
{
    global $CONF;
    global $link;
    global $template;

    if (!isset($_SESSION['admin']['user_id'])) {
        eval ($cmd = '$content = login();');
    } else {
        if ($link[1] == 'admin' && (!isset($link[2]) || $link[2] == '' || preg_match("@^\?@", $link[2]))) {
            eval ($cmd = '$content = order();');
        } else {
            if ($link[1] == 'admin' && in_array($link[2], array(
                        'logout', 'notFound', 'order', 'client', 'invoice', 'receipt', 'settings',
                        'carousel', 'products', 'categories', 'raport'
                    )
                )
            ) {
                eval ($cmd = '$content = ' . $link[2] . "();");
            } else {
                eval ($cmd = '$content = notFoundAdmin404();');
            }
        }
    }

    $template->assign('logoSite', getLogoSite());

    $template->assign('content', $content);
    $template->assign('CONF', $CONF);

    return $template->displayTemplate('admin/admin.tpl');
}
