<?php

function category()
{
    global $db;
    global $link;
    global $template;

    if (isset($link[2]) && $link[2]) {
        $categoryUrl = $db->escape_string($link[2]);

        $sql = "SELECT * FROM category WHERE url = '{$categoryUrl}' AND active = 1";
        $category = $db->select($sql);

        if (!empty($category)) {
            $template->assign('products', getProductsByCategory($category[0]['id']));
            $template->assign('category', $category[0]);

            return $template->fetch('category.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}
