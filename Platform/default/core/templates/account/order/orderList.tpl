<div class="border rounded-bottom p-3 mb-3">
    {if empty($orders)}
        <div class="text-center">
            <h5>Deocamdata nu ai nicio comanda inregistrata.</h5>
            <a class="btn btn-primary btn-custom" href="/" role="button">Intoarce-te la magazin</a>
        </div>
    {else}
        {foreach from=$orders item=order name=order}
            <div class="row">
                <div class="col-4">
                    <div class="font-weight-bold">
                        Comanda #{$order.id}
                    </div>
                    <div>
                        {$order.timestamp|date_format:"%d.%m.%Y %H:%M"}
                    </div>
                </div>

                <div class="col-3 m-auto">
                    {$order.total_price} Lei
                </div>

                <div class="col-2 m-auto">
                    {if $order.active == 1}
                        Comanda plasata
                    {elseif $order.active == 2}
                        Comanda procesata
                    {else}
                        Comanda anulata
                    {/if}
                </div>

                <div class="col-3 m-auto text-right">
                    {if $order.active == 1}
                        <a href="/account/myOrders/cancel/{$order.id}"
                           class="text-dark p-2" data-toggle="tooltip" title="Anuleaza comanda"
                           onclick="return confirm('Esti sigur ca vrei sa anulezi comanda?')">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    {elseif $order.active == 2 && isset($order.invoice.id)}
                        <a href="/lib/file/invoice/Factura-{$order.invoice.series_value}-{$order.invoice.number}.pdf"
                           class="text-dark p-2" data-toggle="tooltip" title="Vizualizeaza factura" target="_blank">
                            <i class="fas fa-file-pdf"></i>
                        </a>
                    {/if}
                    <a href="/account/myOrders/view/{$order.id}"
                       class="text-dark p-2" data-toggle="tooltip" title="Vezi detalii comanda">
                        <i class="fas fa-eye"></i>
                    </a>
                </div>
            </div>
            {if not $smarty.foreach.order.last}
                <hr>
            {/if}
        {/foreach}
    {/if}
</div>
