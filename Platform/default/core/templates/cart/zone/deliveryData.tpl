<div class="custom-control custom-radio custom-control-inline m-2">
    <input type="radio" id="personal" name="typeDelivery" class="custom-control-input"
           value="personal" required checked>
    <label class="custom-control-label" for="personal">Ridicare personala</label>
</div>

<div class="custom-control custom-radio custom-control-inline m-2">
    <input type="radio" id="courier" name="typeDelivery" class="custom-control-input"
           value="courier" required>
    <label class="custom-control-label" for="courier">Livrare prin curier</label>
</div>

<div class="box-type-delivery personal" style="display: block">
    <div class="custom-control custom-radio m-2">
        <input type="radio" id="type-personal" name="deliveryData" class="custom-control-input" value="office" required>
        <label class="custom-control-label" for="type-personal">
            Ridicare personala de la sediul nostru<br>
            <div class="type-personal sub-box-type-delivery font-weight-bold">
                Strada Margeanului, Sectorul 5, Bucuresti
            </div>
        </label>
        <div class="invalid-feedback">Selecteaza o modalitate de livrare</div>
    </div>
</div>

<div class="box-type-delivery courier">
    {foreach from=$delivery_data_courier item=courier}
        <div class="custom-control custom-radio m-2">
            <input type="radio" id="existing-courier-{$courier.id}" name="deliveryData"
                   class="custom-control-input" value="{$courier.id}" required>
            <label class="custom-control-label" for="existing-courier-{$courier.id}">
                <div class="existing-courier-{$courier.id} sub-box-type-delivery font-weight-bold">
                    Nume si numar de telefon
                </div>
                <div>
                    <span class="person_full_name">{$courier.full_name}</span> -
                    <span class="person_phone_number">{$courier.phone_number}</span>
                </div>
                <div class="existing-courier-{$courier.id} sub-box-type-delivery font-weight-bold">
                    Adresa:
                </div>
                <div>
                    <span class="person_address">{$courier.address}</span>
                </div>
            </label>
        </div>
    {/foreach}

    <div class="custom-control custom-radio m-2">
        <input type="radio" id="new-courier" name="deliveryData" class="custom-control-input"
               value="new-courier" required>
        <label class="custom-control-label" for="new-courier">
            Adauga adresa de livrare
        </label>
        <div class="invalid-feedback">Selecteaza o adresa de livrare</div>

        <div class="new-courier sub-box-type-delivery">
            <div class="form-group row">
                <div class="col-sm-12 font-weight-bold pt-2">Persoana de contact</div>
                <label class="col-sm-6 col-form-label" for="full_name">
                    Nume si Prenume <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-courier" id="full_name"
                           placeholder="ex: Popescu Ion" name="courier[full_name]" required>
                    <div class="invalid-feedback">Introduceti un Nume si Prenume valid.</div>
                </label>
                <label class="col-sm-6 col-form-label" for="phone_number">
                    Telefon <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-courier" id="phone_number"
                           placeholder="ex: 07xxxxxxxx" name="courier[phone_number]" required>
                    <div class="invalid-feedback">Introduceti un Telefon valid.</div>
                </label>

                <div class="col-sm-12 font-weight-bold pt-2">Adresa de livrare</div>
                <label class="col-sm-12 col-form-label" for="address">
                    Adresa <span class="text-danger font-weight-bold">*</span>
                    <input type="text" class="form-control detendent-new-courier" id="address"
                           placeholder="ex: Strada, numar, bloc, scara, etaj, apartament, judet, localitate"
                           name="courier[address]" required>
                    <div class="invalid-feedback">Introduceti o Adresa valida.</div>
                </label>

                <small class="col-sm-12">
                    Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>
        </div>
    </div>
</div>
