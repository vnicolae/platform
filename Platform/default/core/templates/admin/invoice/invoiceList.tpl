<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Lista facturi
    </h4>

    {if isset($successMessage)}
        <div class="alert alert-success" role="alert">
            {$successMessage}
        </div>
    {/if}

    <div class="col-sm-12 text-center mb-3">
        <a class="btn btn-secondary btn-custom" href="javascript:///" role="button"
           onclick="$('#filter_invoice').toggle('slow');">Arata filtre</a>
    </div>

    <form id="filter_invoice" method="POST" novalidate=""
          style="display:{if isset($smarty.post.filter_invoice)}block{else}none{/if}">
        <div class="form-group row">
            <label class="col-sm-6 col-form-label" for="series">
                Serie
                <select name="series" class="form-control">
                    <option value="" selected></option>
                    {foreach from=$seriesInvoice item=series}
                    <option value="{$series.id}"
                            {if isset($smarty.post.series) && $smarty.post.series eq $series.id}selected{/if}>
                        {$series.value} - {$series.name}
                    </option>
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-6 col-form-label" for="number">
                Numar
                <input type="text" class="form-control" name="number" id="number"
                       value="{if isset($smarty.post.number)}{$smarty.post.number}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="last_name">
                Nume client
                <input type="text" class="form-control" name="last_name" id="last_name"
                       value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}"
                       placeholder="ex: Popescu">
            </label>
            <label class="col-sm-4 col-form-label" for="first_name">
                Prenume client
                <input type="text" class="form-control" name="first_name" id="first_name"
                       value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}"
                       placeholder="ex: Ion">
            </label>
            <label class="col-sm-4 col-form-label" for="order">
                Id comanda
                <input type="text" class="form-control" name="order" id="order"
                       value="{if isset($smarty.post.order)}{$smarty.post.order}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="user">
                Utilizator
                <select name="user" class="form-control">
                    <option value="" selected></option>
                    {foreach from=$users item=user}
                        <option value="{$user.id}"
                                {if isset($smarty.post.user) && $smarty.post.user eq $user.id}selected{/if}>
                            {$user.first_name} {$user.last_name}
                        </option>
                    {/foreach}
                </select>
            </label>
            <label class="col-sm-4 col-form-label" for="invoice_date">
                Data factura
                <input type="text" class="datepicker form-control" name="invoice_date" id="invoice_date"
                       data-date-format="dd.mm.yyyy"
                       value="{if isset($smarty.post.invoice_date)}{$smarty.post.invoice_date}{/if}">
            </label>
            <label class="col-sm-4 col-form-label" for="status">
                Status
                <select name="status" class="form-control">
                    <option value="" selected></option>
                    <option value="active"
                            {if isset($smarty.post.status) && $smarty.post.status eq "active"}selected{/if}>
                        Factura activa
                    </option>
                    <option value="inactive"
                            {if isset($smarty.post.status) && $smarty.post.status eq "inactive"}selected{/if}>
                        Factura inactiva
                    </option>
                </select>
            </label>

            <div class="col-sm-12 mt-2">
                <button type="submit" class="btn btn-primary btn-custom" name="filter_invoice">Filtreaza</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Serie Numar</th>
            <th scope="col">Client</th>
            <th scope="col">ID comanda</th>
            <th scope="col">Data emitere</th>
            <th scope="col">Data scadenta</th>
            <th scope="col">Valoare totala</th>
            <th scope="col">Status</th>
            <th scope="col">Actiuni</th>
        </tr>
        </thead>

        {if $invoices}
            {foreach from=$invoices item=invoice}
                <tr>
                    <th scope="row">{$invoice.id}</th>
                    <td>{$invoice.series_value} {$invoice.number}</td>
                    <td>
                        <a href="/admin/client/view/{$invoice.client_id}" class="text-dark"
                           data-toggle="tooltip" title="Vezi detalii client">
                            {$invoice.first_name} {$invoice.last_name}
                        </a>
                    </td>
                    <td>
                        <a href="/admin/order/view/{$invoice.order_id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Vezi detalii comanda">
                            {$invoice.order_id}
                        </a>
                    </td>
                    <td>{$invoice.release_date|date_format:"%d.%m.%Y"}</td>
                    <td>{$invoice.due_date|date_format:"%d.%m.%Y"}</td>
                    <td>
                        {if $invoice.reference_id == 0}
                            {$invoice.total_price} Lei
                        {else}
                            - {$invoice.total_price} Lei
                        {/if}
                    </td>
                    <td>
                        {if $invoice.active == 1}
                            Activ
                        {else}
                            Inactive
                        {/if}
                    </td>
                    <td>
                        <a href="/admin/invoice/view/{$invoice.id}" class="text-dark p-2"
                           data-toggle="tooltip" title="Vezi detalii factura" target="_blank">
                            <i class="fas fa-eye"></i>
                        </a>
                        {if $invoice.active == 1 && !isset($invoice.receipt.id) && isset($smarty.session.admin.roles.add_receipt)}
                            <a href="/admin/receipt/add/{$invoice.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Emite chitanta">
                                <i class="fas fa-receipt"></i>
                            </a>
                        {elseif isset($invoice.receipt.id)}
                            <a href="/lib/file/receipt/Chitanta-{$invoice.receipt.series_value}-{$invoice.receipt.number}.pdf"
                               class="text-dark p-2" data-toggle="tooltip" title="Vizualizeaza chitanta" target="_blank">
                                <i class="fas fa-file-pdf"></i>
                            </a>
                        {/if}
                        {if $invoice.active == 1 && isset($smarty.session.admin.roles.cancel_invoice)}
                            <a href="/admin/invoice/cancel/{$invoice.id}" class="text-dark p-2"
                               data-toggle="tooltip" title="Anuleaza factura"
                               onclick="return confirm('Esti sigur ca vrei sa anulezi factura?')">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr class="text-center">
                <td colspan="9">Niciun rezultat</td>
            </tr>
        {/if}
    </table>
</div>
