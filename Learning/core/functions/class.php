<?php

function clasa()
{
    global $CONF;
    global $link;
    global $database;
    global $template;

    if (isset($link[2]) && $link[2]) {
        $classUrl = $database->escape($link[2]);

        $sql = "SELECT * FROM class WHERE url = '{$classUrl}' AND active = 1;";
        $class = $database->query($sql)->fetch_object();

        if (!empty($class)) {
            $template->assign('materials', getAllMaterials());
            $template->assign('class', $class);

            if (isset($link[3])) {
                if ($link[3] && $link[3] === 'material') {
                    if (isset($link[4]) && $link[4]) {
                        $materialUrl = $database->escape($link[4]);

                        $sql = "SELECT * FROM material WHERE url = '{$materialUrl}' AND active = 1;";
                        $material = $database->query($sql)->fetch_object();

                        if (!empty($material)) {
                            $page = 1;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }

                            $template->assign('material', $material);
                            $template->assign('title', $class->name . ' - ' . $material->name);

                            $sql = "SELECT * FROM lesson
                                    WHERE active = 1 AND visible = 1 AND
                                        class_id = '{$class->id}' AND
                                        material_id = '{$material->id}'
                                    ORDER BY updated_at DESC
                                    LIMIT " . ($page - 1) * $CONF['item_per_page'] . ",  " . $CONF['item_per_page'] . ";";
                            $lessons = $database->select($sql);

                            $sql = "SELECT COUNT(id) AS total FROM lesson
                                    WHERE active = 1 AND visible = 1 AND
                                        class_id = '{$class->id}' AND
                                        material_id = '{$material->id}'
                                    ORDER BY updated_at DESC;";
                            $result = $database->query($sql)->fetch_object();

                            $template->assign('lessons', $lessons);
                            $template->assign('page', $page);
                            $template->assign('total', $result->total);
                            $template->assign('number_page', ceil($result->total / $CONF['item_per_page']));
                            $template->assign('redirect_url', $_SERVER['REDIRECT_URL']);

                            return $template->fetch('lessonsFilter.tpl');
                        }
                    }
                }
            }

            return $template->fetch('classMaterial.tpl');
        }

        return $template->fetch('404.tpl');
    }

    return $template->fetch('404.tpl');
}
