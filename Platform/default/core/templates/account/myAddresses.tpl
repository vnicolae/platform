<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Adresele mele
</h4>

<div class="row">
    <div class="col-md-12">
        {if isset($successMessage)}
            <div class="alert alert-success" role="alert">
                {$successMessage}
            </div>
        {/if}
    </div>

    <div class="col-md-12">
        {include file="account/address/deliveryList.tpl"}
    </div>

    <div class="col-md-12">
        {include file="account/address/individualPersonList.tpl"}
    </div>

    <div class="col-md-12">
        {include file="account/address/legalPersonList.tpl"}
    </div>
</div>
