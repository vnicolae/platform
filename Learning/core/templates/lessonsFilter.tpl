<h2 class="mt-4 mb-3 border-bottom-5">
    {$title}
</h2>

<h6 class="mb-3 border-bottom-2">
    <a class="text-dark" href="/">Home</a> <i class="fas fa-angle-right"></i>
    {if $link.1 == 'material'}
        <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
        <a class="text-dark" href="/material/{$material->url}/">{$material->name}</a> <i class="fas fa-angle-right"></i>
        {$class->name}
    {elseif $link.1 == 'clasa'}
        <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
        <a class="text-dark" href="/clasa/{$class->url}/">{$class->name}</a> <i class="fas fa-angle-right"></i>
        {$material->name}
    {elseif $link.1 == 'lesson'}
        {if $link.2 == 'material'}
            <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
            {$material->name}
        {elseif $link.2 == 'clasa'}
            <a class="text-dark" href="/lesson/">Lectii</a> <i class="fas fa-angle-right"></i>
            {$class->name}
        {else}
            Lectii
        {/if}
    {/if}
</h6>

<div class="row">
    {if empty($lessons)}
        <div class="col-md-12">
            Ne pare rau, nu avem lectii in aceasta categorie.
        </div>
    {else}
        <div class="col-md-12">
            <!-- pagination -->
            <ul class="pagination">
                {if $page != 1}
                    <li>
                        <a href="{$redirect_url}?page={$page-1}">
                            <i class="fa fa-angle-left"></i>
                        </a>
                    </li>
                {/if}

                {for $i = 1 to $number_page}
                    <li class="{if $i == $page}active{/if}">
                        <a href="{$redirect_url}?page={$i}">{$i}</a>
                    </li>
                {/for}

                {if $i-1 != $page}
                    <li>
                        <a href="{$redirect_url}?page={$page+1}">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                {/if}
            </ul>
        </div>

        {foreach from=$lessons item=lesson}
            <div class="col-lg-3 col-md-4 col-sm-6">
                {include file='lessonBox.tpl'}
            </div>
        {/foreach}
    {/if}
</div>
