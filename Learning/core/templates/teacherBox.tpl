<div class="border rounded mb-3">
    <div class="div-hover overflow-hidden text-center">
        <a href="/teacher/{$teacher.id}" title="{$teacher.last_name} {$teacher.first_name}" class="text-dark">
            <img src="/images/avatar/{$teacher.avatar|default:'default-avatar.jpg'}" style="max-width: 100%;">
            <h6 class="p-2 mb-0 overflow-hidden" style="height: 55px;">{$teacher.last_name} {$teacher.first_name}</h6>
        </a>
    </div>
</div>
