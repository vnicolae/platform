<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    <div class="container">
        <a class="navbar-brand" href="/admin">
            <img src="//{$CONF.sitepath}images/admin/{$logoSite.logo_site}" style="width: 190px;">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarDropdown" aria-controls="navbarDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarDropdown"
             data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown my-account-dropdown">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-store"></i> Magazin
                    </a>
                    <div class="dropdown-menu my-account-dropdown-menu">
                        <a class="dropdown-item" href="/admin/categories/list">
                            <i class="fas fa-align-left"></i> Categorii
                        </a>
                        <a class="dropdown-item" href="/admin/products/list">
                            <i class="fas fa-cubes"></i> Produse
                        </a>
                        <a class="dropdown-item" href="/admin/client/list">
                            <i class="fas fa-child"></i> Clienti
                        </a>
                        <a class="dropdown-item" href="/admin/carousel/list">
                            <i class="fas fa-images"></i> Carusel
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown my-account-dropdown">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-cogs"></i> Documente
                    </a>
                    <div class="dropdown-menu my-account-dropdown-menu">
                        <a class="dropdown-item" href="/admin/invoice/list">
                            <i class="fas fa-file-invoice"></i> Facturi
                        </a>
                        <a class="dropdown-item" href="/admin/receipt/list">
                            <i class="fas fa-receipt"></i> Chitante
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown my-account-dropdown">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-chart-line"></i> Statistica
                    </a>
                    <div class="dropdown-menu my-account-dropdown-menu">
                        <a class="dropdown-item" href="/admin/raport">
                            <i class="fas fa-chart-bar"></i> Rapoarte
                        </a>
                    </div>
                </li>
                {if isset($smarty.session.admin.roles.access_settings)}
                    <li class="nav-item dropdown my-account-dropdown">
                        <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                            <i class="fas fa-cogs"></i> Setari
                        </a>
                        <div class="dropdown-menu my-account-dropdown-menu">
                            <a class="dropdown-item" href="/admin/settings/user">
                                <i class="fas fa-users"></i> Utilizatori
                            </a>
                            <a class="dropdown-item" href="/admin/settings/company">
                                <i class="fas fa-building"></i> Companie
                            </a>
                            <a class="dropdown-item" href="/admin/settings/series">
                                <i class="fas fa-key"></i> Serii
                            </a>
                            <a class="dropdown-item" href="/admin/settings/vat">
                                <i class="fas fa-percentage"></i> Cote TVA
                            </a>
                        </div>
                    </li>
                {/if}
                <li class="nav-item dropdown my-account-dropdown">
                    <a class="nav-link dropdown-toggle cursor-pointer font-size-18">
                        <i class="fas fa-user-circle font-size-22 pr-1"></i> Contul meu
                    </a>
                    <div class="dropdown-menu my-account-dropdown-menu">
                        <div class="text-nowrap p-1 pl-4 pr-4">
                            Salut, {$smarty.session.admin.first_name} {$smarty.session.admin.last_name}
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/admin/logout">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                    </div>
                </li>
            </ul>
        </div>

        {*<div>*}
            {*<ul class="nav navbar-nav ml-auto">*}
                {*<li class="nav-item font-weight-bold">*}
                    {*<a class="nav-link" href="/admin/logout">Logout</a>*}
                {*</li>*}
            {*</ul>*}
        {*</div>*}
    </div>
</nav>