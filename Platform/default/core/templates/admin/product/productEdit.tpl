<div class="container" id="content-platform-container">
    <h4 class="pb-3 mt-4 mb-3 border-bottom">
        Editeaza produsul #{$product->id}
    </h4>

    {if isset($error)}
        <div class="alert alert-danger" role="alert">
            {$error}
        </div>
    {/if}

    <div class="mt-4">
        <form class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="name">
                    Nume produs <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control product-name" id="name" name="name"
                           value="{$product->name}" required>
                    <div class="invalid-feedback">Introduceti un Nume valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="url">
                    Link <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control product-url" id="url" name="url"
                           value="{$product->url}" required>
                    <div class="invalid-feedback">Introduceti un Link valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="category">
                    Categorie <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <select name="category" class="form-control select2" required>
                        <option value="" selected></option>
                        {foreach from=$categories item=category}
                            <option value="{$category.id}" {if $product->category_id eq $category.id}selected{/if}>
                                {$category.name}
                            </option>
                            {if isset($category.sub_categories)}
                                {foreach from=$category.sub_categories item=subCategory}
                                    <option value="{$subCategory.id}" {if $product->category_id eq $subCategory.id}selected{/if}>
                                        &nbsp;&nbsp;&nbsp;&nbsp;{$subCategory.name}
                                    </option>
                                {/foreach}
                            {/if}
                        {/foreach}
                    </select>
                    <div class="invalid-feedback">Alegeti o Categorie valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="price">
                    Pret <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="price" name="price"
                           value="{$product->price}" placeholder="123.45" required>
                    <div class="invalid-feedback">Introduceti un Pret valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="vat">
                    Cota TVA <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <select name="vat" class="form-control select2" required>
                        <option value="" selected></option>
                        {foreach from=$vats item=vat}
                            <option value="{$vat.id}" {if $product->vat_id eq $vat.id}selected{/if}>
                                {$vat.value}% - {$vat.name}
                            </option>
                        {/foreach}
                    </select>
                    <div class="invalid-feedback">Alegeti o Cota TVA valida.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="stock">
                    Stoc <span class="text-danger font-weight-bold">*</span>
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="stock" name="stock"
                           value="{$product->stock}" placeholder="55" required>
                    <div class="invalid-feedback">Introduceti un Stoc valid.</div>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="discount">
                    Discount?
                </label>
                <div class="col-sm-8">
                    <input type="checkbox" id="discount" name="discount" class="custom-control-input" value="1"
                           {if $product->is_discount}checked{/if} onclick="$('#filter_product').toggle('slow');">
                    <label class="custom-control-label position-relative vertical-webkit-middle" for="discount">
                        <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                           title="Daca este bifat, produsul are discount"></i>
                    </label>
                </div>
            </div>
            <div class="form-group row" id="filter_product"
                 style="display:{if $product->is_discount}flex{else}none{/if}">
                <label class="offset-1 col-sm-2 col-form-label" for="new_price">
                    Pret nou
                </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="new_price" name="new_price"
                           value="{$product->new_price}" placeholder="123.45">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="active">
                    Vizibil
                </label>
                <div class="col-sm-8">
                    <input type="checkbox" id="active" name="active" class="custom-control-input" value="1"
                           {if $product->active}checked{/if}>
                    <label class="custom-control-label position-relative vertical-webkit-middle" for="active">
                        <i class="fas fa-info-circle ml-4" data-toggle="tooltip"
                           title="Daca este bifat, produsul este vizibil in site"></i>
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="details">
                    Detalii
                </label>
                <div class="col-sm-8">
                    <textarea class="form-control" id="details" name="details" rows="3">{$product->details}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="image">
                    Imagine <span class="text-danger font-weight-bold">*</span>
                    <br><strong>395x350</strong>
                </label>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image"
                               data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                               accept="image/*" onchange="loadFile(event)">
                        <label class="custom-file-label" for="image">Alege imagine</label>
                    </div>
                    <img {if $product->image}src="/images/product/{$product->image}"{/if}
                         id="preview_carousel" style="max-width: 100%;">
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-1 col-sm-2 col-form-label" for="images">
                    Imagini secundare
                    <br><strong>395x350</strong>
                </label>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="images" name="images[]"
                               data-file-types="image/jpeg,image/png,image/gif" lang="ro"
                               accept="image/*" onchange="preview_images();" multiple/>
                        <label class="custom-file-label" for="image">Alege imagine</label>
                    </div>
                    <div class="row" id="image_preview">
                        {foreach from=$images item=image}
                            <div class='col-md-3'>
                                <i class='fas fa-trash-alt bg-dark text-light position-absolute right-0 p-2 rounded-circle' onClick='deletePreview(this, {$image.id})'></i>
                                <img style='width: 100%' src="/images/product/{$image.name}">
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <small class="offset-3 col-sm-8">
                    Campurile cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
                </small>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-sm-8">
                    {if isset($smarty.session.admin.roles.edit_product)}
                        <button type="submit" class="btn btn-primary btn-custom" name="edit_product">Salveaza</button>
                    {/if}
                    <a class="btn btn-danger btn-custom" href="/admin/products/list" role="button">Anuleaza</a>
                </div>
            </div>
        </form>
    </div>
</div>
