<?php

/**
 * @param string $businessName
 * @param string $adminFirstName
 * @param string $adminLastName
 * @param string $adminEmail
 * @param string $adminPassword
 */
function createNewBusinessScript(
    string $businessName,
    string $adminFirstName,
    string $adminLastName,
    string $adminEmail,
    string $adminPassword
)
{
    global $db;
    global $CONF;

    $databaseName = 'platform_' . preg_replace('/[^a-zA-Z0-9]/', '', $businessName);
    $databaseUser = preg_replace('/[^a-zA-Z0-9]/', '', $businessName);
    $databasePass = hash('sha256', $databaseName . $databaseUser);

    // Create Database for new business
    if ($db->query("CREATE DATABASE $databaseName")) {
        print_ra("Database $databaseName created successfully");
    } else {
        print_ra("Error creating database $databaseName: " . $db->error());
    }

    // Create User for new business
    if ($db->query("CREATE USER '$databaseUser'@'localhost' IDENTIFIED BY '$databasePass';")) {
        print_ra("User $databaseUser created successfully");
    } else {
        print_ra("Error creating user $databaseUser: " . $db->error());
    }

    // Create Privileges for new business
    if ($db->query("GRANT SELECT, INSERT, UPDATE, DELETE ON $databaseName.* TO '$databaseUser'@'localhost'")) {
        print_ra("Privileges created successfully");
    } else {
        print_ra("Error creating privileges: " . $db->error());
    }

    // Create Tables Business and New Company
    if ($db->query("CREATE TABLE $databaseName.business (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(100) NOT NULL,
                        address varchar(100) NOT NULL,
                        unique_registration_code varchar(10) NOT NULL,
                        trade_register_code varchar(100) NOT NULL,
                        bank_name varchar(100) NOT NULL,
                        iban varchar(100) NOT NULL,
                        social_capital decimal(10,2) DEFAULT NULL,
                        logo_site varchar(100) NOT NULL,
                        active tinyint(4) DEFAULT '1'
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.business (name, logo_site) VALUES ('$businessName', 'logo_header_default.png');")
    ) {
        print_ra("Table Business created successfully");
    } else {
        print_ra("Error creating table Business: " . $db->error());
    }

    // Create Tables User and New Admin User
    if ($db->query("CREATE TABLE $databaseName.user (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        email varchar(100) NOT NULL,
                        password varchar(100) NOT NULL,
                        first_name varchar(100) NOT NULL,
                        last_name varchar(100) NOT NULL,
                        business_id int(11) NOT NULL,
                        type tinyint(4) NOT NULL DEFAULT '1',
                        active tinyint(4) NOT NULL DEFAULT '1',
                        timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        CONSTRAINT FK_business FOREIGN KEY (business_id) REFERENCES business(id),
                        INDEX user_email_index (email)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.user (email, password, first_name, last_name, business_id, type, active) VALUES
                    ('$adminEmail', '$adminPassword', '$adminFirstName', '$adminLastName', 1, 15, 1);")
    ) {
        print_ra("Table User created successfully");
    } else {
        print_ra("Error creating table User: " . $db->error());
    }

    // Create Tables Vat
    if ($db->query("CREATE TABLE $databaseName.vat (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(100) NOT NULL,
                        value double(10,2) NOT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.vat (name, value) VALUES ('Tva Romania', 19.00);")
    ) {
        print_ra("Table Vat created successfully");
    } else {
        print_ra("Error creating table Vat: " . $db->error());
    }

    // Create Tables Category
    if ($db->query("CREATE TABLE $databaseName.category (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        category_id int(11) NOT NULL DEFAULT '0',
                        name varchar(100) NOT NULL,
                        url varchar(100) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1',
                        timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        INDEX category_url_index (url)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.category (category_id, name, url, active) VALUES
                    (0, 'Imbracaminte', 'imbracaminte', 1),
                    (1, 'Tricouri', 'tricouri', 1),
                    (1, 'Fuste', 'fuste', 1),
                    (0, 'Incaltaminte', 'incaltaminte', 1),
                    (4, 'Adidasi', 'adidasi', 1),
                    (4, 'Sandale', 'sandale', 1),
                    (0, 'Accesorii', 'accesorii', 1),
                    (7, 'Ochelari', 'ochelari', 1),
                    (7, 'Ceasuri', 'ceasuri', 1);")
    ) {
        print_ra("Table Category created successfully");
    } else {
        print_ra("Error creating table Category: " . $db->error());
    }

    // Create Tables Product
    if ($db->query("CREATE TABLE $databaseName.product (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(100) NOT NULL,
                        price decimal(10,2) NOT NULL,
                        stock int(11) NOT NULL,
                        url varchar(100) NOT NULL,
                        image varchar(250) NOT NULL,
                        details text NOT NULL,
                        is_discount tinyint(4) NOT NULL DEFAULT '0',
                        new_price double(10,2) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1',
                        timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        category_id int(11) NOT NULL,
                        vat_id int(11) NOT NULL,
                        CONSTRAINT FK_category FOREIGN KEY (category_id) REFERENCES category(id),
                        CONSTRAINT FK_vat FOREIGN KEY (vat_id) REFERENCES vat(id),
                        INDEX product_url_index (url)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.product (name, price, stock, url, image, details, is_discount, new_price, category_id, vat_id, active) VALUES
                    ('Ochelari de soare cu rama alba', '39.00', 32, 'ochelari-de-soare-cu-rama-alba', 'bartosz-sujkowski-1437162-unsplash.jpg', '', 0, '0.00', 8, 1, 1),
                    ('Adidasi sport albi', '189.00', 55, 'adidasi-sport-albi', 'dorian-hurst-800968-unsplash.jpg', 'Pantofi sport cu siret. Exterior 60% piele naturala, 40% tesatura. Interior 100% piele naturala. Talpa voluminoasa.', 0, '0.00', 5, 1, 1),
                    ('Ceas cu curea din piele barbat', '249.00', 10, 'ceas-uc-curea-din-piele-barbat', 'hunters-race-408738-unsplash.jpg', '', 0, '0.00', 9, 1, 1),
                    ('Sandale negre din piele barbat', '149.00', 30, 'sandale-negre-din-piele-barbat', 'michael-frattaroli-257751-unsplash.jpg', '', 0, '0.00', 6, 1, 1),
                    ('Tricou alb unisex', '39.00', 35, 'tricou-alb-unisex', 'modern-essentials-792824-unsplash.jpg', '', 0, '0.00', 2, 1, 1),
                    ('Fusta lunga cu animal print', '99.00', 32, 'fusta-lunga-cu-animal-print', 'bbh-singapore-1417572-unsplash.jpg', '', 0, '0.00', 3, 1, 1);")
    ) {
        print_ra("Table Product created successfully");
    } else {
        print_ra("Error creating table Product: " . $db->error());
    }

    // Create Tables Image
    if ($db->query("CREATE TABLE $databaseName.image (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(255) NOT NULL,
                        product_id int(11) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1',
                        CONSTRAINT FK_product FOREIGN KEY (product_id) REFERENCES product(id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.image (name, product_id, active) VALUES
                    ('dorian-hurst-800973-unsplash.jpg', 2, 1),
                    ('modern-essentials-798264-unsplash.jpg', 5, 1),
                    ('bbh-singapore-1417567-unsplash.jpg', 6, 1);")
    ) {
        print_ra("Table Image created successfully");
    } else {
        print_ra("Error creating table Image: " . $db->error());
    }

    // Create Tables Image Carousel
    if ($db->query("CREATE TABLE $databaseName.image_carousel (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(255) NOT NULL,
                        title varchar(255) NOT NULL,
                        sub_title varchar(255) NOT NULL,
                        link varchar(255) NOT NULL,
                        active int(11) NOT NULL DEFAULT '1'
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.image_carousel (name, title, sub_title, link, active) VALUES
                    ('carousel-glasses.png', 'Vine vara!', 'Incearca modelele de ochelari', '/category/ochelari', 1),
                    ('carousel-summer.png', 'Incearca noii adidasi', '', '/category/adidasi', 1),
                    ('carousel-sea.png', '', '', '/category/ochelari', 1);")
    ) {
        print_ra("Table Image Carousel created successfully");
    } else {
        print_ra("Error creating table Image Carousel: " . $db->error());
    }

    // Create Tables Client
    if ($db->query("CREATE TABLE $databaseName.client (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        email varchar(100) NOT NULL,
                        password varchar(100) NOT NULL,
                        first_name varchar(100) NOT NULL,
                        last_name varchar(100) NOT NULL,
                        phone_number varchar(100) NOT NULL,
                        addressing varchar(10) DEFAULT NULL,
                        INDEX client_email_index (email)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Client created successfully");
    } else {
        print_ra("Error creating table Client: " . $db->error());
    }

    // Create Tables Client Billing Data
    if ($db->query("CREATE TABLE $databaseName.client_billing_data (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        full_name varchar(100) NOT NULL,
                        phone_number varchar(100) NOT NULL,
                        unique_registration_code varchar(100) NOT NULL,
                        address varchar(100) NOT NULL,
                        type_person tinyint(4) NOT NULL COMMENT '1-PF; 2-PJ',
                        active tinyint(4) NOT NULL DEFAULT '1',
                        client_id int(11) NOT NULL,
                        CONSTRAINT FK_client_billing_data FOREIGN KEY (client_id) REFERENCES client(id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Client Billing Data created successfully");
    } else {
        print_ra("Error creating table Client Billing Data: " . $db->error());
    }

    // Create Tables Client Delivery Data
    if ($db->query("CREATE TABLE $databaseName.client_delivery_data (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        full_name varchar(100) NOT NULL,
                        phone_number varchar(100) NOT NULL,
                        address varchar(100) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1',
                        client_id int(11) NOT NULL,
                        CONSTRAINT FK_client_delivery_data FOREIGN KEY (client_id) REFERENCES client(id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Client Delivery Data created successfully");
    } else {
        print_ra("Error creating table Client Delivery Data: " . $db->error());
    }

    // Create Tables Order
    if ($db->query("CREATE TABLE $databaseName.`order` (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        total_price decimal(10,2) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1',
                        client_id int(11) NOT NULL,
                        order_json text NOT NULL,
                        timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        CONSTRAINT FK_order_client FOREIGN KEY (client_id) REFERENCES client(id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Order created successfully");
    } else {
        print_ra("Error creating table Order: " . $db->error());
    }

    // Create Tables Role
    if ($db->query("CREATE TABLE $databaseName.role (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(100) NOT NULL,
                        `key` varchar(100) NOT NULL,
                        description varchar(100) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1'
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.role (id, name, `key`, description, active) VALUES
                    (1, 'Adaugare Produse', 'add_product', 'Poate adauga noi produse in magazin.', 1),
                    (2, 'Editare Produse', 'edit_product', 'Poate edita produsele existente in magazin.', 1),
                    (3, 'Activ/Inactiv Produse', 'cancel_product', 'Poate activa sau dezactiva produsele existente in magazin.', 1),
                    (4, 'Adaugare Categorii', 'add_category', 'Poate adauga noi categorii in magazin.', 1),
                    (5, 'Editare Categorii', 'edit_category', 'Poate edita categoriile existente in magazin.', 1),
                    (6, 'Activ/Inactiv Categorii', 'cancel_category', 'Poate activa sau dezactiva categoriile existente in magazin.', 1),
                    (7, 'Anulare Comenzi', 'cancel_order', 'Poate anula comenzile clientilor.', 1),
                    (8, 'Creeare Facturi', 'add_invoice', 'Poate creea o noua factura pentru o comanda existenta.', 1),
                    (9, 'Anulare Facturi', 'cancel_invoice', 'Poate anula facturile existente.', 1),
                    (10, 'Creeare Chitante', 'add_receipt', 'Poate creea o noua chitanta pentru o factura existenta.', 1),
                    (11, 'Anulare Chitante', 'cancel_receipt', 'Poate anula o chitanta existenta.', 1),
                    (12, 'Acces Setari', 'access_settings', 'Are acces la pagina Setari. Poate modifica seriile, utilizatorii si datele firmei.', 1);")
    ) {
        print_ra("Table Role created successfully");
    } else {
        print_ra("Error creating table Role: " . $db->error());
    }

    // Create Tables User Role
    if ($db->query("CREATE TABLE $databaseName.user_role (
                        user_id int(11) NOT NULL,
                        role_id int(11) NOT NULL,
                        CONSTRAINT FK_user_role_user_id FOREIGN KEY (user_id) REFERENCES user(id),
                        CONSTRAINT FK_user_role_role_id FOREIGN KEY (role_id) REFERENCES role(id),
                        PRIMARY KEY (user_id, role_id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        &&
        $db->query("INSERT INTO $databaseName.user_role (user_id, role_id) VALUES
                    (1, 1),
                    (1, 2),
                    (1, 3),
                    (1, 4),
                    (1, 5),
                    (1, 6),
                    (1, 7),
                    (1, 8),
                    (1, 9),
                    (1, 10),
                    (1, 11),
                    (1, 12);")
    ) {
        print_ra("Table User Role created successfully");
    } else {
        print_ra("Error creating table User Role: " . $db->error());
    }

    // Create Tables Series Invoice
    if ($db->query("CREATE TABLE $databaseName.series_invoice (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(100) NOT NULL,
                        value varchar(100) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1'
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Series Invoice created successfully");
    } else {
        print_ra("Error creating table Series Invoice: " . $db->error());
    }

    // Create Tables Series Receipt
    if ($db->query("CREATE TABLE $databaseName.series_receipt (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        name varchar(100) NOT NULL,
                        value varchar(100) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1'
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Series Receipt created successfully");
    } else {
        print_ra("Error creating table Series Receipt: " . $db->error());
    }

    // Create Tables Invoice
    if ($db->query("CREATE TABLE $databaseName.invoice (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        series_id int(11) NOT NULL,
                        number varchar(10) NOT NULL,
                        release_date date DEFAULT NULL,
                        due_date date DEFAULT NULL,
                        order_id int(11) NOT NULL,
                        user_id int(11) NOT NULL,
                        cancel_id int(11) NOT NULL DEFAULT '0',
                        reference_id int(11) NOT NULL DEFAULT '0',
                        active tinyint(4) NOT NULL DEFAULT '1',
                        timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        CONSTRAINT FK_invoice_series_id FOREIGN KEY (series_id) REFERENCES series_invoice(id),
                        CONSTRAINT FK_invoice_order_id FOREIGN KEY (order_id) REFERENCES `order`(id),
                        CONSTRAINT FK_invoice_user_id FOREIGN KEY (user_id) REFERENCES user(id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Invoice created successfully");
    } else {
        print_ra("Error creating table Invoice: " . $db->error());
    }

    // Create Tables Receipt
    if ($db->query("CREATE TABLE $databaseName.receipt (
                        id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
                        series_id int(11) NOT NULL,
                        number varchar(10) NOT NULL,
                        release_date date DEFAULT NULL,
                        invoice_id int(11) NOT NULL,
                        user_id int(11) NOT NULL,
                        active tinyint(4) NOT NULL DEFAULT '1',
                        timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        CONSTRAINT FK_receipt_series_id FOREIGN KEY (series_id) REFERENCES series_receipt(id),
                        CONSTRAINT FK_receipt_order_id FOREIGN KEY (invoice_id) REFERENCES invoice(id),
                        CONSTRAINT FK_receipt_user_id FOREIGN KEY (user_id) REFERENCES user(id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        print_ra("Table Receipt created successfully");
    } else {
        print_ra("Error creating table Receipt: " . $db->error());
    }

    // Drop all for new business
//    $db->query("DROP USER '$databaseUser'@'localhost';");
//    $db->query("DROP DATABASE $databaseName;");

    $src = $CONF['serverpath'] . 'default';
    $dst = $CONF['serverpath'] . '../' . preg_replace('/[^a-zA-Z0-9]/', '', $businessName);

    if (!mkdir($dst, 0755, true)) {
        die('Failed to create folders...');
    }

    recurse_copy($src, $dst);

    $myfile = fopen($dst . "/core/config.ini", "w") or die("Unable to open file!");
    $txt = "[database]\n" .
        "host = localhost\n" .
        "username = $databaseUser\n" .
        "password = $databasePass\n" .
        "dbname = $databaseName\n";

    fwrite($myfile, $txt);
    fclose($myfile);
}

function recurse_copy($src, $dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                recurse_copy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}
