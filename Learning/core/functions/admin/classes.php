<?php

function classes()
{
    global $link;
    global $database;
    global $template;

    if (isset($link[3]) && $link[3]) {
        if ($link[3] === 'list') {
            $where = "WHERE 1";
            if (isset($_POST['filter_class'])) {
                if ($_POST['class']) {
                    $where .= " AND c.id = '" . $_POST['class'] . "'";
                }
                if ($_POST['status']) {
                    if ($_POST['status'] === 'active') {
                        $where .= " AND c.active = '1'";
                    } elseif ($_POST['status'] === 'inactive') {
                        $where .= " AND c.active = '0'";
                    }
                }
            }

            if (isset($_SESSION['message'])) {
                $successMessage = $_SESSION['message'];
                $template->assign('successMessage', $successMessage);
                unset($_SESSION['message']);
            }
            if (isset($_SESSION['message-error'])) {
                $successMessageError = $_SESSION['message-error'];
                $template->assign('successMessageError', $successMessageError);
                unset($_SESSION['message-error']);
            }

            $sql = "SELECT * FROM class c {$where}";
            $classesList = $database->select($sql);

            $template->assign('classesList', $classesList);
            $template->assign('classes', getAllClasses());

            return $template->fetch('admin/class/classList.tpl');
        }

        if ($link[3] === 'add') {
            if (isset($_POST['add_class'])) {
                $classUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                $classUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $classUrl);

                $sql = "SELECT url FROM class
                        WHERE url = '" . $database->escape($classUrl) . "'";

                if ($database->query($sql)->fetch_object()) {
                    $error = 'Link-ul clasei trebuie sa fie unic!';

                    $template->assign('error', $error);

                    return $template->fetch('admin/class/classAdd.tpl');
                }

                $active = 1;
                if (!isset($_POST['active'])) {
                    $active = 0;
                }

                $sql = "INSERT INTO class 
                        (name, url, active) VALUES (
                        '" . $database->escape($_POST['name']) . "',
                        '" . $database->escape($_POST['url']) . "',
                        '" . intval($active) . "')";
                $database->query($sql);

                $_SESSION['message'] = 'Clasa a fost adaugata.';

                header("Location: /admin/classes/list");
            }

            return $template->fetch('admin/class/classAdd.tpl');
        }

        if ($link[3] === 'edit') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "SELECT * FROM class WHERE id =  '" . intval($link[4]) . "'";
                $classEdit = $database->query($sql)->fetch_object();

                if (empty($classEdit)) {
                    return $template->fetch('admin/somethingWentWrong.tpl');
                }

                $template->assign('classEdit', $classEdit);

                if (isset($_POST['edit_class'])) {
                    $classUrl = strtolower(str_replace(' ', '-', $_POST['url']));
                    $classUrl = preg_replace('/[^a-zA-Z0-9-]/', '', $classUrl);

                    $sql = "SELECT url FROM class
                            WHERE url = '" . $database->escape($classUrl) . "'
                            AND id != '" . intval($link[4]) . "'";

                    if ($database->query($sql)->fetch_object()) {
                        $error = 'Link-ul clasei trebuie sa fie unic!';

                        $template->assign('error', $error);

                        return $template->fetch('admin/class/classEdit.tpl');
                    }

                    $active = 1;
                    if (!isset($_POST['active'])) {
                        $active = 0;
                    }

                    $sql = "UPDATE class
                            SET name = '" . $database->escape($_POST['name']) . "',
                                url = '" . $database->escape($_POST['url']) . "',
                                active = '" . intval($active) . "'
                            WHERE id = '" . intval($link[4]) . "'";
                    $database->query($sql);

                    $_SESSION['message'] = 'Clasa a fost editata.';

                    header("Location: /admin/classes/list");
                }

                return $template->fetch('admin/class/classEdit.tpl');
            }
        }

        if ($link[3] === 'enable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE class SET active = 1
                        WHERE id = '" . intval($link[4]) . "' AND active = 0";
                $database->query($sql);

                $_SESSION['message'] = 'Clasa a fost activata.';

                header("Location: /admin/classes/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        if ($link[3] === 'disable') {
            if (isset($link[4]) && intval($link[4])) {
                $sql = "UPDATE class SET active = 0
                        WHERE id = '" . intval($link[4]) . "' AND active = 1";
                $database->query($sql);

                $_SESSION['message'] = 'Clasa a fost dezactivat.';

                header("Location: /admin/classes/list");
            }

            return $template->fetch('admin/404.tpl');
        }

        return $template->fetch('admin/404.tpl');
    }

    return $template->fetch('admin/404.tpl');
}
