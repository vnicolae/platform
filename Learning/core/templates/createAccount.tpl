<h2 class="mt-4 mb-3 border-bottom-5">
    Creeaza-ti rapid un cont
</h2>

<form class="needs-validation" method="POST" novalidate autocomplete="off">
    {if isset($errors) && empty($error)}
        <div class="form-group row">
            <div class="offset-4 col-sm-7">
                <div class="alert alert-danger mb-0" role="alert">
                    {foreach from=$errors item=error}
                        <li class="list-group">{$error}</li>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}

    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="last_name">
            Nume <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="form-control" id="last_name" name="last_name"
                   value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}" required>
            <div class="invalid-feedback">Introduceti un Nume valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="first_name">
            Prenume <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="form-control" id="first_name" name="first_name"
                   value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}" required>
            <div class="invalid-feedback">Introduceti un Prenume valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="email">
            Email <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="email" class="form-control" id="email" name="email"
                   value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" required>
            <div class="invalid-feedback">Introduceti un Email valid.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="birth_date">
            Data nasterii <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="text" class="datepicker form-control" name="birth_date" id="birth_date"
                   value="{if isset($smarty.post.birth_date)}{$smarty.post.birth_date}{/if}"
                   data-date-format="dd.mm.yyyy" required>
            <div class="invalid-feedback">Introduceti o Data nasterii valida.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="password">
            Parola <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="password" class="form-control" id="password" name="password" required>
            <div class="invalid-feedback">Introduceti o Parola valida.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="confirm_password">
            Confirmare parola <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
            <div class="invalid-feedback">Introduceti Confirmarea parolei.</div>
        </div>
    </div>
    <div class="form-group row">
        <label class="offset-1 col-sm-3 col-form-label" for="role">
            Sunt <span class="text-danger font-weight-bold">*</span>
        </label>
        <div class="col-sm-7">
            <div class="custom-control custom-radio m-2">
                <input type="radio" id="Elev" name="role" class="custom-control-input" value="1"
                       {if isset($smarty.post.role) && $smarty.post.role == 1}checked{/if} required>
                <label class="custom-control-label" for="Elev">Elev</label>
            </div>
            <div class="custom-control custom-radio m-2">
                <input type="radio" id="Profesor" name="role" class="custom-control-input" value="5"
                       {if isset($smarty.post.role) && $smarty.post.role == 5}checked{/if} required>
                <label class="custom-control-label" for="Profesor">Profesor</label>

                <div class="invalid-feedback">Selectați daca sunteți elev sau cadru didactic</div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <small class="offset-4 col-sm-7">
            Campurile marcate cu <span class="text-danger font-weight-bold">*</span> sunt obligatorii
        </small>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-7">
            <button type="submit" class="btn btn-primary btn-custom" name="create_account">Creeaza cont</button>
        </div>
    </div>
</form>
