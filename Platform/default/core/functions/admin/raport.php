<?php

use Dompdf\Dompdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

function raport()
{
    global $db;
    global $CONF;
    global $template;

    $where = "AND 1";

    if (isset($_POST['invoice_series']) && $_POST['invoice_series']) {
        $where .= " AND i.series_id = '" . intval($_POST['invoice_series']) . "'";
    }
    if (isset($_POST['invoice_number']) && $_POST['invoice_number']) {
        $where .= " AND i.number = '" . intval($_POST['invoice_number']) . "'";
    }
    if (isset($_POST['last_name']) && $_POST['last_name']) {
        $where .= " AND c.last_name LIKE '%" . $db->escape_string($_POST['last_name']) . "%'";
    }
    if (isset($_POST['first_name']) && $_POST['first_name']) {
        $where .= " AND c.first_name LIKE '%" . $db->escape_string($_POST['first_name']) . "%'";
    }
    if (isset($_POST['email']) && $_POST['email']) {
        $where .= " AND c.email LIKE '%" . $db->escape_string($_POST['email']) . "%'";
    }
    if (isset($_POST['order']) && $_POST['order']) {
        $where .= " AND i.order_id = '" . intval($_POST['order']) . "'";
    }
    if (isset($_POST['min_date']) && $_POST['min_date']) {
        $where .= " AND i.release_date >= '" . date('Y-m-d', strtotime($_POST['min_date'])) . "'";
    }
    if (isset($_POST['max_date']) && $_POST['max_date']) {
        $where .= " AND i.release_date <= '" . date('Y-m-d', strtotime($_POST['max_date'])) . "'";
    }
    if (isset($_POST['user']) && $_POST['user']) {
        $where .= " AND i.user_id = '" . intval($_POST['user']) . "'";
    }
    if (isset($_POST['min_due_date']) && $_POST['min_due_date']) {
        $where .= " AND i.due_date >= '" . date('Y-m-d', strtotime($_POST['min_due_date'])) . "'";
    }
    if (isset($_POST['max_due_date']) && $_POST['max_due_date']) {
        $where .= " AND i.due_date <= '" . date('Y-m-d', strtotime($_POST['max_due_date'])) . "'";
    }

    $sql = "SELECT i.*, c.first_name, c.last_name, o.total_price, o.client_id, si.value as 'series_value', o.order_json
            FROM invoice i
            JOIN series_invoice si ON si.id  = i.series_id
            JOIN `order` o ON i.order_id = o.id
            JOIN client c ON o.client_id = c.id
            WHERE i.active = 1 $where
            ORDER BY timestamp DESC ";
    $results = $db->select($sql);

    $totalVAT = 0;
    $totalWithoutVAT = 0;
    $totalGeneral = 0;
    foreach ($results as $key => $invoice) {
        $sql = "SELECT r.id, r.number, sr.value as 'series_value'
                FROM receipt r
                JOIN series_receipt sr ON r.series_id = sr.id
                WHERE r.active = 1 AND r.invoice_id = {$invoice['id']}";
        $results[$key]['receipt'] = $db->query($sql)->fetch_assoc();

        $subTotalVAT = 0;
        $subTotalWithoutVAT = 0;
        $order = json_decode($invoice['order_json']);
        foreach ($order->cart->products as $product) {
            $priceWithoutVAT = ($product->price * 100) / ($product->vat->value + 100);
            $productWithoutVAT = $product->quantity * $priceWithoutVAT;
            $subTotalWithoutVAT = $subTotalWithoutVAT + $productWithoutVAT;
            $priceVAT = $product->quantity * ($priceWithoutVAT * $product->vat->value) / 100;
            $subTotalVAT = $subTotalVAT + $priceVAT;
        }

        if ($order->delivery->type_delivery === 'courier') {
            $priceWithoutVAT = ($order->cart->deliveryPrice * 100) / (19.00 + 100);
            $subTotalWithoutVAT = $subTotalWithoutVAT + $priceWithoutVAT;
            $priceVAT = ($priceWithoutVAT * $product->vat->value) / 100;
            $subTotalVAT = $subTotalVAT + $priceVAT;
        }

        $results[$key]['totals']['subTotalWithoutVAT'] = number_format($subTotalWithoutVAT, 2);
        $results[$key]['totals']['subTotalVAT'] = number_format($subTotalVAT, 2);

        $totalVAT += $subTotalVAT;
        $totalWithoutVAT += $subTotalWithoutVAT;
    }

    $totalGeneral += $totalVAT + $totalWithoutVAT;

    $template->assign('results', $results);
    $template->assign('totalVAT', $totalVAT);
    $template->assign('totalWithoutVAT', $totalWithoutVAT);
    $template->assign('totalGeneral', $totalGeneral);
    $template->assign('CONF', $CONF);

    if (isset($_POST['export_pdf'])) {
        $html = $template->fetch('admin/raport/raportTable.tpl');

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $pdf_name = "Raport_PDF_" . date('d-m-Y', strtotime('now')) . ".pdf";

        // Output the generated PDF to Browser
        $dompdf->stream($pdf_name);

        $pdf = $dompdf->output();
        $file_location = $CONF['serverpath'] . "lib/file/raport/" . $pdf_name;
        file_put_contents($file_location, $pdf);
    }

    if (isset($_POST['export_excel'])) {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', '#');
        $sheet->setCellValue('B1', 'Denumire client');
        $sheet->setCellValue('C1', 'Serie Numar');
        $sheet->setCellValue('D1', 'Data emitere');
        $sheet->setCellValue('E1', 'Data scadenta');
        $sheet->setCellValue('F1', 'Val. fara TVA');
        $sheet->setCellValue('G1', 'Val. TVA');
        $sheet->setCellValue('H1', 'Total general');
        $sheet->setCellValue('I1', 'Doc. plata');
        $sheet->setCellValue('J1', 'Val. achitata');
        $sheet->setCellValue('K1', 'Rest plata');

        $fontBold = [
            'font' => [
                'bold' => true
            ],
        ];
        $border = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_MEDIUM,
                    'color' => ['argb' => 'black'],
                ],
            ],
        ];
        $backgroudGreen = [
            'fillType' => Fill::FILL_SOLID,
            'color' => [
                'rgb' => '66ff33'
            ],
        ];
        $backgroudYellow = [
            'fillType' => Fill::FILL_SOLID,
            'color' => [
                'rgb' => 'ffff00'
            ],
        ];
        $backgroudRed = [
            'fillType' => Fill::FILL_SOLID,
            'color' => [
                'rgb' => 'FF0000'
            ],
        ];

        if ($results) {
            $curNumber = 2;
            $amountPaid = 0;
            $amountDue = 0;

            foreach ($results as $result) {
                $sheet->setCellValue('A' . $curNumber, $curNumber - 1);
                $sheet->setCellValue('B' . $curNumber, $result['last_name'] . ' ' . $result['first_name']);
                $sheet->setCellValue('C' . $curNumber, $result['series_value'] . ' ' . $result['number']);
                $sheet->setCellValue('D' . $curNumber, date('Y-m-d', strtotime($result['release_date'])));
                $sheet->getStyle('D' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $sheet->setCellValue('E' . $curNumber, date('Y-m-d', strtotime($result['due_date'])));
                $sheet->getStyle('E' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $sheet->setCellValue('F' . $curNumber, number_format($result['totals']['subTotalWithoutVAT'], 2));
                $sheet->getStyle('F' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->setCellValue('G' . $curNumber, number_format($result['totals']['subTotalVAT'], 2));
                $sheet->getStyle('G' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->setCellValue('H' . $curNumber, number_format($result['total_price'], 2));
                $sheet->getStyle('H' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                if (isset($result['receipt']['id'])) {
                    $sheet->setCellValue('I' . $curNumber, $result['receipt']['series_value'] . ' ' . $result['receipt']['number']);
                } else {
                    $sheet->setCellValue('I' . $curNumber, '-');
                }
                if (isset($result['receipt']['id'])) {
                    $sheet->setCellValue('J' . $curNumber, number_format($result['total_price'], 2));
                    $sheet->getStyle('J' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $amountPaid = $amountPaid + $result['total_price'];
                } else {
                    $sheet->setCellValue('J' . $curNumber, '0.00');
                    $sheet->getStyle('J' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
                if (isset($result['receipt']['id'])) {
                    $sheet->setCellValue('K' . $curNumber, '0.00');
                    $sheet->getStyle('K' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                } else {
                    $sheet->setCellValue('K' . $curNumber, number_format($result['total_price'], 2));
                    $sheet->getStyle('K' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $amountDue = $amountDue + $result['total_price'];
                }
                $curNumber++;
            }

            $sheet->setCellValue('F' . $curNumber, number_format($totalWithoutVAT, 2));
            $sheet->getStyle('F' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue('G' . $curNumber, number_format($totalVAT, 2));
            $sheet->getStyle('G' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue('H' . $curNumber, number_format($totalGeneral, 2));
            $sheet->getStyle('H' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue('J' . $curNumber, number_format($amountPaid, 2));
            $sheet->getStyle('J' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue('K' . $curNumber, number_format($amountDue, 2));
            $sheet->getStyle('K' . $curNumber)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach (range('A', 'K') as $col) {
                $sheet->getColumnDimension($col)->setAutoSize(true);
            }

            $sheet->getStyle('A1:K1')->applyFromArray($fontBold);
            $sheet->getStyle('F2:F' . $curNumber)->applyFromArray($fontBold);
            $sheet->getStyle('G2:G' . $curNumber)->applyFromArray($fontBold);
            $sheet->getStyle('H2:H' . $curNumber)->applyFromArray($fontBold);
            $sheet->getStyle('H2:H' . $curNumber)->getFill()->applyFromArray($backgroudGreen);
            $sheet->getStyle('J2:J' . $curNumber)->applyFromArray($fontBold);
            $sheet->getStyle('J2:J' . $curNumber)->getFill()->applyFromArray($backgroudYellow);
            $sheet->getStyle('K2:K' . $curNumber)->applyFromArray($fontBold);
            $sheet->getStyle('K2:K' . $curNumber)->getFill()->applyFromArray($backgroudRed);

            $sheet->getStyle('F' . $curNumber . ':H' . $curNumber)->applyFromArray($border);
            $sheet->getStyle('J' . $curNumber . ':K' . $curNumber)->applyFromArray($border);

            $sheet->getStyle('A2:A' . --$curNumber)->applyFromArray($fontBold);
            $sheet->getStyle('A1:K' . $curNumber)->applyFromArray($border);
        } else {
            $sheet->setCellValue('A2', 'Niciun rezultat');
            $sheet->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->mergeCells("A2:K2");

            $sheet->getStyle('A1:K2')->applyFromArray($border);
            $sheet->getStyle('A1:K2')->applyFromArray($fontBold);

            foreach (range('A', 'K') as $col) {
                $sheet->getColumnDimension($col)->setAutoSize(true);
            }
        }

        $writer = new Xlsx($spreadsheet);

        $excel_name = "Raport_EXCEL_" . date('d-m-Y', strtotime('now'));

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $excel_name . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output', 'xlsx');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');


        $file_location = $CONF['serverpath'] . "lib/file/raport/" . $excel_name . ".xlsx";
        $writer->save($file_location);
    }

    $sql = "SELECT * FROM user";
    $users = $db->select($sql);

    $sql = "SELECT * FROM series_invoice";
    $seriesInvoice = $db->select($sql);

    $template->assign('users', $users);
    $template->assign('seriesInvoice', $seriesInvoice);

    return $template->fetch('admin/raport/raportView.tpl');
}
