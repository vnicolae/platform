<h4 class="pb-3 mt-3 mb-3 border-bottom">
    Cosul meu
</h4>

<ul class="nav flex-column flex-nowrap pb-3">
    <li class="nav-item">
        <a class="nav-link collapsed text-dark border rounded-top" href="#" data-toggle="collapse" data-target="#submenu-cart-page-info">
            <strong class="font-size-18">Detalii cos</strong>
        </a>

        <div class="collapse border rounded-bottom p-3 show" id="submenu-cart-page-info">
            {if !isset($smarty.session.cart) || empty($smarty.session.cart)}
                <div class="text-center">
                    <h5>Nu ai niciun produs in cos</h5>
                    <a class="btn btn-primary btn-custom" href="/" role="button">Intoarce-te la magazin</a>
                </div>
            {else}
                {assign var="totalQuantity" value=0}
                {assign var="totalPrice" value=0}
                <div id="cart-page-info">
                    {foreach from=$smarty.session.cart item=productCart}
                        <div class="row">
                            <div class="col-3">
                                <div class="text-center">
                                    <div class="overflow-hidden height-120">
                                        <img src="/images/product/{$productCart.image|default:'default-image.jpg'}"
                                             class="img-fluid width-120">
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="font-weight-bold">
                                    <a href="/product/{$productCart.url}" class="text-dark">
                                        {$productCart.name}
                                    </a>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-group row align-items-center">
                                    <div class="col-8">
                                        <select class="form-control form-control-sm sync-quantity-to-cart-page" data-id="{$productCart.id}">
                                            {section name=loop start=1 loop=26 max=$productCart.stock step=1}
                                                <option value="{$smarty.section.loop.index}"
                                                        {if $smarty.section.loop.index == {$productCart.quantityCart}} selected {/if}
                                                >{$smarty.section.loop.index}</option>
                                            {/section}
                                        </select>
                                    </div>
                                    <strong>buc</strong>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="text-right">
                                    {if $productCart.is_discount}
                                        {assign var="totalProductPrice" value=$productCart.quantityCart * $productCart.new_price}
                                        {assign var="totalProductOldPrice" value=$productCart.quantityCart * $productCart.price}
                                        {assign var="totalProductEconomy" value=$totalProductOldPrice - $totalProductPrice}
                                        <h4>{$totalProductPrice|string_format: "%.2f"} Lei</h4>
                                        <h5>
                                            <del>{$totalProductOldPrice|string_format: "%.2f"} Lei</del>
                                        </h5>
                                        <p>Economisesti:<br>{$totalProductEconomy|string_format: "%.2f"} Lei</p>
                                    {else}
                                        {assign var="totalProductPrice" value=$productCart.quantityCart * $productCart.price}
                                        <h4>{$totalProductPrice|string_format: "%.2f"} Lei</h4>
                                    {/if}
                                    <br>
                                    <a class="btn btn-sm position-absolute bottom-0 right-0 pr-3 delete-product-to-cart-page"
                                       title="Sterge produsul din cos" data-id="{$productCart.id}" role="button">
                                        Sterge
                                    </a>
                                </div>
                            </div>
                            {assign var="totalPrice" value=$totalPrice + $totalProductPrice}
                        </div>
                        <hr>
                    {/foreach}

                    <div class="row">
                        <div class="col-7 border-right">
                            <h5>Sumar Comanda</h5>
                            <table class="table table-borderless table-paddingless">
                                <tr>
                                    <td>Cost produse</td>
                                    <td class="text-right">
                                        {$totalPrice|string_format: "%.2f"} Lei
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cost livrare</td>
                                    <td class="text-right" id="deliveryPrice">
                                        <strong class="text-success">GRATUIT</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-5">
                            <h5>Total</h5>
                            <h4 id="totalPrice">{$totalPrice|string_format: "%.2f"} Lei</h4>
                        </div>
                    </div>
                </div>
            {/if}
        </div>

        {if (!isset($smarty.session.client) || empty($smarty.session.client))
            && isset($smarty.session.cart) && !empty($smarty.session.cart)}
            <a class="nav-link collapsed text-dark border rounded-top" href="#" data-toggle="collapse" data-target="#submenu-my-account">
                <strong class="font-size-18">Contul meu</strong>
            </a>

            <div class="collapse border rounded-bottom p-3 show" id="submenu-my-account">
                <div class="row">
                    <div class="col-6 text-center">
                        <h6>Ai deja cont la noi?</h6>
                        <a class="btn btn-primary btn-custom" href="/loginAccount/?ref=myCart" role="button">Intra in cont</a>
                    </div>

                    <div class="col-6 text-center">
                        <h6>Inca nu ti-ai creeat cont?</h6>
                        <a class="btn btn-primary btn-custom" href="/createAccount/?ref=myCart" role="button">Cont nou</a>
                    </div>
                </div>
            </div>
        {/if}

        {if isset($smarty.session.client) && !empty($smarty.session.client) &&
            isset($smarty.session.cart) && !empty($smarty.session.cart)}
            <form class="needs-validation-cart" method="POST" novalidate autocomplete="off">
                <a class="nav-link collapsed text-dark border rounded-top" href="#" data-toggle="collapse" data-target="#submenu-order-detail">
                    <strong class="font-size-18">Detalii comanda</strong>
                </a>

                <div class="collapse border rounded-bottom p-3 show" id="submenu-order-detail">
                    <h6>Date de facturare</h6>
                    {include file="cart/zone/billingData.tpl"}

                    <hr>
                    <h6>Modalitate livrare</h6>
                    {include file="cart/zone/deliveryData.tpl"}

                    <hr>
                    <h6>Metoda de plata</h6>
                    {include file="cart/zone/paymentData.tpl"}

                    <hr>
                    <h6>Observatii</h6>
                    <div class="form-group m-2">
                        <textarea class="form-control" name="note" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12 text-center mt-3">
                        <button type="submit" class="btn btn-primary btn-custom" name="send_order">Trimite comanda</button>
                    </div>
                </div>
            </form>
        {/if}
    </li>
</ul>
