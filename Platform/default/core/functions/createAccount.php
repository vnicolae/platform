<?php

function createAccount()
{
    global $db;
    global $template;

    if (!isset($_SESSION['client']) || empty($_SESSION['client'])) {
        $errors = array();

        if (isset($_POST['create_account'])) {
            if ($_POST['password'] !== $_POST['confirm_password']) {
                $errors[] = 'Parolele nu coincid!';
            }

            $sql = "SELECT * FROM client WHERE email = '" . $db->escape_string($_POST['email']) . "'";
            $client = $db->query($sql)->fetch_object();

            if (!empty($client)) {
                $errors[] = 'E-mail este deja folosit!';
            }

            if (!empty($errors)) {
                $template->assign('errors', $errors);
            } else {
                $sql = "INSERT INTO client (email, password, first_name, last_name, phone_number) 
                    VALUES ('" . $db->escape_string($_POST['email']) . "', '" . md5($db->escape_string($_POST['password'])) . "',
                    '" . $db->escape_string($_POST['first_name']) . "', '" . $db->escape_string($_POST['last_name']) . "',
                    '" . $db->escape_string($_POST['phone_number']) . "')";
                $client_id = $db->query($sql, true);

                $_SESSION['client']['client_id'] = $client_id;
                $_SESSION['client']['email'] = $_POST['email'];
                $_SESSION['client']['first_name'] = $_POST['first_name'];
                $_SESSION['client']['last_name'] = $_POST['last_name'];

                if (isset($_GET['ref']) && $_GET['ref'] === 'myCart') {
                    header("Location: /myCart");
                    exit;
                }

                header("Location: /");
            }
        }

        return $template->fetch('createAccount.tpl');
    }

    return $template->fetch('404.tpl');
}
